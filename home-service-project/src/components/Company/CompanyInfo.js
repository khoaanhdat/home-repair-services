import React from 'react';

import BannerPage from '../Layout/BannerPage';
import Card from '../Layout/Card';
import classes from './CompanyInfo.module.css';

const CompanyInfo = (props) => {
    return (
        <div className={classes.info}>
            <BannerPage bannerImg={props.bannerImg} />
            <div className={classes.logo}>
                <img src={props.companyLogo} alt="" />
            </div>
            <div className={classes.details}>
                <Card
                    className="border-primary"
                    header={props.companyName}
                    bodyNameClass="company-info"
                >
                    <div className={classes.description}>
                        <span>{props.companyDescription}</span>
                    </div>
                    <div className={classes['info-basic']}>
                        <span>Đánh Giá: {props.avgReview}</span>
                        <span>Số Lượng Nhân Viên: {props.countEmp}</span>
                        <span>Khách Hàng: {props.countCustomer}</span>
                        <span>Mã Số Thuế: {props.taxCode}</span>
                    </div>
                </Card>
            </div>
        </div>
    );
}

export default CompanyInfo;