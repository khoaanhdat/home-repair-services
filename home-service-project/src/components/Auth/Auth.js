import React, { useContext } from 'react';

import AuthContext from '../../store/auth-context';
import Modal from '../UI/Modal';
import classes from './Auth.module.css';

const Auth = (props) => {
    const authCtx = useContext(AuthContext);

    const hideAuthFormPopup = () => {
       authCtx.hideLoginForm();
       authCtx.hideSignUpForm();
    }

    return (
        <Modal onClose={hideAuthFormPopup}>
            <section className={classes.wrapper}>
                <div className={classes.content}>
                    <header>
                        <h1>{props.title}</h1>
                    </header>
                    <section>
                        <form action='' className={classes["auth-form"]}>
                            {props.children}
                        </form>
                    </section>
                </div>
            </section>
        </Modal>
    );
}

export default Auth;