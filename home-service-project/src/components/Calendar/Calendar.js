import React, { useState } from 'react';
import CalendarLib from 'react-calendar';

import './Calendar.css';
import classes from './Calendar.module.css';
import InputAuth from '../UI/InputAuth';

const Calendar = (props) => {
  const [date, setDate] = useState(props.initDate ? props.initDate : new Date());
  const [isShowCalendar, setIsShowCalendar] = useState(false);

  const focusHandler = () => {
    setIsShowCalendar(true);
    if (props.onFocus) {
      props.onFocus();
    }
  }

  const blurHandler = () => {
    setTimeout(() => {
      setIsShowCalendar(false);
    }, 500);
  }

  const onDateChange = (e) => {
    setDate(e);
    if (props.addBookTime) {
      props.addBookTime(e.toLocaleDateString('en-GB'));
    }
  }

  return (
    <div className={classes.calendar}>
      <InputAuth
        label=""
        placeholder="Chọn ngày bạn muốn sửa"
        input={{
          id: "",
          type: "text"
        }}
        value={date.toLocaleDateString('en-GB')}
        onChange={() => { }}
        onFocus={focusHandler}
        onBlur={blurHandler}
        disabled={props.disabled}
      />
      {isShowCalendar && (
        <CalendarLib
          onChange={onDateChange}
          value={date}
          minDate={props.minDate}
        />
      )}
    </div>
  );
}

export default Calendar;