import React, { useContext, useEffect } from 'react';

import Button from '../UI/Button';
import InputAuth from '../UI/InputAuth';
import Auth from '../Auth/Auth';
import classes from './Register.module.css';
import OTP from '../UI/OTP';
import useOTP from '../../hooks/use-otp';
import useInput from '../../hooks/use-input';
import useHttp from '../../hooks/use-http';
import { SignUp } from '../../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import AuthContext from '../../store/auth-context';
import { useHistory } from 'react-router';
import { MessageContext } from '../../store/message-context';

const Register = (props) => {
    // First Name
    const {
        value: enteredFirstName,
        isValid: enteredFirstNameIsValid,
        hasError: firstNameInputHasError,
        messageError: firstNameMessageError,
        valueChanged: firstNameChangeHandler,
        valueBlur: firstNameBlurHandler,
        checkIsNotEmpty: firstNameCheckIsNotEmpty
    } = useInput();

    const firstNameOnChanged = (e) => {
        firstNameChangeHandler(e);
        firstNameCheckIsNotEmpty(e.target.value);
    }

    const firstNameOnBlur = () => {
        firstNameBlurHandler();
        firstNameCheckIsNotEmpty(enteredFirstName);
    }

    //Last Name
    const {
        value: enteredLastName,
        isValid: enteredLastNameIsValid,
        hasError: lastNameInputHasError,
        messageError: lastNameMessageError,
        valueChanged: lastNameChangeHandler,
        valueBlur: lastNameBlurHandler,
        checkIsNotEmpty: lastNameCheckIsNotEmpty
    } = useInput();

    const lastNameOnChanged = (e) => {
        lastNameChangeHandler(e);
        lastNameCheckIsNotEmpty(e.target.value);
    }

    const lastNameOnBlur = () => {
        lastNameBlurHandler();
        lastNameCheckIsNotEmpty(enteredLastName);
    }

    // Phone Number
    const {
        value: enteredPhone,
        isValid: enteredPhoneIsValid,
        hasError: phoneInputHasError,
        messageError: phoneMessageError,
        valueChanged: phoneChangeHandler,
        valueBlur: phoneBlurHandler,
        checkIsNotEmpty: phoneCheckIsNotEmpty,
        checkIsNumber: phoneCheckIsNumber,
        checkIsPhoneNumber
    } = useInput();

    const phoneOnChanged = (event) => {
        phoneChangeHandler(event);
        phoneCheckIsNotEmpty(event.target.value);
        phoneCheckIsNumber(event.target.value);
        checkIsPhoneNumber(event.target.value);
    }

    const phoneOnBlur = () => {
        phoneBlurHandler();
        phoneCheckIsNotEmpty(enteredPhone);
        phoneCheckIsNumber(enteredPhone);
        checkIsPhoneNumber(enteredPhone);
    }

    // Validate OTP
    const phoneFormatted = enteredPhone.replace('0', '+84');

    const { otpIsValid, isSendOTP, errorMessage: errorMessageOTP, otpOnChange, onGetOTPSubmit } = useOTP(phoneFormatted);

    // Password
    const {
        value: enteredPassword,
        isValid: enteredPasswordIsValid,
        hasError: passwordInputHasError,
        messageError: passwordMessageError,
        valueChanged: passwordChangeHandler,
        valueBlur: passwordBlurHandler,
        checkIsNotEmpty: passwordCheckIsNotEmpty,
    } = useInput();

    const passwordOnChanged = (event) => {
        passwordChangeHandler(event);
        passwordCheckIsNotEmpty(event.target.value);
    }

    const passwordOnBlur = () => {
        passwordBlurHandler(enteredPassword);
        passwordCheckIsNotEmpty(enteredPassword);
    }

    // Repeat Password
    const {
        value: enteredRePassword,
        isValid: enteredRePasswordIsValid,
        hasError: rePasswordInputHasError,
        messageError: rePasswordMessageError,
        valueChanged: rePasswordChangeHandler,
        valueBlur: rePasswordBlurHandler,
        checkIsNotEmpty: rePasswordCheckIsNotEmpty,
    } = useInput();

    const rePasswordOnChanged = (event) => {
        rePasswordChangeHandler(event);
        rePasswordCheckIsNotEmpty(event.target.value);
    }

    const rePasswordOnBlur = () => {
        rePasswordBlurHandler(enteredRePassword);
        rePasswordCheckIsNotEmpty(enteredRePassword);
    }

    //check match Password
    let rePasswordIsValid = false;
    if (enteredRePassword === enteredPassword) {
        rePasswordIsValid = true;
    }

    //Generate repassword message error
    let rePasswordErrorText = '';
    if (rePasswordInputHasError) {
        rePasswordErrorText = rePasswordMessageError;
    } else if (!rePasswordIsValid && enteredRePassword.length > 0) {
        rePasswordErrorText = 'Mật khẩu không trùng khớp';
    }

    // Check form is valid
    let formIsValid = false;
    if (enteredFirstNameIsValid &&
        enteredLastNameIsValid &&
        enteredPhoneIsValid &&
        enteredPasswordIsValid &&
        otpIsValid &&
        (enteredRePasswordIsValid && rePasswordIsValid)) {
        formIsValid = true;
    }

    const history = useHistory();

    //Show message Box
    const messageCtx = useContext(MessageContext);

    //Handler send server
    const { sendRequest: signUpRequest, data, status: statusSignUp, error } = useHttp(SignUp);
    const authCtx = useContext(AuthContext);

    useEffect(() => {
        if (statusSignUp === 'completed' && !error) {
            authCtx.onLogin(data.idToken, data.expiresIn, data.customerId);
            authCtx.hideSignUpForm();
            history.replace('/');
            messageCtx.addMessage({
                message: "Đăng ký thành công.",
                className: "success"
            });
        }
    }, [statusSignUp, authCtx]);

    //Confirm submit Register
    const registerSubmit = (e) => {
        e.preventDefault();

        if (formIsValid) {
            signUpRequest({
                Last_Name: enteredLastName,
                First_Name: enteredFirstName,
                Phone: enteredPhone,
                Password: enteredPassword,
                Role: 3 //Customer
            });
        }
    }

    return (
        <Auth title="Đăng Ký Tài Khoản">
            <InputAuth
                label="Tên họ & đệm"
                placeholder="Nhập tên họ & đệm"
                input={{
                    id: "lastname",
                    type: "text"
                }}
                defaultValue={enteredLastName}
                className={`${lastNameInputHasError ? 'invalid' : ''}`}
                onChange={lastNameOnChanged}
                onBlur={lastNameOnBlur}
                error={lastNameInputHasError ? lastNameMessageError : ''}
            />

            <InputAuth
                label="Tên"
                placeholder="Nhập tên"
                input={{
                    id: "firstname",
                    type: "text"
                }}
                defaultValue={enteredFirstName}
                className={`${firstNameInputHasError ? 'invalid' : ''}`}
                onChange={firstNameOnChanged}
                onBlur={firstNameOnBlur}
                error={firstNameInputHasError ? firstNameMessageError : ''}
            />

            <InputAuth
                label="Số điện thoại"
                placeholder="Nhập số điện thoại"
                input={{
                    id: "tel",
                    type: "text"
                }}
                defaultValue={enteredPhone}
                className={`${phoneInputHasError ? 'invalid' : ''}`}
                onChange={phoneOnChanged}
                onBlur={phoneOnBlur}
                error={phoneInputHasError ? phoneMessageError : ''}
                disabled={otpIsValid && enteredPhoneIsValid}
            />

            <OTP
                inputDisabled={otpIsValid || !enteredPhoneIsValid}
                onChange={otpOnChange}
                onClick={onGetOTPSubmit}
                disabled={isSendOTP || !enteredPhoneIsValid}
                error={errorMessageOTP}
            />

            <InputAuth
                label="Mật khẩu"
                placeholder="Nhập mật khẩu"
                input={{
                    id: "password",
                    type: "password"
                }}
                defaultValue={enteredPassword}
                className={`${passwordInputHasError ? 'invalid' : ''}`}
                onChange={passwordOnChanged}
                onBlur={passwordOnBlur}
                error={passwordInputHasError ? passwordMessageError : ''}
            />
            <InputAuth
                label="Nhập lại mật khẩu"
                placeholder="Nhập lại mật khẩu"
                input={{
                    id: "repassword",
                    type: "password"
                }}
                defaultValue={enteredRePassword}
                className={`${rePasswordInputHasError ? 'invalid' : ''}`}
                onChange={rePasswordOnChanged}
                onBlur={rePasswordOnBlur}
                error={rePasswordErrorText}
            />
            {error && <p className={classes.error}>{error}</p>}
            {statusSignUp !== 'pending' && <Button onClick={registerSubmit} disabled={!formIsValid}>Đăng ký</Button>}
            {statusSignUp === 'pending' && <LoadingSpinner />}
        </Auth>
    );
}

export default Register;