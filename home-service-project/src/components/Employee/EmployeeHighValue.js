import React, { Fragment, useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";

import useHttp from "../../hooks/use-http";
import { GetListEmpHighValue } from "../../lib/api";
import AuthContext from "../../store/auth-context";
import EmployeeRandomContext from "../../store/employee-random-context";
import { MessageContext } from "../../store/message-context";
import ServicesContext from "../../store/services-context";
import LoadingSpinner from "../UI/LoadingSpinner";
import Modal from "../UI/Modal";
import classes from "./EmployeeHighValue.module.css";
import EmployeeHighValueItem from "./EmployeeHighValueItem";

const EmployeeHighValue = () => {
  const { isShowEmpHighValue, hideEmployeeHignValue } = useContext(
    EmployeeRandomContext
  );

  const servicesCtx = useContext(ServicesContext);
  const { dataServiceRegister } = servicesCtx.servicesState;
  const { typeServiceId, location } = dataServiceRegister;

  const {
    sendRequest,
    status,
    data: listEmpHighValue,
    error,
  } = useHttp(GetListEmpHighValue, true);

  // Check unauthorized
  const history = useHistory();
  const { token, onLogout } = useContext(AuthContext);
  const { addMessage } = useContext(MessageContext);

  useEffect(() => {
    if (typeServiceId && location && isShowEmpHighValue) {
      sendRequest({
        typeId: typeServiceId,
        lat: location.lat,
        lng: location.lng,
        token,
      });
    }
  }, [sendRequest, typeServiceId, location, isShowEmpHighValue]);

  let listEmpHighValueGenerate;

  if (status === "pending") {
    listEmpHighValueGenerate = <LoadingSpinner />;
  } else if (error) {
    if (error === "Unauthorized") {
      addMessage({ auth: true });
      onLogout();

      setTimeout(() => {
        history.go(0);
      }, 3000);
      return false;
    } else {
      listEmpHighValueGenerate = <span>{error}</span>;
    }
  } else if (
    status === "completed" &&
    (!listEmpHighValue || listEmpHighValue.length === 0)
  ) {
    listEmpHighValueGenerate = <span>Không tìm thấy nhân viên nào</span>;
  } else {
    const posibleCompanyList = listEmpHighValue.map((emp) => (
      <EmployeeHighValueItem
        Emp_Avatar={emp.Emp_Avatar}
        Emp_Name={emp.Emp_Name}
        Rating={emp.Rating}
        Nearest_Distance={emp.Nearest_Distance}
        Seniority={emp.Seniority}
        Company_Name={emp.Company_Name}
        Emp_Id={emp.Emp_Id}
        Company_Id={emp.Company_Id}
        List_Service={emp.List_Service}
      />
    ));

    listEmpHighValueGenerate = (
      <Fragment>
        <div className={classes["wrapper-list"]}>{posibleCompanyList}</div>
      </Fragment>
    );
  }

  return (
    <Fragment>
      {isShowEmpHighValue && (
        <Modal
          customModalClass="employee-high-value"
          customBackdropClass="employee-random"
          onClose={hideEmployeeHignValue}
          title="Danh sách nhân viên chất lượng tốt"
        >
          {listEmpHighValueGenerate}
        </Modal>
      )}
    </Fragment>
  );
};

export default EmployeeHighValue;
