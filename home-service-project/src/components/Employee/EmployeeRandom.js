import { Fragment, useContext, useEffect } from "react";
import { Link } from "react-router-dom";

import classes from "./EmployeeRandom.module.css";
import Modal from "../UI/Modal";
import EmployeeRandomContext from "../../store/employee-random-context";
import ServicesList from "../Services/ServicesList";
import useHttp from "../../hooks/use-http";
import ServicesContext from "../../store/services-context";
import { FindRandomRepair } from "../../lib/api";
import LoadingSpinner from "../UI/LoadingSpinner";
import { useHistory } from "react-router";
import AuthContext from "../../store/auth-context";
import { MessageContext } from "../../store/message-context";

const EmployeeRandom = (props) => {
  const employeeRandomCtx = useContext(EmployeeRandomContext);
  const { isShowEmployeeRandom } = employeeRandomCtx;

  const servicesCtx = useContext(ServicesContext);
  const { dataServiceRegister } = servicesCtx.servicesState;
  const { typeServiceId, location } = dataServiceRegister;

  const {
    sendRequest,
    status,
    data: employeeRandom,
    error,
  } = useHttp(FindRandomRepair, true);

  // Check unauthorized
  const history = useHistory();
  const { token, onLogout } = useContext(AuthContext);
  const { addMessage } = useContext(MessageContext);

  useEffect(() => {
    if (typeServiceId && location && isShowEmployeeRandom) {
      sendRequest({
        typeId: typeServiceId,
        lat: location.lat,
        lng: location.lng,
        token,
      });
    }
  }, [sendRequest, typeServiceId, location, isShowEmployeeRandom]);

  const hideEmployeeRandomHandler = () => {
    employeeRandomCtx.hideEmployeeRandom();
  };

  let result;

  if (status === "pending") {
    result = <LoadingSpinner />;
  } else if (error) {
    if (error === "Unauthorized") {
      setTimeout(() => {
        history.push("/");
        onLogout();
        addMessage({ auth: true });
      }, 100);
      return false;
    } else {
      result = <span>{error}</span>;
    }
  } else if (
    status === "completed" &&
    (employeeRandom.Emp_Id === null ||
      !employeeRandom ||
      Object.keys(employeeRandom).length === 0)
  ) {
    result = <span>Không tìm thấy nhân viên nào</span>;
  } else {
    result = (
      <Fragment>
        <div className={classes["employee-random"]}>
          <div className={classes.image}>
            <img
              src={employeeRandom.Emp_Avatar}
              alt={employeeRandom.Emp_Name}
            />
          </div>

          <div className={classes.info}>
            <div className={classes.item}>
              <span>Họ tên</span>
              <span>{employeeRandom.Emp_Name}</span>
            </div>
            <div className={classes.item}>
              <span>Khoảng cách tới bạn</span>
              <span>{employeeRandom.Nearest_Distance}</span>
            </div>
            <div className={classes.item}>
              <span>Công ty làm việc</span>
              <span>
                <Link target="_blank" to={`/companies/${employeeRandom.Company_Id}`}>
                  {employeeRandom.Company_Name}
                </Link>
              </span>
            </div>
            <div className={classes.item}>
              <span>Kinh nghiệm làm việc</span>
              <span>
                {employeeRandom.Seniority !== ""
                  ? employeeRandom.Seniority
                  : "Chưa cập nhật"}
              </span>
            </div>
            <div className={classes.item}>
              <span>Đánh giá</span>
              <span>
                {employeeRandom.Rating !== null
                  ? `${employeeRandom.Rating.toFixed(1)} / 5 đánh giá`
                  : "Chưa cập nhật"}
              </span>
            </div>
          </div>
        </div>
        <ServicesList
          className="active"
          companyId={employeeRandom.Company_Id}
          empId={employeeRandom.Emp_Id}
          title="Danh Sách Dịch Vụ"
          serviceList={employeeRandom.List_Service}
        />
      </Fragment>
    );
  }

  return (
    <Fragment>
      {isShowEmployeeRandom && (
        <Modal
          customModalClass="emp-random"
          customBackdropClass="employee-random"
          onClose={hideEmployeeRandomHandler}
          title="Nhân Viên Gần Bạn Nhất"
        >
          {result}
        </Modal>
      )}
    </Fragment>
  );
};

export default EmployeeRandom;
