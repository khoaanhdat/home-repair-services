import React, { useCallback, useState } from "react";
import { Link } from "react-router-dom";
import ServicesList from "../Services/ServicesList";
import Button from "../UI/Button";

import classes from './EmployeeHighValueItem.module.css';

const EmployeeHighValueItem = (props) => {
  const [isShowListService, setIsShowListService] = useState(false);

  const empClickHandler = useCallback((e) => {
    e.preventDefault();
    e.stopPropagation();

    setIsShowListService((prevState) => (prevState = !prevState));
  }, []);

  return (
    <div className={classes.link}>
      <div key={props.id} className={classes.item}>
        <img onClick={empClickHandler} src={props.Emp_Avatar ?? "https://picsum.photos/200/300"} alt={props.Emp_Name} />
        <div onClick={empClickHandler} className={classes.info}>
          <div className={classes["info-detail"]}>
            <span>Họ tên: </span>
            <span>
              {props.Emp_Name}
            </span>
          </div>
          <div className={classes["info-detail"]}>
            <span>Đánh giá: </span>
            <span>{props.Rating.toFixed(1)} / 5 đánh giá</span>
          </div>
          <div className={classes["info-detail"]}>
            <span>Khoảng cách tới bạn: </span>
            <span>{props.Nearest_Distance}</span>
          </div>
          <div className={classes["info-detail"]}>
            <span>Kinh nghiệm làm việc: </span>
            <span>{props.Seniority === "" ? "Chưa cập nhật" : props.Seniority}</span>
          </div>
          <div className={classes["info-detail"]}>
            <span>Công ty làm việc: </span>
            <span>{props.Company_Name}</span>
          </div>
        </div>
        <div className={classes.actions}>
          <Link target="_blank" to={`/employees/${props.Emp_Id}`}>
            <Button>Xem chi tiết</Button>
          </Link>
        </div>
      </div>

      <ServicesList
        className={isShowListService ? "active" : ""}
        companyId={props.Company_Id}
        empId={props.Emp_Id}
        title="Danh Sách Dịch Vụ"
        serviceList={props.List_Service}
      />
    </div>
  );
};

export default EmployeeHighValueItem;
