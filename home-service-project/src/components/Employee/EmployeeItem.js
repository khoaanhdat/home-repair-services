import React from 'react';

import classes from './EmployeeItem.module.css';
import BannerPage from '../Layout/BannerPage';
import Card from '../Layout/Card';
import AvatarBox from '../UI/AvatarBox';

const EmployeeItem = (props) => {
    return (
        <div className={classes['emp-item']}>
            <BannerPage bannerImg={props.bannerImg} />
            <Card header="Thông Tin Chi Tiết">
                <div className={classes['emp-info']}>
                    <AvatarBox avatar={props.empAvt} avatarName={props.empName}>
                        <span>{props.empName}</span>
                        <span>Trạng thái : {props.empAvailable === 0 ? 'Vắng mặt' : 'Có mặt'}</span>
                    </AvatarBox>

                    <div className={classes.details}>
                        <span>Đánh giá : {props.empReview}/5</span>
                        <span>Tham gia : {props.empSeniority} tháng</span>
                        <span>Giới tính : {props.empSex === 0 ? 'Nữ' : 'Nam'}</span>
                        <span>Ngày sinh : {props.empBirthDay}</span>
                        <span>Địa chỉ : {props.empAddress}</span>
                    </div>
                </div>

                <div className={classes['emp-des']}>
                    <h2>Giới Thiệu</h2>
                    <div className={classes.des}>
                        {props.empDescriptions}
                    </div>
                </div>
            </Card>
        </div>
    );
}

export default EmployeeItem;