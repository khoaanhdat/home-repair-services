import React from 'react';
import { Link } from 'react-router-dom';

import classes from './EmployeeList.module.css';

const EmployeeList = (props) => {
    return (
        <div className={classes["table-users"]}>
            <div className={classes.header}>Danh Sách Nhân Viên ({props.employeeList.length})</div>
            <table cellSpacing={0}>
                <tbody>
                    <tr>
                        <th></th>
                        <th>Họ tên</th>
                        <th>Đánh giá</th>
                        <th width={230}>Địa chỉ</th>
                        <th>Kinh nghiệm làm việc</th>
                        <th>Trạng thái</th>
                        <th></th>
                    </tr>
                    {
                        props.employeeList.map((item, key) => (
                            <tr key={key}>
                                <td><img src={item.Emp_Avt} alt="" /></td>
                                <td>{item.Emp_Name}</td>
                                <td>{item.Emp_Review_Avg}/5</td>
                                <td>{item.Emp_Address}</td>
                                <td>{item.Seniority}</td>
                                <td>{item.Status}</td>
                                <td>
                                    <Link target="_blank" to={`/employees/${item.Emp_Id}`}>
                                        Xem chi tiết
                                    </Link>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </div>
    );
}

export default EmployeeList;