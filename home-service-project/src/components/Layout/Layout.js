import React, { Fragment, useContext } from 'react';

import classes from './Layout.module.css';
import MainHeader from '../Layout/MainHeader/MainHeader';
import Login from '../Login/Login';
import Register from '../Register/Register';
import AuthContext from '../../store/auth-context';
import MessageBox from '../UI/MessageBox';
import { MessageContext } from '../../store/message-context';

const Layout = (props) => {
    const authCtx = useContext(AuthContext);
    const messageCtx = useContext(MessageContext);
    const {isShow, message, className} = messageCtx;

    return (
        <Fragment>
            <MessageBox isShow={isShow} message={message} className={className}/>
            <MainHeader />
            {authCtx.isLoginFormShown && <Login />}
            {authCtx.isSignUpFormShown && <Register />}
            <div className={classes['page-wrapper']}>
                {props.children}
            </div>
        </Fragment>
    );
};

export default Layout;