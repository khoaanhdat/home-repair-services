import React, { useEffect, useState } from "react";

import Navigation from "./Navigation";
import classes from "./MainHeader.module.css";

const MainHeader = () => {
    const [activeMenuScroll, setActiveMenuScroll] = useState(false);
    useEffect(() => {
        document.addEventListener("scroll", () => {
            const scrollCheck = window.scrollY > 100;
            if (scrollCheck) {
                setActiveMenuScroll(true);
            } else {
                setActiveMenuScroll(false);
            }
        })
    });

    return (
        <header className={`${classes.toolbar} ${activeMenuScroll ? classes['sticky-header'] : ''}`}>
            <Navigation />
        </header>
    )
}

export default MainHeader;