import React, { Fragment, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import AuthContext from '../../../store/auth-context';
import { MessageContext } from '../../../store/message-context';
import classes from './Navigation.module.css';
import 'font-awesome/css/font-awesome.min.css';

const Navigation = () => {
    const authCtx = useContext(AuthContext);
    const messageCtx = useContext(MessageContext);

    const showLoginFormPopup = () => {
        authCtx.showLoginForm();
    }

    const showSignUpFormPopup = () => {
        authCtx.showSignUpForm();
    }

    const logoutHandler = () => {
        authCtx.onLogout();
        messageCtx.addMessage({
            message: "Đăng xuất thành công.",
            className: "success"
        });
    }

    const authNavHtml = () => {
        if (authCtx.isLoggedIn) {
            return (
                <Fragment>
                    <li>
                        <NavLink to="/post-repair" activeClassName={classes.active}>
                            Đăng Thông Báo Sửa Chữa
                        </NavLink>
                    </li>

                    <li className={classes.dropdown}>
                        <i className={`${classes['icon-user']} fa fa-user`} />
                        <ul>
                            <li><Link to="/account">Thông tin cá nhân</Link></li>
                            <li>
                                <Link to='' onClick={logoutHandler}>
                                    Logout
                                </Link>
                            </li>
                        </ul>
                    </li>
                </Fragment>
            );
        } else {
            return (
                <Fragment>
                    <li className="btnSignIn" onClick={showLoginFormPopup}>
                        <Link to=''>Đăng Nhập</Link>
                    </li>
                    <li className="btnSignUp" onClick={showSignUpFormPopup}>
                        <Link to=''>Đăng Ký</Link>
                    </li>
                </Fragment>
            );
        }
    }

    return (
        <nav className={classes.navs}>
            <div className={classes['nav-items']}>
                <ul>
                    <li>
                        <NavLink exact to="/" activeClassName={classes.active}>
                            Trang Chủ
                        </NavLink>
                    </li>

                    {authNavHtml()}
                </ul>
            </div>
        </nav>
    );
}

export default Navigation;