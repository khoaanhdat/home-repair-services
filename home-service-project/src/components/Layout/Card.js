import React from 'react';

import classes from './Card.module.css';

const Card = (props) => {
    let customClass = props.className ? props.className : '';
    let bodyCustomClass = props.bodyNameClass ? props.bodyNameClass : '';

    return (
        <div className={`${classes.card} ${classes[customClass]}`}>
            <div className={classes["card-header"]}>{props.header}</div>
            <div className={`${classes["card-body"]} ${classes["text-primary"]} ${classes[bodyCustomClass]}`}>
                {props.children}
            </div>
        </div>
    );

}

export default Card;