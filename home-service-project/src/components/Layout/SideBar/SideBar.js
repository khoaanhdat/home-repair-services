import { NavLink, useRouteMatch } from "react-router-dom";

import classes from './SideBar.module.css';

const SideBar = (props) => {
    const match = useRouteMatch();

    return (
        <div className={classes.sidebar}>
            <NavLink exact activeClassName={classes.active} to={`${match.path}`}>Thông Tin Của Tôi</NavLink>
            <NavLink activeClassName={classes.active} to={`${match.path}/post-history`}>Danh Sách Bài Đăng</NavLink>
            <NavLink activeClassName={classes.active} to={`${match.path}/register-service`}>Dịch Vụ Đã Đăng Ký</NavLink>
        </div>
    );
}

export default SideBar;