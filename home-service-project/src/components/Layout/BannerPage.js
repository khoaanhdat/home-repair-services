import React from 'react';

import classes from './BannerPage.module.css';

const BannerPage = (props) => {
    return (
        <div className={classes.banner}>
            <img src={props.bannerImg} alt="" />
        </div>
    );
}

export default BannerPage;