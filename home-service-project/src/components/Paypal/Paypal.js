import React, { useCallback, useEffect, useRef } from 'react';
import ReactDOM from "react-dom";

import Modal from '../UI/Modal';

const PayPalButton = window.paypal.Buttons.driver("react", { React, ReactDOM });

const Paypal = ({ price, getOrderInfo, hidePayPal }) => {    
    const createOrder = useCallback((data, actions) => {
        return actions.order.create({
            intent: "CAPTURE",
            purchase_units: [
                {
                    description: "Cool looking table",
                    amount: {
                        currency_code: "USD",
                        value: price,
                    }
                },
            ],
        });
    }, [price]);

    const onApprove = async (data, actions) => {
        const order = await actions.order.capture();
        getOrderInfo(order);
    };

    return (
        <Modal customModalClass='paypal' onClose={hidePayPal}>
            <PayPalButton
                createOrder={(data, actions) => createOrder(data, actions)}
                onApprove={(data, actions) => onApprove(data, actions)}
            />
        </Modal>
    );
}

export default Paypal;
