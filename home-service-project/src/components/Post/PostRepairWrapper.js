import React, { useContext, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';

import classes from './PostRepairWrapper.module.css';
import SearchAddress from '../../components/Home/SearchAddress';
import BannerPage from '../../components/Layout/BannerPage';
import Card from '../Layout/Card';
import TypeServicesList from '../Services/TypeServicesList';
import UnitServices from '../Services/UnitServices';
import Calendar from '../Calendar/Calendar';
import Button from '../UI/Button';
import ServicesContext from '../../store/services-context';
import InputAuth from '../UI/InputAuth';
import { PostRepairNotice } from '../../lib/api';
import useHttp from '../../hooks/use-http';
import { MessageContext } from '../../store/message-context';
import AuthContext from '../../store/auth-context';

const PostRepairWarraper = (props) => {
    const history = useHistory();

    const serviceCtx = useContext(ServicesContext);

    const inputDescriptions = useRef('');

    const { sendRequest, status, error } = useHttp(PostRepairNotice);

    // Getr token
    const { token, onLogout, customerId } = useContext(AuthContext);

    const postRepairHandler = (event) => {
        event.preventDefault();
        const { dataServiceRegister } = serviceCtx.servicesState;

        const { typeServiceId: typeId, address, location, bookTime, quantity } = dataServiceRegister;

        sendRequest({
            dataPost: {
                Customer_Id: customerId,
                Type_Id: typeId,
                Address: address,
                Location: `"[${location.lat}, ${location.lng}]"`,
                Description: inputDescriptions.current.value,
                Book_Date: bookTime,
                Quantity: quantity
            },
            token
        });
    }

    //Add MessageBox 
    const { addMessage } = useContext(MessageContext);

    useEffect(() => {
        const timeRedirect = setTimeout(() => {
            if (status === 'completed' && !error) {
                history.push('/account/post-history');
                addMessage({
                    message: "Đăng thông báo sửa chữa thành công.",
                    className: "success"
                });
            }
        }, 500);

        if (error) {
            if (error === 'Unauthorized') {
                setTimeout(() => {
                    history.push('/');
                    onLogout();
                    addMessage({ auth: true });
                }, 100);
            } else {
                addMessage({
                    message: error,
                    className: "error"
                });
            }
        }

        return () => {
            clearTimeout(timeRedirect);
        }
    }, [status])

    const addBookTimeHandler = (time) => {
        serviceCtx.addBookTime(time);
    }

    return (
        <div className={`${classes['post-repair']} ${status === 'pending' ? classes.disabled : ''}`}>
            <BannerPage bannerImg="https://i.picsum.photos/id/287/4288/2848.jpg?hmac=f_-W7-bOKUxLoH9uOz4Hwk9D8zYTgzbHX7i_vY_ljug" />
            <Card header="Thông Báo Sửa Chữa">
                <form onSubmit={postRepairHandler} className={classes.form}>
                    <SearchAddress label="Nhập địa chỉ của bạn..." className="post-repair" customButtonClass="post-repair" />
                    <div className={classes['services']}>
                        <h1>Dịch vụ bạn muốn sửa chữa</h1>
                        <TypeServicesList />
                        <UnitServices />
                    </div>
                    <div className={classes['services']}>
                        <h1>Mô tả về dịch vụ bạn muốn sử dụng</h1>
                        <InputAuth
                            ref={inputDescriptions}
                            input={{
                                id: "description"
                            }}
                            placeholder="Ví dụ: Tối muốn dọn dẹp phòng bếp..."
                        />
                    </div>
                    <div className={classes['time-book']}>
                        <h1>Thời gian bạn muốn</h1>
                        <Calendar addBookTime={addBookTimeHandler} minDate={new Date()} />
                    </div>
                    <div className={classes.actions}>
                        <Button type="submit">Xác Nhận</Button>
                    </div>
                </form>
            </Card>
        </div>
    );
}

export default PostRepairWarraper;