import { Fragment, useContext, useEffect } from 'react';

import useHttp from '../../hooks/use-http';
import classes from './PostListHistory.module.css';
import Button from '../UI/Button';
import { GetListPostByIdCustomer } from '../../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import usePagination from '../../hooks/use-pagination';
import { Pagination } from '../UI/Pagination';
import { useHistory } from 'react-router';
import AuthContext from '../../store/auth-context';
import { MessageContext } from '../../store/message-context';

const PostListHistory = (props) => {
    const history = useHistory();
    const { token, onLogout } = useContext(AuthContext);
    const { addMessage } = useContext(MessageContext);

    const { sendRequest, status, data: listPost, error } = useHttp(
        GetListPostByIdCustomer,
        true
    );

    const perPage = 3;
    const {
        currentList: currentPosts,
        currentPage,
        paginateHandler
    } = usePagination(1, perPage, listPost);

    useEffect(() => {
        const send = setTimeout(() => {
            sendRequest({
                customerId: props.customerId,
                token
            });
        }, 500);
        return () => {
            clearTimeout(send);
        }
    }, [sendRequest, props.customerId]);

    if (status === 'pending') {
        return <LoadingSpinner />
    }

    if (error) {
        if (error === 'Unauthorized') {
            onLogout();
            history.push('/');
            addMessage({auth: true});
            return;
        }
        return <span>{error}</span>
    }

    if (status === 'completed' && (!listPost || listPost.length === 0)) {
        return <span>Bạn chưa đăng thông báo sửa chữa nào.</span>
    }

    if (!currentPosts || currentPosts.length === 0) {
        return <LoadingSpinner />
    }

    const paginateClick = (page) => {
        paginateHandler(page);
    }

    const generateListPost = () => {
        const listPostHtml = currentPosts.map((item, key) => {
            return (
                <div key={key} className={`${classes['post-item']} ${key % 2 !== 0 ? classes['reverse'] : ''}`}>
                    <div className={classes.info}>
                        <div className={classes.row}>
                            <span>Loại dịch vụ</span>
                            <span>{item.Type_Name}</span>
                        </div>
                        <div className={classes.row}>
                            <span>Địa chỉ đăng ký</span>
                            <span>{item.Address}</span>
                        </div>
                        <div className={classes.row}>
                            <span>Mô tả yêu cầu</span>
                            <span>{item.Description}</span>
                        </div>
                        <div className={classes.row}>
                            <span>Ngày yêu cầu sửa</span>
                            <span>{item.Book_Date}</span>
                        </div>
                        <div className={classes.row}>
                            <span>Định lượng</span>
                            <span>{item.Quantity}</span>
                        </div>
                        <div className={classes.row}>
                            <span>Công ty nhận</span>
                            <span>
                                {item.Company_Name ? <a href={`/companies/${item.Company_Id}`}>{item.Company_Name}</a> : 'Chưa có'}
                            </span>
                        </div>
                        <div className={classes.row}>
                            <span>Nhân viên sửa</span>
                            <span>
                                {item.Emp_Name ? <a href={`/employees/${item.Emp_Id}`}>{item.Emp_Name}</a> : 'Chưa có'}
                            </span>
                        </div>
                        <div className={classes.row}>
                            <span>Trạng thái hoàn thành</span>
                            <span>{item.Status ? 'Đã hoàn thành' : 'Chưa hoàn thành'}</span>
                        </div>
                        <div className={classes.row}>
                            <span>Ngày công ty nhận</span>
                            <span>{item.Approve_Date ?? 'Chưa có'}</span>
                        </div>
                        <div className={classes.row}>
                            <span>Dịch vụ được chọn</span>
                            <span>{item.Service_Name ?? 'Chưa có'}</span>
                        </div>
                        <div className={classes.row}>
                            <span>Tổng tiền</span>
                            <span>{item.Total_Price ? `${item.Total_Price} đ` : 'Chưa có'}</span>
                        </div>

                        {item.Payment !== null && (
                            <div className={classes.row}>
                                <span>Thanh Toán</span>
                                <span>{item.Payment ? 'Đã thanh toán' : <Button>Thanh Toán</Button>}</span>
                            </div>
                        )}
                    </div>
                    <div className={classes['image-type']}>
                        <img src={item.Type_Image} alt={item.Type_Name} />
                    </div>
                </div>
            );
        });
        return (
            <Fragment>
                <div>
                    {listPostHtml}
                </div>
                <Pagination currentPage={currentPage} listPerPage={perPage} totalList={listPost.length} paginateClick={paginateClick} />
            </Fragment>
        );
    }



    return (
        <Fragment>
            { generateListPost()}
        </Fragment>
    );
}

export default PostListHistory;