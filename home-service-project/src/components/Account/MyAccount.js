import { useEffect, useContext } from 'react';
import { Route, useHistory, useRouteMatch } from 'react-router-dom';

import classes from './MyAccount.module.css';
import PostListHistory from '../Account/PostListHistory';
import DashBoard from '../Account/DashBoard';
import SideBar from "../Layout/SideBar/SideBar";
import RegisteredService from '../Services/RegisteredService';
import { GetCustomerById } from '../../lib/api';
import useHttp from '../../hooks/use-http';
import LoadingSpinner from '../UI/LoadingSpinner';
import AuthContext from '../../store/auth-context';
import { MessageContext } from '../../store/message-context';

const MyAccount = (props) => {
    const match = useRouteMatch();
    const history = useHistory();
    const { token, onLogout, customerId } = useContext(AuthContext);
    const { addMessage } = useContext(MessageContext);

    const { sendRequest, status, data: customerInfo, error } = useHttp(
        GetCustomerById,
        true
    );

    useEffect(() => {
        const send = setTimeout(() => {
            sendRequest({ customerId, token });
        }, 500);
        return () => {
            clearTimeout(send);
        }
    }, [sendRequest, customerId]);

    let dashboard;

    if (status === 'pending') {
        dashboard = <LoadingSpinner />;
    } else if (error) {
        if (error === 'Unauthorized') {
            setTimeout(() => {
                history.push('/');
                onLogout();
                addMessage({ auth: true });
            }, 100);
        } else {
            dashboard = <span>{error}</span>
        }
    } else if (status === 'complete' && (!customerInfo || Object.keys(customerInfo).length === 0)) {
        dashboard = <span>Không tìm thấy dữ liệu</span>
    } else {
        dashboard = <DashBoard customerInfo={customerInfo} />
    }

    return (
        <div className={classes['main-content']}>
            <Route path='/account' exact>
                <div className={classes.title}>
                    <h1>Thông Tin Của Tôi</h1>
                </div>
                <div className={classes.content}>
                    {dashboard}
                </div>
            </Route>
            <Route path={`${match.path}/post-history`}>
                <div className={classes.title}>
                    <h1>Danh Sách Bài Đăng</h1>
                </div>
                <div className={classes.content}>
                    <PostListHistory customerId={customerId} />
                </div>
            </Route>
            <Route path={`${match.path}/register-service`}>
                <div className={classes.title}>
                    <h1>Dịch Vụ Đã Đăng Ký</h1>
                </div>
                <div className={classes.content}>
                    <RegisteredService customerId={customerId} />
                </div>
            </Route>
            <SideBar />
        </div>
    );
}

export default MyAccount;