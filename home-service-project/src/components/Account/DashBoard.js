import { Fragment, useContext, useEffect, useRef, useState } from 'react';

import AvatarBox from '../UI/AvatarBox';
import classes from './Dashboard.module.css';
import Button from '../UI/Button';
import useInput from '../../hooks/use-input';
import useHttp from '../../hooks/use-http';
import { UpdateCustomer, UploadFile } from '../../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import Calendar from '../Calendar/Calendar';
import { MessageContext } from '../../store/message-context';
import { useHistory } from 'react-router';
import AuthContext from '../../store/auth-context';

const DashBoard = (props) => {
    const { customerId } = useContext(AuthContext);

    const [isUpdate, setIsUpdate] = useState(false);
    const [formIsTouched, setFormIsTouched] = useState(false);

    const { sendRequest, status, error } = useHttp(UpdateCustomer);

    const { addMessage } = useContext(MessageContext);

    // Check unauthorized
    const history = useHistory();
    const { token, onLogout } = useContext(AuthContext);

    useEffect(() => {
        if (status === 'completed' && !error) {
            setIsUpdate(false);
            addMessage({
                message: "Cập nhật thông tin thành công.",
                className: "success"
            });
        } else {
            if (error) {
                if (error === 'Unauthorized') {
                    setTimeout(() => {
                        history.push('/');
                        onLogout();
                        addMessage({ auth: true });
                    }, 100);
                } else {
                    addMessage({
                        message: error,
                        className: "error"
                    });
                }
            }
        }


    }, [status]);

    const { First_Name, Last_Name, Phone, Password, BirthDay, Avatar } = props.customerInfo;

    const convertedBirthDay = new Date(BirthDay.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3"));

    const formTouchedHandler = () => {
        setFormIsTouched(true);
    }

    // First Name
    const {
        value: enteredFirstName,
        isValid: enteredFirstNameIsValid,
        hasError: firstNameInputHasError,
        messageError: firstNameMessageError,
        valueChanged: firstNameChangeHandler,
        valueBlur: firstNameBlurHandler,
        checkIsNotEmpty: firstNameCheckIsNotEmpty
    } = useInput(First_Name);

    const firstNameOnChanged = (e) => {
        firstNameChangeHandler(e);
        firstNameCheckIsNotEmpty(e.target.value);
    }

    const firstNameOnBlur = () => {
        firstNameBlurHandler();
        firstNameCheckIsNotEmpty(enteredFirstName);
    }

    // Last Name
    const {
        value: enteredLastName,
        isValid: enteredLastNameIsValid,
        hasError: lastNameInputHasError,
        messageError: lastNameMessageError,
        valueChanged: lastNameChangeHandler,
        valueBlur: lastNameBlurHandler,
        checkIsNotEmpty: lastNameCheckIsNotEmpty,
    } = useInput(Last_Name);

    const lastNameOnChanged = (e) => {
        lastNameChangeHandler(e);
        lastNameCheckIsNotEmpty(e.target.value);
    }

    const lastNameOnBlur = () => {
        lastNameBlurHandler();
        lastNameCheckIsNotEmpty(enteredLastName);
    }

    // Phone Number
    const {
        value: enteredPhone,
        isValid: enteredPhoneIsValid,
        hasError: phoneInputHasError,
        messageError: phoneMessageError,
        valueChanged: phoneChangeHandler,
        valueBlur: phoneBlurHandler,
        checkIsNotEmpty: phoneCheckIsNotEmpty,
        checkIsNumber: phoneCheckIsNumber,
        checkIsPhoneNumber
    } = useInput(Phone.replaceAll('.', ""));

    const phoneOnChanged = (event) => {
        phoneChangeHandler(event);
        phoneCheckIsNotEmpty(event.target.value);
        phoneCheckIsNumber(event.target.value);
        checkIsPhoneNumber(event.target.value);
    }

    const phoneOnBlur = () => {
        phoneBlurHandler();
        phoneCheckIsNotEmpty(enteredPhone);
        phoneCheckIsNumber(enteredPhone);
        checkIsPhoneNumber(enteredPhone);
    }

    // Password
    const {
        value: enteredPassword,
        isValid: enteredPasswordIsValid,
        hasError: passwordInputHasError,
        messageError: passwordMessageError,
        valueChanged: passwordChangeHandler,
        valueBlur: passwordBlurHandler,
        checkIsNotEmpty: passwordCheckIsNotEmpty,
    } = useInput(Password);

    const passwordOnChanged = (event) => {
        passwordChangeHandler(event);
        passwordCheckIsNotEmpty(event.target.value);
    }

    const passwordOnBlur = () => {
        passwordBlurHandler(enteredPassword);
        passwordCheckIsNotEmpty(enteredPassword);
    }

    // Repeat Password
    const {
        value: enteredRePassword,
        isValid: enteredRePasswordIsValid,
        hasError: rePasswordInputHasError,
        messageError: rePasswordMessageError,
        valueChanged: rePasswordChangeHandler,
        valueBlur: rePasswordBlurHandler,
        checkIsNotEmpty: rePasswordCheckIsNotEmpty,
    } = useInput(Password);

    const rePasswordOnChanged = (event) => {
        rePasswordChangeHandler(event);
        rePasswordCheckIsNotEmpty(event.target.value);
    }

    const rePasswordOnBlur = () => {
        rePasswordBlurHandler(enteredRePassword);
        rePasswordCheckIsNotEmpty(enteredRePassword);
    }

    //check match Password
    let rePasswordIsValid = false;
    if (enteredRePassword === enteredPassword) {
        rePasswordIsValid = true;
    }

    // Check form is valid
    let formIsValid = false;
    if (enteredFirstNameIsValid &&
        enteredLastNameIsValid &&
        enteredPhoneIsValid &&
        enteredPasswordIsValid &&
        (enteredRePasswordIsValid && rePasswordIsValid)) {
        formIsValid = true;
    }

    // Handle upload file image
    const [imagePreview, setImagePreview] = useState(Avatar);
    const [selectedImage, setSelectedImage] = useState(null);
    const fileInputRef = useRef();

    const onImageChange = event => {
        if (event.target.files && event.target.files[0]) {
            let img = event.target.files[0];
            setImagePreview(prevState => prevState = URL.createObjectURL(img));
            setSelectedImage(img);
            setFormIsTouched(true);
        }
    };

    const saveInfoHandler = async (e) => {
        e.preventDefault();

        if (formIsValid) {
            let imageUrl;
            if (selectedImage === null) {
                imageUrl = Avatar;
            } else {
                const fd = new FormData();
                fd.append('file', selectedImage, selectedImage.name);
        
                const response = await fetch('https://home-repair-api.herokuapp.com/uploadFile', {
                    method: 'POST',
                    body: fd
                });
        
                const data = await response.json();
                imageUrl = data.fileDownloadUri;
            }
            sendRequest({
                customerUpdateData: {
                    Customer_Id: customerId,
                    Last_Name: enteredLastName,
                    First_Name: enteredFirstName,
                    Phone: enteredPhone,
                    Password: enteredPassword,
                    BirthDay: BirthDay,
                    Avatar: imageUrl
                },
                token
            });


        }
    }

    const updateClickHandler = () => {
        setIsUpdate((prevState) => prevState = !prevState);
        setFormIsTouched(false);
    }

    const pickImageHandler = (e) => {
        e.preventDefault();
        fileInputRef.current.click();
    }

    return (
        <form onSubmit={saveInfoHandler} className={classes.dashboard}>
            <AvatarBox customClass="account" avatar={imagePreview} avatarName={enteredFirstName} isUpdate={isUpdate}>
                {isUpdate &&
                    <Fragment>
                        <input type="file"
                            name="myImage"
                            label="Chọn hình ảnh"
                            className={classes.name}
                            required={false}   
                            onChange={onImageChange}
                            ref={fileInputRef}
                            style={{ 'display': 'none' }}
                        />
                        <Button className="pick-image" onClick={pickImageHandler}>Chọn ảnh</Button>

                        <input
                            type="text"
                            placeholder="Nhập họ & đệm"
                            value={enteredLastName}
                            className={`${lastNameInputHasError ? classes.invalid : ''} ${classes.name}`}
                            onChange={lastNameOnChanged}
                            onBlur={lastNameOnBlur}
                            onFocus={formTouchedHandler}
                        />
                        {lastNameInputHasError && <p className={classes.error}>{lastNameMessageError}</p>}

                        <input
                            type="text"
                            placeholder="Nhập tên"
                            value={enteredFirstName}
                            className={`${firstNameInputHasError ? classes.invalid : ''} ${classes.name}`}
                            onChange={firstNameOnChanged}
                            onBlur={firstNameOnBlur}
                            onFocus={formTouchedHandler}
                        />
                        {firstNameInputHasError && <p className={classes.error}>{firstNameMessageError}</p>}
                    </Fragment>
                }
                {!isUpdate && <span>{`${enteredLastName} ${enteredFirstName}`}</span>}
            </AvatarBox>

            <div className={classes.detail}>
                <div className={classes.item}>
                    <span>Số Điện Thoại</span>
                    <input
                        type="text"
                        value={isUpdate ? enteredPhone.replaceAll('.', "") : enteredPhone}
                        disabled={!isUpdate}
                        className={phoneInputHasError ? classes.invalid : ''}
                        onChange={phoneOnChanged}
                        onBlur={phoneOnBlur}
                        onFocus={formTouchedHandler}
                    />
                    {phoneInputHasError && <p className={classes.error}>{phoneMessageError}</p>}
                </div>
                <div className={classes.item}>
                    <span>Mật Khẩu</span>
                    <input
                        type="password"
                        defaultValue={Password}
                        className={passwordInputHasError ? classes.invalid : ''}
                        onChange={passwordOnChanged}
                        onBlur={passwordOnBlur}
                        disabled={!isUpdate}
                        onFocus={formTouchedHandler}
                    />
                    {passwordInputHasError && <p className={classes.error}>{passwordMessageError}</p>}
                </div>
                {isUpdate &&
                    <div className={classes.item}>
                        <span>Nhập lại mật khẩu</span>
                        <input
                            type="password"
                            defaultValue={enteredPassword}
                            className={rePasswordInputHasError ? classes.invalid : ''}
                            onChange={rePasswordOnChanged}
                            onBlur={rePasswordOnBlur}
                            onFocus={formTouchedHandler}
                        />
                        {rePasswordInputHasError && <p className={classes.error}>{rePasswordMessageError}</p>}
                        {!rePasswordIsValid && enteredRePassword.length > 0 && <p className={classes.error}>Mật khẩu không trùng khớp</p>}
                    </div>
                }
                <div className={`${classes.item} ${classes.calendar}`}>
                    <span>Ngày Sinh</span>
                    <Calendar initDate={convertedBirthDay} disabled={!isUpdate} onFocus={formTouchedHandler} />
                </div>
            </div>

            <div className={classes.actions}>
                {isUpdate && status !== 'pending' && <Button type="submit" disabled={!(formIsValid && formIsTouched)}>Lưu</Button>}
                {!isUpdate && <Button onClick={updateClickHandler}>Sửa Thông Tin</Button>}
                {isUpdate && formIsValid && formIsTouched && status === 'pending' && <LoadingSpinner />}
            </div>
        </form >
    );
}

export default DashBoard;