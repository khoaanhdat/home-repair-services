import React from 'react';

import classes from './Pagination.module.css';

export const Pagination = ({ listPerPage, totalList, paginateClick, currentPage }) => {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalList / listPerPage); i++) {
        pageNumbers.push(i);
    }
    return (
        <div className={classes.pagination}>
            {pageNumbers.map(number => (
                <a key={number} onClick={() => paginateClick(number)} href="#" className={`${currentPage === number ? classes.active : ''}`}>
                    {number}
                </a>
            ))}
        </div>
    )
}
