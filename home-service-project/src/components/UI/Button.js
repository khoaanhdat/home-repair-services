import React from 'react';

import classes from './Button.module.css';

const Button = (props) => {
    let classBtn = props.className ? props.className : '';
    

    return (
        <button
            id={props.id}
            type={props.type || 'button'}
            className={`${classes.button} ${classBtn ? classes[classBtn] : ''}`}
            onClick={props.onClick}
            disabled={props.disabled}
        >
            {props.children}
        </button>
    );
};

export default Button;
