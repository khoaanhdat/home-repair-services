import React from 'react';

import classes from './Input.module.css';

const Input = React.forwardRef((props, ref) => {
    return (
        <div className={`${classes['form__group']} ${props.className ? classes[props.className] : ''}`}>
            <input
                className={classes['form__field']}
                {...props.input}
                {...props.inputProps}
                required={props.required}
                ref={ref}
            />
            <label className={classes['form__label']} htmlFor={props.input.id}>{props.label}</label>
        </div>
    );
});

export default Input;