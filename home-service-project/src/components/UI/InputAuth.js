import React, { forwardRef } from 'react';

import classes from './InputAuth.module.css';

const InputAuth = forwardRef((props, ref) => {
    let customClass = props.className ? props.className : '';

    return (
        <div className={`${classes['input-group']} ${classes[customClass]}`}>
            <label htmlFor={props.input.id}>{props.label}</label>
            <input
                ref={ref}
                defaultValue={props.defaultValue}
                onChange={props.onChange}
                {...props.input}
                placeholder={props.placeholder}
                required={props.required}
                value={props.value}
                onFocus={props.onFocus}
                onBlur={props.onBlur}
                disabled={props.disabled}
                
            />
            {props.error && <p className={classes.error}>{props.error}</p>}
        </div>
    );
});

export default InputAuth;