import classes from './Rate.module.css';

const Rate = (props) => {
    return (
        <div className={classes.rate}>
            <input onChange={props.onChange} type="radio" id="star5" name="rate" value="5" />
            <label htmlFor="star5" title="Rất hài lòng">5 stars</label>
            <input onChange={props.onChange} type="radio" id="star4" name="rate" value="4" />
            <label htmlFor="star4" title="Hài lòng"></label>
            <input onChange={props.onChange} type="radio" id="star3" name="rate" value="3" />
            <label htmlFor="star3" title="Tạm được"></label>
            <input onChange={props.onChange} type="radio" id="star2" name="rate" value="2" />
            <label htmlFor="star2" title="Không hài lòng"></label>
            <input onChange={props.onChange} type="radio" id="star1" name="rate" value="1" />
            <label htmlFor="star1" title="Rất không hài lòng"></label>
        </div>
    );
}

export default Rate;