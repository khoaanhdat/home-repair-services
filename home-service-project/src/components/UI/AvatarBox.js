import { Fragment } from 'react';
import classes from './AvatarBox.module.css';

const AvatarBox = (props) => {
    return (
        <div className={`${classes.avatar} ${props.customClass ? classes[props.customClass] : ''}`}>
            {!props.isUpdate && <img src={props.avatar} alt={props.avatarName} />}
            {props.isUpdate && (
                <Fragment>
                    <img src={props.avatar} />                    
                </Fragment>
            )}
            <div className={classes.name}>
                {props.children}
            </div>
        </div>
    );
}

export default AvatarBox;