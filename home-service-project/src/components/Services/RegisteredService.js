import { Fragment, useContext, useEffect, useRef, useState } from 'react';
import { Link, Route, useHistory, useRouteMatch } from 'react-router-dom';

import classes from './RegisteredService.module.css';
import Button from '../UI/Button';
import Rate from '../UI/Rate';
import useHttp from '../../hooks/use-http';
import { GetListRegisterServiceByIdCustomer, Review, PaymentOnline } from '../../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import Paypal from '../Paypal/Paypal';
import { MessageContext } from '../../store/message-context';
import { Pagination } from '../UI/Pagination';
import usePagination from '../../hooks/use-pagination';
import AuthContext from '../../store/auth-context';

const RegisteredService = (props) => {
    document.body.classList.remove('modal-open');
    const [isPayPalShow, setIsPayPalShow] = useState(false);
    const [processIsPayment, setProcessIsPayment] = useState();
    const [totalPrice, setTotalPrice] = useState(0);

    const { token, onLogout } = useContext(AuthContext);
    const match = useRouteMatch();
    let history = useHistory();

    const [empIdComment, setEmpIdComment] = useState(null);
    const [companyIdComment, setCompanyIdComment] = useState(null);
    const [rating, setRating] = useState();
    const [placeholderReview, setPlaceholderReview] = useState('');

    const contentRef = useRef('');

    const { sendRequest: reviewRequest, status: reviewStatus, error: errorReview } = useHttp(Review);

    const reviewCompanyClick = (e, companyId) => {
        setPlaceholderReview('Viết đánh giá công ty');
        setCompanyIdComment(companyId);
        setEmpIdComment(null);
    }

    const reviewEmployeeClick = (e, empId) => {
        setPlaceholderReview('Viết đánh giá thợ sửa chữa');
        setEmpIdComment(empId);
        setCompanyIdComment(null);
    }

    const rateClickHandler = (e) => {
        setRating(e.target.value);
    }

    const addReviewHandler = (e) => {
        e.preventDefault();

        reviewRequest({
            reviewData: {
                Customer_Id: props.customerId,
                Company_Id: companyIdComment,
                Emp_Id: empIdComment,
                Rating: rating,
                Content: contentRef.current.value
            },
            token
        });
    }

    const {
        sendRequest: getListRegisterService,
        status,
        data: listRegisteredService,
        error: errorGetListRegisterService
    } = useHttp(
        GetListRegisterServiceByIdCustomer,
        true
    );

    useEffect(() => {
        const send = setTimeout(() => {
            getListRegisterService({
                customerId: props.customerId,
                token
            });
        }, 500);
        return () => {
            clearTimeout(send);
        }
    }, [getListRegisterService, props.customerId, token]);

    //Add MessageBox
    const { addMessage } = useContext(MessageContext);

    useEffect(() => {
        if (reviewStatus === 'completed' && !errorReview) {
            const timer = setTimeout(() => {
                history.push('/account/register-service');
                setCompanyIdComment(null);
                setEmpIdComment(null);
                getListRegisterService({
                    customerId: props.customerId,
                    token
                });

                addMessage({
                    message: "Đánh giá của bạn đã được ghi nhận.",
                    className: "success"
                });
            }, 700);

            return () => {
                clearTimeout(timer);
            }
        }

        if (errorReview) {
            if (errorReview === 'Unauthorized') {
                setTimeout(() => {
                    history.push('/');
                    onLogout();
                    addMessage({ auth: true });
                }, 100);
            } else {
                addMessage({
                    message: errorReview,
                    className: "error"
                });
            }
        }
    }, [getListRegisterService, history, reviewStatus, addMessage, props.customerId, errorReview, onLogout]);

    const { sendRequest: paymentOnline, data, status: statusPaymentOnline, error: errorPaymentOnline } = useHttp(PaymentOnline);

    //After payment success
    useEffect(() => {
        if (statusPaymentOnline === 'completed' && !errorPaymentOnline) {
            setTimeout(() => {
                setIsPayPalShow(false);
                getListRegisterService({
                    customerId: props.customerId,
                    token
                });
                addMessage({
                    message: "Thanh toán thành công.",
                    className: "success"
                });
            }, 500);

        }

        if (errorPaymentOnline) {
            if (errorPaymentOnline === 'Unauthorized') {
                setTimeout(() => {
                    history.push('/');
                    onLogout();
                    addMessage({ auth: true });
                }, 100);
            } else {
                addMessage({
                    message: errorPaymentOnline,
                    className: "error"
                });
            }
        }
    }, [getListRegisterService, statusPaymentOnline, addMessage, props.customerId, errorPaymentOnline]);

    const paymentHandler = (e, processId, totalPrice) => {
        e.preventDefault();
        e.stopPropagation();

        setIsPayPalShow(true);
        setProcessIsPayment(processId);

        //Convert VND to USD
        setTotalPrice((totalPrice / 23000).toFixed(2));
    }

    const getOrderInfo = (order) => {
        if (order.status === 'COMPLETED') {
            //Send request
            paymentOnline({
                processId: { Process_Id: processIsPayment },
                token
            });
        }
    }

    const hidePayPalHandler = () => {
        setIsPayPalShow(false);
    }

    //Add pagination
    const perPage = 3;
    const {
        currentList: currentRegisteredServices,
        currentPage,
        paginateHandler
    } = usePagination(1, perPage, listRegisteredService);

    const paginateClick = (page) => {
        paginateHandler(page);
    }

    if (status === 'pending') {
        return <LoadingSpinner />
    }

    if (errorGetListRegisterService) {
        if (errorGetListRegisterService === 'Unauthorized') {
            setTimeout(() => {
                history.push('/');
                onLogout();
                addMessage({ auth: true });
            }, 100);
            return false;
        } else {
            return <span>{errorGetListRegisterService}</span>
        }
    }

    if (status === 'completed' && (!listRegisteredService || listRegisteredService.length === 0)) {
        return <span>Bạn chưa đăng ký dịch vụ nào.</span>
    }

    if (!currentRegisteredServices || currentRegisteredServices.length === 0) {
        return <LoadingSpinner />
    }

    const listRegisteredServiceHtml = currentRegisteredServices.map((item, key) => {
        return (
            <div key={key} className={`${classes['register-service-item']} ${key % 2 !== 0 ? classes['reverse'] : ''}`}>
                <div className={classes.info}>
                    <div className={classes.row}>
                        <span>Dịch vụ đăng ký</span>
                        <span>{item.Service_Name}</span>
                    </div>
                    <div className={classes.row}>
                        <span>Địa chỉ đăng ký</span>
                        <span>{item.Address}</span>
                    </div>
                    <div className={classes.row}>
                        <span>Ngày đăng ký</span>
                        <span>{item.Date_Booked}</span>
                    </div>
                    <div className={classes.row}>
                        <span>Định lượng</span>
                        <span>{item.Quantity}</span>
                    </div>
                    <div className={classes.row}>
                        <span>Công ty đăng ký</span>
                        <span>
                            {<a href={`/companies/${item.Company_Id}`}>{item.Company_Name}</a>}
                        </span>
                    </div>
                    <div className={classes.row}>
                        <span>Nhân viên sửa</span>
                        <span>
                            {<a href={`/employees/${item.Emp_Id}`}>{item.Emp_Name}</a>}
                        </span>
                    </div>
                    <div className={classes.row}>
                        <span>Trạng thái hoàn thành</span>
                        <span>{item.Status ? 'Đã hoàn thành' : 'Chưa hoàn thành'}</span>
                    </div>
                    <div className={classes.row}>
                        <span>Tổng tiền</span>
                        <span>{item.Total_Price ? `${item.Total_Price} đ` : 'Chưa có'}</span>
                    </div>

                    {item.Payment !== null && (
                        <div className={classes.row}>
                            <span>Thanh Toán</span>
                            {
                                item.Payment ? <span>Đã thanh toán</span> :
                                    <Button onClick={(e) => paymentHandler(e, item.Process_Id, item.Total_Price)}>Thanh Toán</Button>
                            }
                        </div>
                    )}

                    {item.Payment === 1 && (
                        <Fragment>
                            <div className={classes.row}>
                                {item.Company_Review === null &&
                                    <Button id={`company-${item.Process_Id}`} onClick={(e) => reviewCompanyClick(e, item.Company_Id)} className="success">
                                        <Link to={`${match.path}/comment/${item.Process_Id}`}>Đánh giá công ty</Link>
                                    </Button>}

                                {item.Emp_Review === null &&
                                    <Button id={`emp-${item.Process_Id}`} onClick={(e) => reviewEmployeeClick(e, item.Emp_Id)} className="success">
                                        <Link to={`${match.path}/comment/${item.Process_Id}`}>Đánh giá thợ sửa chữa</Link>
                                    </Button>}
                            </div>

                            <Route path={`${match.path}/comment/${item.Process_Id}`}>
                                <form onSubmit={addReviewHandler} className={classes['add-comment']}>
                                    <Rate onChange={rateClickHandler} />
                                    <textarea ref={contentRef} placeholder={placeholderReview} id='comment' rows='5' ></textarea>
                                    {reviewStatus === 'pending' && <LoadingSpinner />}
                                    <Button type="submit">Lưu Đánh Giá</Button>
                                </form>
                            </Route>
                        </Fragment>
                    )}
                </div>
                <div className={classes['image-type']}>
                    <img src={item.Type_Image} alt={item.Type_Name} />
                </div>
            </div>
        );
    });

    return (
        <div className={classes['post-list-history']}>
            {
                isPayPalShow &&
                <Paypal price={totalPrice} getOrderInfo={getOrderInfo} hidePayPal={hidePayPalHandler} />
            }
            {listRegisteredServiceHtml}
            <Pagination currentPage={currentPage} listPerPage={perPage} totalList={listRegisteredService.length} paginateClick={paginateClick} />
        </div>
    );
}

export default RegisteredService;