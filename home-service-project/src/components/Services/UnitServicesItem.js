import React, { useContext } from 'react';

import classes from './UnitServicesItem.module.css';
import InputAuth from '../UI/InputAuth';
import ServicesContext from '../../store/services-context';

const AdditionalServicesItem = (props) => {
    const serviceCtx = useContext(ServicesContext);
    const { dataServiceRegister } = serviceCtx.servicesState;

    const quantityHandler = (event) => {
        serviceCtx.addQuantity(event.target.value);
    }
    return (
        <div className={classes['addition-item']}>
            <InputAuth
                className="addtion-item"
                label={`${props.name} ước tính`}
                placeholder={`Nhập ${props.name} bạn muốn`}
                input={{
                    id: props.id,
                    type: "number",
                    name: "quantity"
                }}
                onChange={quantityHandler}
                required={true}
                value={dataServiceRegister.quantity || ''}
            />
            <span> ( {props.unit} )</span>
        </div>
    );
}

export default AdditionalServicesItem;