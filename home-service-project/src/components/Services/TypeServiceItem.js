import React, { useCallback } from 'react';
import classes from './TypeServiceItem.module.css';

const TypeServiceItem = (props) => {
    const serviceChooseHandler = useCallback((event) => {
        if (event.target.checked) {
            const {unitid, typeserviceid} = event.target.attributes;
            props.checkedUnit(unitid.value);
            props.checkedTypeService(typeserviceid.value);
        }
    }, [props]);

    return (
        <div key={props.id} className={classes["service-item"]}>
            <img src={props.imageTypeService} alt="" />
            <span>{props.typeServiceName}</span>
            <input required onChange={serviceChooseHandler} typeserviceid={props.typeServiceId} type="radio" name="service" unitid={props.UnitId} />
        </div>
    );
}

export default TypeServiceItem;