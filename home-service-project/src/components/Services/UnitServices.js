import React, { Fragment, useContext, useEffect } from 'react';

import useHttp from '../../hooks/use-http';
import ServicesContext from '../../store/services-context';
import classes from './UnitServices.module.css';
import UnitServicesItem from './UnitServicesItem';
import { GetListUnit } from '../../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import AuthContext from '../../store/auth-context';

const UnitServices = (props) => {
    const { token } = useContext(AuthContext);
    const { sendRequest, status, data: listUnit, error } = useHttp(GetListUnit, true);

    useEffect(() => {
        sendRequest({
            token
        });
    }, [sendRequest]);

    const serviceCtx = useContext(ServicesContext);

    const { dataServiceRegister } = serviceCtx.servicesState;
    const unitId = dataServiceRegister.unitId;

    if (status === 'pending') {
        return <LoadingSpinner />
    }

    if (error) {
        return <div>{error}</div>
    }

    if (status === 'complete' && (!listUnit || listUnit.length === 0)) {
        return <div>Không tìm thấy dữ liệu</div>
    }

    if (unitId !== null) {
        const listUnitFilter = listUnit.filter((item) => item.Unit_Id === +unitId);

        const listUnitServicesItem = listUnitFilter.map((item, key) => {
            return (
                <UnitServicesItem id={item.Unit_Id} key={key} name={item.Name} unit={item.Unit} />
            );
        });

        return (
            <div className={classes.additions}>
                {listUnitServicesItem}
            </div>
        );
    } else {
        return <Fragment></Fragment>
    }
}

export default UnitServices;