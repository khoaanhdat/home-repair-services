import React, { useContext, useEffect, useState } from "react";

import ServicesContext from "../../store/services-context";
import TypeServiceItem from "./TypeServiceItem";
import classes from "./TypeServicesList.module.css";
import { getTypeServicesList } from "../../lib/api";
import useHttp from "../../hooks/use-http";
import LoadingSpinner from "../UI/LoadingSpinner";
import AuthContext from "../../store/auth-context";
import { useHistory } from "react-router";
import { MessageContext } from "../../store/message-context";

const TypeServicesList = (props) => {
  // Check unauthorized
  const history = useHistory();
  const { token, onLogout } = useContext(AuthContext);
  const { addMessage } = useContext(MessageContext);

  const {
    sendRequest,
    status,
    data: listTypeServices,
    error,
  } = useHttp(getTypeServicesList, true);

  useEffect(() => {
    sendRequest({
      token,
    });
  }, [sendRequest, token]);

  const serviceCtx = useContext(ServicesContext);

  const chooseUnitHandler = (unitId) => {
    serviceCtx.chooseUnit(unitId);
  };

  const chooseTypeServiceHandler = (typeServiceId) => {
    serviceCtx.chooseTypeService(typeServiceId);
    serviceCtx.addQuantity("");
  };

  if (status === "pending") {
    return <LoadingSpinner />;
  }

  if (error) {
    if (error === "Unauthorized") {
      history.push('/');
      onLogout();
      addMessage({ auth: true });
    } else {
      return <span>{error}</span>;
    }
  }

  if (
    status === "completed" &&
    (!listTypeServices || listTypeServices.length === 0)
  ) {
    return <div>Không tìm thấy dữ liệu</div>;
  }

  const servicesList = listTypeServices.map((item) => {
    return (
      <TypeServiceItem
        key={item.Type_Id}
        id={item.Type_Id}
        imageTypeService={item.Type_Image}
        typeServiceName={item.Type_Name}
        UnitId={item.Unit_Id}
        typeServiceId={item.Type_Id}
        checkedUnit={chooseUnitHandler}
        checkedTypeService={chooseTypeServiceHandler}
      />
    );
  });

  return <div className={classes["services-list"]}>{servicesList}</div>;
};

export default TypeServicesList;
