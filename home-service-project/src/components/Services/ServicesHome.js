import React, { Fragment, useContext } from 'react';

import ServicesContext from '../../store/services-context';
import Modal from '../UI/Modal';
import UnitServiceItem from './UnitServices';
import TypeServicesList from './TypeServicesList';
import Button from '../UI/Button'
import classes from './ServicesHome.module.css'
import PosibleCompanyContext from '../../store/posible-company-context';
import AuthContext from '../../store/auth-context';

const ServicesHome = (props) => {
    const posibleCompanyCtx = useContext(PosibleCompanyContext);

    const { isLoggedIn } = useContext(AuthContext);
    const servicesCtx = useContext(ServicesContext);
    const isShowServices = servicesCtx.isServicesShown;
    const { dataServiceRegister } = servicesCtx.servicesState;
    const unitId = dataServiceRegister.unitId;

    const hideServiceHome = () => {
        servicesCtx.hideServices();
    }

    const comfirmServices = (event) => {
        event.preventDefault();

        //show posible company
        posibleCompanyCtx.showPosibleCompany();
    }

    return (
        <Fragment>
            {isShowServices && (
                <Modal title="Dịch Vụ Bạn Muốn Sửa Chữa" onClose={hideServiceHome}>
                    <form onSubmit={comfirmServices} action="" className={classes['form-services']}>
                        {isLoggedIn ? (
                            <Fragment>
                                <TypeServicesList />
                                {unitId !== null && <UnitServiceItem />}
                                <div className={classes.actions}>
                                    <Button type="submit">Xác Nhận</Button>
                                </div>
                            </Fragment>
                        ) : <span>Bạn chưa đăng nhập.</span>}

                    </form>
                </Modal>
            )}
        </Fragment>
    );
}

export default ServicesHome;