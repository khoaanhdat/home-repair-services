import { useContext, useEffect } from "react";
import { useHistory } from "react-router-dom";

import ServicesContext from "../../store/services-context";
import Button from "../UI/Button";
import LoadingSpinner from "../UI/LoadingSpinner";
import classes from "./ServicesList.module.css";
import { ChoosePlaceRepair } from "../../lib/api";
import useHttp from "../../hooks/use-http";
import { MessageContext } from "../../store/message-context";
import AuthContext from "../../store/auth-context";
import Modal from "../UI/Modal";

const ServicesList = (props) => {
  const { sendRequest, status, error } = useHttp(ChoosePlaceRepair);

  const serviceCtx = useContext(ServicesContext);

  const { dataServiceRegister } = serviceCtx.servicesState;

  const quantity = +dataServiceRegister.quantity;

  // Check unauthorized
  const history = useHistory();
  const { token, onLogout, customerId } = useContext(AuthContext);
  const { addMessage } = useContext(MessageContext);

  const choosedCompanyHandler = (e, serviceId, totalPrice) => {
    e.preventDefault();

    const { address, location, bookTime } = dataServiceRegister;

    sendRequest({
      data: {
        Customer_Id: customerId,
        Service_Id: serviceId,
        Emp_Id: props.empId,
        Address: address,
        Location: `"[${location.lat}, ${location.lng}]"`,
        Date_Booked: bookTime,
        Quantity: quantity,
        Total_Price: totalPrice,
      },
      token,
    });
  };

  useEffect(() => {
    const timeRedirect = setTimeout(() => {
      if (status === "completed" && !error) {
        history.push("/account/register-service");
        addMessage({
          message: "Chọn nơi sửa chữa thành công.",
          className: "success",
        });
      }
    }, 500);

    if (error) {
      if (error === "Unauthorized") {
        setTimeout(() => {
          history.push("/");
          onLogout();
          addMessage({ auth: true });
        }, 100);
      } else {
        addMessage({
          message: error,
          className: "error",
        });
      }
    }
    return () => {
      clearTimeout(timeRedirect);
    };
  }, [status, error]);

  const listService = props.serviceList.map((item, key) => {
    return (
      <div key={key} className={classes["service-item"]}>
        <div className={classes.col}>
          <p>Tên dịch vụ</p>
          <p>{item.Service_Name}</p>
        </div>
        <div className={classes.col}>
          <p>Giá của bạn</p>
          <p>{(item.Service_Price * quantity).toLocaleString("it-IT")} đ</p>
        </div>

        <Button
          onClick={(e) =>
            choosedCompanyHandler(
              e,
              item.Service_Id,
              item.Service_Price * quantity
            )
          }
          disabled={status === "pending"}
        >
          Chọn
        </Button>
      </div>
    );
  });

  return (
    <div
      className={`${classes["list-services"]} ${
        props.className ? classes[props.className] : ""
      }`}
    >
      <span className={classes.title}>{props.title}</span>
      {listService}
    </div>
  );
};

export default ServicesList;
