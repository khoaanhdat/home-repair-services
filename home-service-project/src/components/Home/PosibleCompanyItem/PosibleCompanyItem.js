import React, { useCallback, useContext, useState } from "react";

import { Link } from "react-router-dom";
import Button from "../../UI/Button";
import classes from "./PosibleCompanyItem.module.css";
import ServicesList from "../../Services/ServicesList";

const PosibleCompanyItem = (props) => {
  const [isShowListService, setIsShowListService] = useState(false);

  const companyClickHandler = useCallback((e) => {
    e.preventDefault();
    e.stopPropagation();

    setIsShowListService((prevState) => (prevState = !prevState));
  }, []);

  return (
    <div className={classes.link}>
      <div key={props.id} className={classes.item}>
        <img onClick={companyClickHandler} src={props.logo} alt="" />
        <div onClick={companyClickHandler} className={classes.info}>
          <div className={classes["info-detail"]}>
            <span>{props.name}</span>
            <span>
              {props.review !== null
                ? `${props.review.toFixed(1)}/5 đánh giá`
                : "Chưa cập nhật"}
            </span>
          </div>
          <div className={classes["info-detail"]}>
            <span>Nhân viên online: </span>
            <span>{props.onlineEmp}</span>
          </div>
          <div className={classes["info-detail"]}>
            <span>Nhân viên gần nhất: </span>
            <span>{props.nearestStaff}</span>
          </div>
        </div>
        <div className={classes.actions}>
          <Link target="_blank" to={`/companies/${props.companyId}`}>
            <Button>Xem chi tiết</Button>
          </Link>
        </div>
      </div>

      <ServicesList
        className={isShowListService ? "active" : ""}
        companyId={props.companyId}
        empId={props.empId}
        title="Danh Sách Dịch Vụ"
        serviceList={props.serviceList}
      />
    </div>
  );
};

export default PosibleCompanyItem;
