import { Fragment, useContext, useEffect } from "react";

import Modal from "../UI/Modal";
import classes from "./PosibleCompaniesList.module.css";
import Button from "../UI/Button";
import ServicesContext from "../../store/services-context";
import { GetListCompanyAround } from "../../lib/api";
import useHttp from "../../hooks/use-http";
import PosibleCompanyItem from "./PosibleCompanyItem/PosibleCompanyItem";
import LoadingSpinner from "../UI/LoadingSpinner";
import { useHistory } from "react-router";
import AuthContext from "../../store/auth-context";
import { MessageContext } from "../../store/message-context";

const PosibleCompaniesList = (props) => {
  const servicesCtx = useContext(ServicesContext);
  const { dataServiceRegister } = servicesCtx.servicesState;
  const { typeServiceId, location } = dataServiceRegister;

  const {
    sendRequest,
    status,
    data: listCompaniesPosible,
    error,
  } = useHttp(GetListCompanyAround, true);

  // Check unauthorized
  const history = useHistory();
  const { token, onLogout } = useContext(AuthContext);
  const { addMessage } = useContext(MessageContext);

  useEffect(() => {
    sendRequest({
      typeId: typeServiceId,
      lat: location.lat,
      lng: location.lng,
      token,
    });
  }, [sendRequest, typeServiceId, location]);

  let result = null;

  if (status === "pending") {
    result = <LoadingSpinner />;
  } else if (error) {
    if (error === "Unauthorized") {
      addMessage({ auth: true });
      onLogout();

      setTimeout(() => {
        history.go(0);
      }, 3000);
    }
  } else if (
    status === "completed" &&
    (!listCompaniesPosible || listCompaniesPosible.length === 0)
  ) {
    result = <div>Không tìm thấy công ty nào. Xin vui lòng thử lại sau.</div>;
  } else {
    const posibleCompanyList = listCompaniesPosible.map((company) => (
      <PosibleCompanyItem
        key={company.Company_Id}
        companyId={company.Company_Id}
        logo={company.Company_Logo}
        name={company.Company_Name}
        review={company.Company_Review}
        onlineEmp={company.Count_Emp_Online}
        nearestStaff={company.Nearest_Distance}
        empId={company.Emp_Id}
        serviceList={company.List_Services}
      />
    ));

    result = (
      <Fragment>
        <div className={classes["wrapper-list"]}>{posibleCompanyList}</div>
        <div className={classes.action}>
          <Button className="success" onClick={props.findEmployeeRandomHandler}>
            Tìm Thợ Gần Nhất
          </Button>
          <Button
            className="warning"
            onClick={props.findEmployeeHighValueHandler}
          >
            Tìm Thợ Chất Lượng Tốt
          </Button>
        </div>
      </Fragment>
    );
  }

  return (
    <Fragment>
      {result !== null && (
        <Modal
        customModalClass="posible-company"
          customBackdropClass="posible-company"
          onClose={props.hidePosibleCompanyForm}
          title="Danh Sách Công Ty Sửa Chữa Gần Bạn"
        >
          {result}
        </Modal>
      )}
    </Fragment>
  );
};

export default PosibleCompaniesList;
