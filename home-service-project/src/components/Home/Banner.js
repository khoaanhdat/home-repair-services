import React from 'react';

import Slider from 'react-slick';
import 'slick-carousel/slick/slick.min';
import './slick.css';
import 'slick-carousel/slick/slick-theme.css';

import classes from './Banner.module.css';
import image1 from '../../assets/images/slide_1.jpg';
import image2 from '../../assets/images/slide_2.jpg';
import image3 from '../../assets/images/slide_3.jpg';
import imageStar from '../../assets/images/stars.webp';

const Banner = (props) => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    return (
        <div className={classes['banner-wrapper']}>
            <div className={classes.banners}>
                <Slider {...settings}>
                    <div className={classes['banner-item']}>
                        <img src={image1} alt=""/>
                        <div className={classes['banner-star']}>
                            <img src={imageStar} alt="" />
                        </div>
                        <div className={classes['banner-title']}>
                            <span>Home Cleaning</span>
                            <span>Chăm Sóc Ngôi Nhà Của Bạn</span>
                        </div>
                    </div>
                    <div className={classes['banner-item']}>
                        <img src={image2} alt=""/>
                        <div className={classes['banner-star']}>
                            <img src={imageStar} alt="" />
                        </div>
                        <div className={classes['banner-title']}>
                            <span>Home Cleaning 2</span>
                            <span>Chăm Sóc Ngôi Nhà Của Bạn 2</span>
                        </div>
                    </div>
                    <div className={classes['banner-item']}>
                        <img src={image3} alt=""/>
                        <div className={classes['banner-star']}>
                            <img src={imageStar} alt="" />
                        </div>
                        <div className={classes['banner-title']}>
                            <span>Home Cleaning 3</span>
                            <span>Chăm Sóc Ngôi Nhà Của Bạn 3</span>
                        </div>
                    </div>
                </Slider>
            </div>
        </div>

    );
}

export default Banner;