import React, { useCallback, useContext, useRef, useState } from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";
import { MessageContext } from "../../store/message-context";
import ServicesContext from "../../store/services-context";

import Button from "../UI/Button";
import Input from "../UI/Input";
import classes from "./SearchAddress.module.css";

const SearchAddress = (props) => {
  const [address, setAddress] = useState("");

  const [location, setLocation] = useState(null);

  const {showServices, addAddressSearch} = useContext(ServicesContext);

  const { addMessage } = useContext(MessageContext);

  const addressInputRef = useRef();

  const handleChanged = (address) => {
    setAddress(address);
  };

  const handleSelected = useCallback((addressSelected) => {
    geocodeByAddress(addressSelected)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        setAddress(addressSelected);
        setLocation(latLng);
        addAddressSearch(addressSelected, latLng);
      })
      .catch((error) => console.log("Error", error));
  }, []);

  const searchOptions = {
    location: new window.google.maps.LatLng(10.762622, 106.660172),
    radius: 1,
  };

  const showServicesHandler = () => {
    if (!!address && location) { 
      showServices();      
    } else {
      addMessage({
        message: "Vui lòng nhập và chọn địa chỉ của bạn",
        className: "error"
      });
    }
  };

  return (
    <PlacesAutocomplete
      value={address}
      onChange={handleChanged}
      onSelect={handleSelected}
      searchOptions={searchOptions}
      shouldFetchSuggestions={address.length > 2}
      debounce={500}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div
          className={`${classes["search-input"]} ${classes[props.className]}`}
        >
          <Input
            ref={addressInputRef}
            required={true}
            label={props.label}
            input={{
              id: "address",
              type: "input",
              name: "address",
            }}
            inputProps={getInputProps({
              placeholder: props.label,
            })}
          />
          <div className={classes["autocomplete-dropdown-container"]}>
            {loading && <div>Loading...</div>}
            {suggestions.map((suggestion) => {
              const className = suggestion.active
                ? "suggestion-item--active"
                : "suggestion-item";
              // inline style for demonstration purpose
              const style = suggestion.active
                ? { backgroundColor: "#a7bac2", cursor: "pointer" }
                : { backgroundColor: "#ffffff", cursor: "pointer" };
              return (
                <div
                  key={suggestion.placeId}
                  {...getSuggestionItemProps(suggestion, {
                    className,
                    style,
                  })}
                >
                  <span>{suggestion.description}</span>
                </div>
              );
            })}
          </div>
          <Button onClick={showServicesHandler} className={props.customButtonClass}>
            Tìm
          </Button>
        </div>
      )}
    </PlacesAutocomplete>
  );
};

export default SearchAddress;
