import React, { Fragment, useContext } from 'react';

import PosibleCompaniesList from './PosibleCompaniesList';
import PosibleCompanyContext from '../../store/posible-company-context';
import EmployeeRandomContext from '../../store/employee-random-context';

const PosibleCompany = (props) => {
    const posibleCompanyCtx = useContext(PosibleCompanyContext);

    const employeeRandomCtx = useContext(EmployeeRandomContext);

    const isShowPosibleCompany = posibleCompanyCtx.isShowPosibleCompany;

    const hidePosibleCompanyForm = () => {
        posibleCompanyCtx.hidePosibleCompany();
    }

    const findEmployeeRandomHandler = () => {
        employeeRandomCtx.showEmployeeRandom();
    }

    const findEmployeeHighValueHandler = () => {
        employeeRandomCtx.showEmployeeHighValue();
    }

    return (
        <Fragment>
            {isShowPosibleCompany &&
                <PosibleCompaniesList
                    hidePosibleCompanyForm={hidePosibleCompanyForm}
                    findEmployeeRandomHandler={findEmployeeRandomHandler}
                    findEmployeeHighValueHandler={findEmployeeHighValueHandler}
                />
            }
        </Fragment>
    );
}

export default PosibleCompany;