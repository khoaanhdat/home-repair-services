import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import classes from './Login.module.css';
import Button from '../UI/Button';
import InputAuth from '../UI/InputAuth';
import Auth from '../Auth/Auth';
import AuthContext from '../../store/auth-context';
import useInput from '../../hooks/use-input';
import useHttp from '../../hooks/use-http';
import { LogIn } from '../../lib/api';
import LoadingSpinner from '../UI/LoadingSpinner';
import { MessageContext } from '../../store/message-context';

const Login = (props) => {
    const {
        value: enteredPhone,
        isValid: enteredPhoneIsValid,
        hasError: phoneInputHasError,
        messageError: phoneMessageError,
        valueChanged: phoneChangeHandler,
        valueBlur: phoneBlurHandler,
        checkIsNotEmpty: phoneCheckIsNotEmpty,
        checkIsNumber: phoneCheckIsNumber
    } = useInput();

    const phoneOnChange = (event) => {
        phoneChangeHandler(event);
        phoneCheckIsNotEmpty(event.target.value);
        phoneCheckIsNumber(event.target.value);
    }

    const phoneOnBlur = () => {
        phoneBlurHandler();
        phoneCheckIsNotEmpty(enteredPhone);
        phoneCheckIsNumber(enteredPhone);
    }

    const {
        value: enteredPassword,
        isValid: enteredPasswordIsValid,
        hasError: passwordInputHasError,
        messageError: passwordMessageError,
        valueChanged: passwordChangeHandler,
        valueBlur: passwordBlurHandler,
        checkIsNotEmpty: passwordCheckIsNotEmpty,
    } = useInput();

    const passwordOnChange = (event) => {
        passwordChangeHandler(event);
        passwordCheckIsNotEmpty(event.target.value);
    }

    const passwordOnBlur = () => {
        passwordBlurHandler(enteredPassword);
        passwordCheckIsNotEmpty(enteredPassword);
    }

    let formIsValid = false;

    if (enteredPhoneIsValid && enteredPasswordIsValid) {
        formIsValid = true;
    }

    const authCtx = useContext(AuthContext);

    //Handler send request Login to Server
    const { sendRequest: logInRequest, data, error, status } = useHttp(LogIn);
    const [errorMes, setErrorMes] = useState();

    const history = useHistory();

    const loginHandler = () => {
        logInRequest({
            Phone: enteredPhone,
            Password: enteredPassword
        });
    }

    //Show message Box
    const { addMessage } = useContext(MessageContext);

    useEffect(() => {
        if (status === 'completed' && !error) {
            if (data.roleId !== 3) {
                setErrorMes('Bạn không có quyền sử dụng tài khoản này');
            } else {
                authCtx.onLogin(data.idToken, data.expiresIn, data.customerId);
                authCtx.hideLoginForm();
                history.replace('/');
                addMessage({
                    message: "Đăng nhập thành công.",
                    className: "success"
                });
            }
        }
    }, [status, error, authCtx, data, history]);

    return (
        <Auth title="Đăng Nhập">
            <InputAuth
                label="Số điện thoại"
                placeholder="Nhập số điện thoại"
                input={{
                    id: "phone",
                    type: "text"
                }}
                onChange={phoneOnChange}
                onBlur={phoneOnBlur}
                error={phoneInputHasError ? phoneMessageError : ''}
                className={phoneInputHasError ? 'invalid' : ''}
            />

            <InputAuth
                label="Mật khẩu"
                placeholder="Nhập mật khẩu"
                input={{
                    id: "password",
                    type: "password"
                }}
                onChange={passwordOnChange}
                onBlur={passwordOnBlur}
                error={passwordInputHasError ? passwordMessageError : ''}
                className={passwordInputHasError ? 'invalid' : ''}
            />
            {error && <p className={classes.error}>{error}</p>}
            {errorMes && <p className={classes.error}>{errorMes}</p>}
            {status === 'pending' && <LoadingSpinner />}
            {status !== 'pending' && <Button disabled={!formIsValid} onClick={loginHandler}>Đăng nhập</Button>}
        </Auth>
    );
}

export default Login;