import { useEffect, useState } from 'react';
import firebase from '../lib/firebase';

const useOTP = (phoneNumber) => {
    const [otp, setOtp] = useState("");
    const [isSendOTP, setIsSendOTP] = useState(false);
    const [otpIsValid, setOtpIsValid] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const otpOnChange = (e) => {
        setTimeout(() => {
            setOtp(e.target.value);
        }, 500);        
    }

    const configureCaptcha = () => {
        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('get-otp-button', {
            'size': 'invisible',
            'callback': (response) => {
                // reCAPTCHA solved, allow signInWithPhoneNumber.
                onGetOTPSubmit();
            }
        });
    }

    const onGetOTPSubmit = (e) => {
        e.preventDefault();

        configureCaptcha();

        const appVerifier = window.recaptchaVerifier;

        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then((confirmationResult) => {
                // SMS sent. Prompt user to type the code from the message, then sign the
                // user in with confirmationResult.confirm(code).
                window.confirmationResult = confirmationResult;
                setIsSendOTP(true);
            }).catch((error) => {
                // Error; SMS not sent
                console.log(error);
                setIsSendOTP(false);
            });
    }

    useEffect(() => {
        if (otp.trim().length === 6 && isSendOTP) {
            window.confirmationResult.confirm(otp).then((result) => {
                // User signed in successfully.
                // const user = result.user;
                setOtpIsValid(true);
                setErrorMessage('');
            }).catch((error) => {
                // User couldn't sign in (bad verification code?)
                setErrorMessage('Mã OTP không chính xác.');
                console.log(error);
            });
        }
    }, [otp, isSendOTP]);

    return {
        otpIsValid,
        isSendOTP,
        errorMessage,
        otpOnChange,
        onGetOTPSubmit
    }
}

export default useOTP;