import React, { useCallback, useState } from "react";

export const MessageContext = React.createContext({
    isShow: false,
    message: '',
    className: '',
    addMessage: (message, className, timer, auth) => { }
});

export const MessageContextProvider = (props) => {
    const [isShow, setIsShow] = useState(false);
    const [message, setMessage] = useState();
    const [className, setClassName] = useState();

    const addMessage = useCallback(({message, className, timer = 3000, auth}) => {
        if (auth) {
            message = 'Bạn không có quyền thực hiện chức năng này. Xin vui lòng đăng nhập lại';
            className = 'error';
            timer = 5000;
        }
        setMessage(message);
        setClassName(className);
        setIsShow(true);
        setTimeout(() => {
            setIsShow(false);
        }, timer);
    }, []);

    const contextValue = {
        isShow,
        message,
        className,
        addMessage
    }

    return <MessageContext.Provider value={contextValue}>{props.children}</MessageContext.Provider>
}