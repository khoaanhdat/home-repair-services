import React, { useState } from 'react';
import EmployeeRandomContext from './employee-random-context';

const EmloyeeRandomProvider = props => {
    const [isShowEmployeeRandom, setIsShowEmployeeRandom] = useState(false);
    const [isShowEmpHighValue, setIsShowEmpHighValue] = useState(false);
    
    const showEmployeeRandom = () => {
        setIsShowEmployeeRandom(true);
        document.body.classList.add('modal-open');
    }

    const hideEmployeeRandom = () => {
        setIsShowEmployeeRandom(false);
        document.body.classList.remove('modal-open');
    }

    const showEmployeeHighValue = () => {
        setIsShowEmpHighValue(true);
        document.body.classList.add('modal-open');
    }

    const hideEmployeeHignValue = () => {
        setIsShowEmpHighValue(false);
        document.body.classList.remove('modal-open');
    }
    
    const employeeRandomContext = {
        isShowEmployeeRandom,
        isShowEmpHighValue,
        showEmployeeRandom,
        hideEmployeeRandom,
        showEmployeeHighValue,
        hideEmployeeHignValue
    }

    return (
        <EmployeeRandomContext.Provider value={employeeRandomContext}>
            {props.children}
        </EmployeeRandomContext.Provider>
    )
}

export default EmloyeeRandomProvider;