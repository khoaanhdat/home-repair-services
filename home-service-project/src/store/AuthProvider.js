import React, { useState } from 'react';
import cookie from 'react-cookies';

import AuthContext from './auth-context';

const AuthProvider = props => {
    const [isLoginFormShown, setIsLoginFormShow] = useState(false);
    const [isSignUpFormShown, setIsSignUpFormShown] = useState(false);

    let token = null, customerId = null;
    const cookieDataCustomer = cookie.load('customer');
    if (cookieDataCustomer !== undefined) {
        token = cookieDataCustomer.token;
        customerId = cookieDataCustomer.customerId;
    }

    const [tokenState, setTokenState] = useState(token);
    const [customerIdState, setCustomerIdState] = useState(customerId);

    const userIsLoggedIn = !!tokenState;

    const loginHandler = (token, expiresIn, customerId) => {
        setTokenState(token);
        setCustomerIdState(customerId);

        const expirationTime = new Date();
        expirationTime.setTime(expirationTime.getTime() + (+expiresIn * 1000));

        cookie.save(
            'customer',
            JSON.stringify({ token: token, customerId: customerId }),
            { path: '/', expires: expirationTime }
        );
    };

    const logoutHandler = () => {
        setTokenState(null);
        setCustomerIdState(null);
        cookie.remove('customer', { path: '/' });
    };

    const showLoginFormFn = () => {
        setIsLoginFormShow(true);
        document.body.classList.add('modal-open');
    }

    const showSignUpFormFn = () => {
        setIsSignUpFormShown(true);
        document.body.classList.add('modal-open');
    }

    const hideLoginFormFn = () => {
        setIsLoginFormShow(false);
        document.body.classList.remove('modal-open');
    }

    const hideSignUpFormFn = () => {
        setIsSignUpFormShown(false);
        document.body.classList.remove('modal-open');
    }

    const authContext = {
        isLoginFormShown,
        isSignUpFormShown,
        showLoginForm: showLoginFormFn,
        showSignUpForm: showSignUpFormFn,
        hideLoginForm: hideLoginFormFn,
        hideSignUpForm: hideSignUpFormFn,
        token: tokenState,
        customerId: customerIdState,
        isLoggedIn: userIsLoggedIn,
        onLogin: loginHandler,
        onLogout: logoutHandler
    }

    return (
        <AuthContext.Provider value={authContext}>
            {props.children}
        </AuthContext.Provider>
    )
}

export default AuthProvider;