import React from 'react';

const ServicesContext = React.createContext({
    isServicesShown: false,
    servicesState: null,
    chooseUnit: (unitId) => { },
    addAddressSearch: (address, location) => { },
    addQuantity: (quantity) => { },
    addEmp: (empId) => { },
    chooseTypeService: (typeServiceId) => { },
    chooseService: (serviceId) => { },
    addPosibleCompany: (companyId) => { },
    addBookTime: (dateTime) => { },
    showServices: () => { },
    hideServices: () => { }
});

export default ServicesContext;