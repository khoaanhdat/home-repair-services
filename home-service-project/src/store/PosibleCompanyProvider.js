import React, { useState } from 'react';
import PosibleCompanyContext from './posible-company-context';

const PosibleCompanyProvider = props => {
    const [isShowPosibleCompany, setIsShowPosibleCompany] = useState(false);
    
    const showPosibleCompany = () => {
        setIsShowPosibleCompany(true);
        document.body.classList.add('modal-open');
    }

    const hidePosibleCompany = () => {
        setIsShowPosibleCompany(false);
        document.body.classList.remove('modal-open');
    }
    
    const posibleCompanyContext = {
        isShowPosibleCompany,
        showPosibleCompany,
        hidePosibleCompany
    }

    return (
        <PosibleCompanyContext.Provider value={posibleCompanyContext}>
            {props.children}
        </PosibleCompanyContext.Provider>
    )
}

export default PosibleCompanyProvider;