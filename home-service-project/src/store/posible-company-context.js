import React from 'react';

const PosibleCompanyContext = React.createContext({
    isShowPosibleCompany: false,
    showPosibleCompany: () => {},
    hidePosibleCompany: () => {}
});

export default PosibleCompanyContext;