import React from 'react';

const AuthContext = React.createContext({
    isLoginFormShown: false,
    isSignUpFormShown: false,
    showLoginForm: () => {},
    showSignUpForm: () => {},
    hideLoginForm: () => {},
    hideSignUpForm: () => {},
    token: '',
    customerId: null,
    isLoggedIn: false,
    onLogout: () => { },
    onLogin: (token, expirationTime) => { }
});

export default AuthContext;