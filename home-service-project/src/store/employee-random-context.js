import React from 'react';

const EmployeeRandomContext = React.createContext({
    isShowEmployeeRandom: false,
    isShowEmpHighValue: false,
    showEmployeeRandom: () => {},
    hideEmployeeRandom: () => {},
    showEmployeeHighValue: () => {},
    hideEmployeeHignValue: () => {}
});

export default EmployeeRandomContext;