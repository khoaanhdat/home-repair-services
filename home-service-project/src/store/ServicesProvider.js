import React, { useReducer, useState } from 'react';
import ServicesContext from './services-context';

const defaultServicesState = {
    dataServiceRegister: {
        unitId: null,
        location: null,
        address: null,
        typeServiceId: null,
        quantity: '',
        companyId: null,
        serviceId: null,
        empId: null,
        bookTime: new Date().toLocaleDateString('en-GB')
    }
}

const servicesReducer = (state, action) => {
    if (action.type === 'CHOOSE_UNIT') {
        return {
            dataServiceRegister: {
                ...state.dataServiceRegister,
                unitId: action.unitId
            }
        }
    }

    if (action.type === 'ADD_ADDRESS_LOCATION') {
        return {
            dataServiceRegister: {
                ...state.dataServiceRegister,
                location: action.location,
                address: action.address
            }
        }
    }

    if (action.type === 'ADD_QUANTITY') {
        return {
            dataServiceRegister: {
                ...state.dataServiceRegister,
                quantity: action.quantity
            }
        }
    }

    if (action.type === 'ADD_EMP') {
        return {
            dataServiceRegister: {
                ...state.dataServiceRegister,
                empId: action.empId
            }
        }
    }

    if (action.type === 'CHOOSE_TYPE_SERVICE') {
        return {
            dataServiceRegister: {
                ...state.dataServiceRegister,
                typeServiceId: action.typeServiceId
            }
        }
    }

    if (action.type === 'CHOOSE_SERVICE') {
        return {
            dataServiceRegister: {
                ...state.dataServiceRegister,
                serviceId: action.serviceId
            }
        }
    }

    if (action.type === 'ADD_POSIBLE_COMPANY') {
        return {
            dataServiceRegister: {
                ...state.dataServiceRegister,
                companyId: action.companyId
            }
        }
    }

    if (action.type === 'ADD_BOOK_TIME') {
        return {
            dataServiceRegister: {
                ...state.dataServiceRegister,
                bookTime: action.bookTime
            }
        }
    }

    return defaultServicesState;
}

const ServicesProvider = props => {

    const [isServicesShown, setIsServicesShown] = useState(false);

    const [servicesState, dispatchServiceAction] = useReducer(servicesReducer, defaultServicesState);

    const chooseUnit = (unitId) => {
        dispatchServiceAction({
            type: 'CHOOSE_UNIT',
            unitId
        });
    }

    const addAddressSearch = (address, location) => {
        dispatchServiceAction({
            type: 'ADD_ADDRESS_LOCATION',
            address,
            location
        });
    }

    const addQuantity = (quantity) => {
        dispatchServiceAction({
            type: 'ADD_QUANTITY',
            quantity
        });
    }

    const addEmp = (empId) => {
        dispatchServiceAction({
            type: 'ADD_QUANTITY',
            empId
        });
    }

    const chooseTypeService = (typeServiceId) => {
        dispatchServiceAction({
            type: 'CHOOSE_TYPE_SERVICE',
            typeServiceId
        });
    }

    const chooseService = (serviceId) => {
        dispatchServiceAction({
            type: 'CHOOSE_SERVICE',
            serviceId
        });
    }

    const addPosibleCompany = (companyId) => {
        dispatchServiceAction({
            type: 'ADD_POSIBLE_COMPANY',
            companyId
        });
    }

    const addBookTime = (bookTime) => {
        dispatchServiceAction({
            type: 'ADD_BOOK_TIME',
            bookTime
        });
    }

    const showServices = () => {
        setIsServicesShown(true);
        document.body.classList.add('modal-open');
    }

    const hideServices = () => {
        setIsServicesShown(false);
        document.body.classList.remove('modal-open');
    }

    const servicesContext = {
        isServicesShown,
        servicesState,
        chooseUnit,
        addAddressSearch,
        addQuantity,
        addEmp,
        chooseTypeService,
        chooseService,
        addPosibleCompany,
        addBookTime,
        showServices,
        hideServices
    }

    return (
        <ServicesContext.Provider value={servicesContext}>
            {props.children}
        </ServicesContext.Provider>
    )
}

export default ServicesProvider;