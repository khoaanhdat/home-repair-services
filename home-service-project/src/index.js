import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import './index.css';
import App from './App';
import AuthProvider from './store/AuthProvider';
import { MessageContextProvider } from './store/message-context';

ReactDOM.render(
  <MessageContextProvider>
    <AuthProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </AuthProvider>
  </MessageContextProvider>,
  document.getElementById('root')
);