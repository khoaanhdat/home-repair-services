import React, { useContext, useState } from 'react';
import { Route, Switch } from 'react-router';

import './App.css';
import Layout from './components/Layout/Layout';
import CompanyDetail from './pages/CompanyDetail';
import Home from './pages/Home';
import NotFound from './pages/NotFound';
import PostRepair from './pages/PostRepair';
import EmployeeDetail from './pages/EmployeeDetail';
import AuthContext from './store/auth-context';
import Account from './pages/Account';

function App() {
  const headers = new Headers();
  headers.append('Accss-Control-Allow-Origin', 'http://localhost:3000');
  headers.append('Access-Control-Allow-Credentials', 'true');
  headers.append('GET', 'POST', 'OPTIONS');

  const authCtx = useContext(AuthContext);

  return (
    <Layout>
      <Switch>
        <Route path='/' exact>
          <Home />
        </Route>

        {authCtx.isLoggedIn && (
          <Route path='/post-repair'>
            <PostRepair />
          </Route>
        )}

        <Route path='/companies/:companyId'>
          <CompanyDetail />
        </Route>

        <Route path='/employees/:empId'>
          <EmployeeDetail />
        </Route>

        {authCtx.isLoggedIn && (
          <Route path='/account'>
            <Account />
          </Route>
        )}

        <Route path='*'>
          <NotFound />
        </Route>

      </Switch>
    </Layout>
  );
}

export default App;
