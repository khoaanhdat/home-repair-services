import React, { Fragment } from "react";

import PosibleCompany from "../components/Home/PosibleCompany";
import PosibleComapanyProvider from "../store/PosibleCompanyProvider";
import Banner from "../components/Home/Banner";
import SearchAddress from "../components/Home/SearchAddress";
import ServicesHome from "../components/Services/ServicesHome";
import ServicesProvider from "../store/ServicesProvider";
import EmloyeeRandomProvider from "../store/EmployeeRandomProvider";
import EmployeeRandom from "../components/Employee/EmployeeRandom";
import EmployeeHighValue from "../components/Employee/EmployeeHighValue";

const Home = (props) => {
  return (
    <Fragment>
      <Banner />
      <ServicesProvider>
        <PosibleComapanyProvider>
          <EmloyeeRandomProvider>
            <SearchAddress label="Bạn đang ở đâu nhỉ..." />
            <ServicesHome />
            <PosibleCompany />
            <EmployeeRandom />
            <EmployeeHighValue />
          </EmloyeeRandomProvider>
        </PosibleComapanyProvider>
      </ServicesProvider>
    </Fragment>
  );
};

export default Home;
