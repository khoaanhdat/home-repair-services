import React, { Fragment, useContext, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';

import CompanyInfo from '../components/Company/CompanyInfo';
import EmployeeList from '../components/Employee/EmployeeList';
import LoadingSpinner from '../components/UI/LoadingSpinner';
import useHttp from '../hooks/use-http';
import { GetCompanyDetailById } from '../lib/api';
import AuthContext from '../store/auth-context';
import { MessageContext } from '../store/message-context';

const CompanyDetail = (props) => {
    const params = useParams();
    const { companyId } = params;

    // Check unauthorized
    const history = useHistory();
    const { token, onLogout } = useContext(AuthContext);
    const { addMessage } = useContext(MessageContext);

    const { sendRequest, status, data: companyInfo, error } = useHttp(
        GetCompanyDetailById,
        true
    );

    useEffect(() => {
        sendRequest({ companyId, token });
    }, [companyId, sendRequest, token]);

    if (status === 'pending') {
        return <LoadingSpinner />
    }

    if (error) {
        if (error === 'Unauthorized') {
            setTimeout(() => {
                history.push('/');
                onLogout();
                addMessage({ auth: true });
            }, 100);
            return false;
        } else {
            return <span>{error}</span>
        }
    }

    if (status === 'complete' && (!companyInfo || Object.keys(companyInfo).length === 0)) {
        return <span>Không có dữ liệu</span>
    }

    return (
        <Fragment>
            <CompanyInfo
                bannerImg="https://kythuattoanmy.vn/wp-content/uploads/2020/11/bg_2-min.jpg"
                companyLogo={companyInfo.Company_Logo}
                avgReview={companyInfo.Avg_Review}
                countEmp={companyInfo.Count_Emp}
                countCustomer={companyInfo.Count_Customer}
                taxCode={companyInfo.Tax_Code}
                companyName={companyInfo.Company_Name}
                companyDescription={companyInfo.Company_Description}
            />

            <EmployeeList employeeList={companyInfo.Employees} />
        </Fragment>
    );
}

export default CompanyDetail;