import { Fragment } from "react";

import MyAccount from "../components/Account/MyAccount";
import BannerPage from '../components/Layout/BannerPage';

const Account = (props) => {
    return (
        <Fragment>
            <BannerPage bannerImg="https://i.picsum.photos/id/287/4288/2848.jpg?hmac=f_-W7-bOKUxLoH9uOz4Hwk9D8zYTgzbHX7i_vY_ljug" />
            <div className="account">
                <MyAccount />                
            </div>
        </Fragment>
    );
}

export default Account;