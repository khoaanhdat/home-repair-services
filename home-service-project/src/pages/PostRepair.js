import React from 'react';

import ServicesPropvider from '../store/ServicesProvider';
import PostRepairWarraper from '../components/Post/PostRepairWrapper';

const PostRepair = (props) => {
    return (
        <ServicesPropvider>
            <PostRepairWarraper />
        </ServicesPropvider>

    );
}

export default PostRepair;