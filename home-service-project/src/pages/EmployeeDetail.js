import React, { Fragment, useContext, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";

import EmployeeItem from "../components/Employee/EmployeeItem";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import useHttp from "../hooks/use-http";
import { GetEmployeeDetailById } from '../lib/api';
import AuthContext from "../store/auth-context";
import { MessageContext } from "../store/message-context";

const EmployeeDetail = (props) => {
    const params = useParams();
    const { empId } = params;

    // Check unauthorized
    const history = useHistory();
    const { token, onLogout } = useContext(AuthContext);
    const { addMessage } = useContext(MessageContext);

    const { sendRequest, status, data: employeeInfo, error } = useHttp(
        GetEmployeeDetailById,
        true
    );

    useEffect(() => {
        sendRequest({empId, token});
    }, [empId, sendRequest, token]);

    if (status === 'pending') {
        return <LoadingSpinner />
    }

    if (error) {
        if (error === 'Unauthorized') {
            setTimeout(() => {
                history.push('/');
                onLogout();
                addMessage({ auth: true });
            }, 100);
            return false;
        } else {
            return <span>{error}</span>
        }
    }

    if (status === 'complete' && (!employeeInfo || Object.keys(employeeInfo).length === 0)) {
        return <span>Không có dữ liệu</span>
    }

    return (
        <Fragment>
            <EmployeeItem
                empName={employeeInfo.Emp_Name}
                empSex={employeeInfo.Emp_Sex}
                empAvailable={employeeInfo.Emp_Avaible}
                empReview={employeeInfo.Emp_Review}
                empSeniority={employeeInfo.Emp_Seniority}
                empAvt={employeeInfo.Emp_Avt}
                empAddress={employeeInfo.Emp_Address}
                empDescriptions={employeeInfo.Emp_Description}
                empBirthDay={employeeInfo.Emp_Birthday}
                bannerImg="https://kythuattoanmy.vn/wp-content/uploads/2020/11/bg_2-min.jpg"
            />
        </Fragment>
    );
}

export default EmployeeDetail;