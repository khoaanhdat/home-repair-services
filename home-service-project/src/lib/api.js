const API_DOMAIN = "https://home-repair-api.herokuapp.com";

export const getTypeServicesList = async ({ token }) => {
  const response = await fetch(`${API_DOMAIN}/GetListTypeService`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();
  const listTypeService = data.List_Type_Service;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...listTypeService];
};

export const GetListUnit = async ({ token }) => {
  const response = await fetch(`${API_DOMAIN}/GetListUnit`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();
  const listUnit = data.List_Unit;

  if (!response.ok) {
    throw new Error(data.message || "Không thể lấy dữ liệu.");
  }

  return [...listUnit];
};

export const GetListCompanyAround = async ({ typeId, lat, lng, token }) => {
  const response = await fetch(
    `${API_DOMAIN}/GetListCompanyAround?Type_Id=${typeId}&Location=${lat},${lng}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
  const data = await response.json();
  const listCompanyAround = data.List_Company_Around;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...listCompanyAround];
};

export const GetListEmpHighValue = async ({ typeId, lat, lng, token }) => {
  const response = await fetch(
    `${API_DOMAIN}/Collaborative_Filtering?Type_Id=${typeId}&Location=${lat},${lng}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
  const data = await response.json();
  const listEmpHighValue = data.Results;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...listEmpHighValue];
};

export const GetCompanyDetailById = async ({ companyId, token }) => {
  if (companyId) {
    const response = await fetch(
      `${API_DOMAIN}/GetCompanyDetailById?Company_Id=${companyId}`,
      {
        headers: {
          Authorization: token,
        },
      }
    );
    const data = await response.json();

    if (!response.ok) {
      throw new Error(data.error || "Không thể lấy dữ liệu.");
    }

    return data;
  }
};

export const GetEmployeeDetailById = async ({ empId, token }) => {
  if (empId) {
    const response = await fetch(
      `${API_DOMAIN}/GetEmployeeDetailById?Emp_Id=${empId}`,
      {
        headers: {
          Authorization: token,
        },
      }
    );
    const data = await response.json();

    if (!response.ok) {
      throw new Error(data.error || "Không thể lấy dữ liệu.");
    }

    return data;
  }
};

export const FindRandomRepair = async ({ typeId, lat, lng, token }) => {
  const response = await fetch(
    `${API_DOMAIN}/FindRandomRepair?Type_Id=${typeId}&Location=${lat},${lng}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return data;
};

export const GetListPostByIdCustomer = async ({ customerId, token }) => {
  if (customerId) {
    const response = await fetch(
      `${API_DOMAIN}/GetListPostByIdCustomer?Customer_Id=${customerId}`,
      {
        headers: {
          Authorization: token,
        },
      }
    );
    const data = await response.json();
    const listPost = data.List_Post;

    if (!response.ok) {
      throw new Error(data.message || "Không thể lấy dữ liệu.");
    }

    return [...listPost];
  }
};

export const GetListRegisterServiceByIdCustomer = async ({
  customerId,
  token,
}) => {
  if (customerId) {
    const response = await fetch(
      `${API_DOMAIN}/GetListRegisterServiceByIdCustomer?Customer_Id=${customerId}`,
      {
        headers: {
          Authorization: token,
        },
      }
    );
    const data = await response.json();
    const listRegisterService = data.List_Register_Service;

    if (!response.ok) {
      throw new Error(data.error || "Không thể lấy dữ liệu.");
    }

    return [...listRegisterService];
  }
};

export const GetCustomerById = async ({ customerId, token }) => {
  if (customerId) {
    const response = await fetch(
      `${API_DOMAIN}/GetCustomerById?Customer_Id=${customerId}`,
      {
        headers: {
          Authorization: token,
        },
      }
    );
    const data = await response.json();

    if (!response.ok) {
      throw new Error(data.error || "Không thể lấy dữ liệu.");
    }

    return data;
  }
};

export const UpdateCustomer = async ({ customerUpdateData, token }) => {
  const response = await fetch(`${API_DOMAIN}/UpdateCustomer`, {
    method: "PUT",
    body: JSON.stringify(customerUpdateData),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.message || "Không thể cập nhật.");
  }

  return data;
};

export const PostRepairNotice = async ({ dataPost, token }) => {
  const response = await fetch(`${API_DOMAIN}/PostRepairNotice`, {
    method: "POST",
    body: JSON.stringify(dataPost),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();

  if (!response.ok) {
    let errorMessage = data.error;
    throw new Error(errorMessage);
  }

  return data;
};

export const ChoosePlaceRepair = async (registerServiceData) => {
  const response = await fetch(`${API_DOMAIN}/ChoosePlaceRepair`, {
    method: "POST",
    body: JSON.stringify(registerServiceData.data),
    headers: {
      "Content-Type": "application/json",
      Authorization: registerServiceData.token,
    },
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Không thể cập nhật.");
  }

  return data;
};

export const Review = async ({ reviewData, token }) => {
  const response = await fetch(`${API_DOMAIN}/Review`, {
    method: "POST",
    body: JSON.stringify(reviewData),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Máy chủ bị lỗi. Xin vui lòng thử lại sau.");
  }

  return data;
};

export const SignUp = async (registerData) => {
  const response = await fetch(`${API_DOMAIN}/SignUp`, {
    method: "POST",
    body: JSON.stringify(registerData),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const data = await response.json();

  if (!response.ok) {
    let errorMessage = data.error;
    if (errorMessage === "Duplicate unique phone please choose others!") {
      errorMessage =
        "Số điện thoại này đã đăng ký tài khoản. Xin vui lòng kiểm tra lại";
    }
    throw new Error(errorMessage);
  }

  return data;
};

export const PaymentOnline = async ({ processId, token }) => {
  const response = await fetch(`${API_DOMAIN}/PaymentOnline`, {
    method: "PUT",
    body: JSON.stringify(processId),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Không thể thanh toán.");
  }

  return data;
};

export const LogIn = async (loginData) => {
  const response = await fetch(`${API_DOMAIN}/LogIn`, {
    method: "POST",
    body: JSON.stringify(loginData),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const data = await response.json();

  if (!response.ok) {
    let errorMessage = data.error;
    if (errorMessage === "Wrong phone and password") {
      errorMessage = "Số điện thoại hoặc mật khẩu sai. Xin kiểm tra lại.";
    }
    throw new Error(errorMessage);
  }

  return data;
};
