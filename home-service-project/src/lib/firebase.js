import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyBAhRCjKVbYIBLv6fZWu9kCVLTzn2kQFY4",
    authDomain: "homeservice-4b675.firebaseapp.com",
    projectId: "homeservice-4b675",
    storageBucket: "homeservice-4b675.appspot.com",
    messagingSenderId: "288293479650",
    appId: "1:288293479650:web:8df426c8689391eb306abb"
}
firebase.initializeApp(config);
export default firebase;