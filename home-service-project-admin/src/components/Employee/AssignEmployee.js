import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import classes from "./AssignEmployee.module.css";
import Modal from "../UI/Modal";
import Button from "../UI/Button";
import Calendar from "../UI/Calendar";
import AssignEmployeeContext from "../../store/assign-employee-context";
import useHttp from "../../hooks/use-http";
import { GetEmployee, AssignmentWork } from "../../lib/api";
import FontAwesome from "../UI/FontAwesome";
import AuthContext from "../../store/auth-context";
import { MessageContext } from "../../store/message-context";

const AssignEmployee = ({ listServices, reloadPosts }) => {
  const { token, onLogout, companyId } = useContext(AuthContext);

  const [serviceId, setServiceId] = useState(null);

  const assignEmpCtx = useContext(AssignEmployeeContext);

  const { dataAssignEmp } = assignEmpCtx.assignEmpState;

  const { postId, quantity, address, dateBooked, customerId, location } =
    dataAssignEmp;

    const convertedDateBooked = new Date(
      dateBooked.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3")
    );

  const [dateAssign, onChange] = useState(new Date(convertedDateBooked));

  let locationFormat = location.replaceAll("[", "");
  locationFormat = locationFormat.replaceAll("]", "");
  locationFormat = locationFormat.replaceAll('"', "");

  const listServicesOptions = listServices.map((item, key) => {
    return (
      <option price={item.price} key={key} value={item.Service_Id}>
        {item.Service_Name}
      </option>
    );
  });

  const { addMessage } = useContext(MessageContext);
  const history = useHistory();

  // Handler find employee by date
  const {
    sendRequest: getEmployee,
    data: listEmpData,
    error: errorGetEmp,
    status: statusGetEmp,
  } = useHttp(GetEmployee, true);

  useEffect(() => {
    const date = dateAssign.toLocaleDateString("en-GB");

    getEmployee({
      companyId,
      date,
      location: locationFormat,
      token,
    });
  }, [dateAssign]);

  const chooseServicesHandler = (e) => {
    setServiceId(e.target.value);
  };

  const {
    sendRequest: assignWork,
    status: statusAssignWork,
    error: errorAssignWork,
  } = useHttp(AssignmentWork);

  const assignEmpHandler = (event, empId) => {
    event.preventDefault();

    const approveDate = dateAssign.toLocaleDateString("en-GB");

    assignWork({
      assignData: {
        Post_Id: postId,
        Emp_Id: empId,
        Address: address,
        Location: locationFormat,
        Service_Id: serviceId,
        Date_Booked: dateBooked,
        Customer_Id: customerId,
        Quantity: quantity,
        Approve_Date: approveDate,
      },
      token,
    });
  };

  useEffect(() => {
    if (statusAssignWork === "completed" && !errorAssignWork) {
      assignEmpCtx.hidePopup();

      reloadPosts();

      addMessage({
        message: "Phân công thành công.",
        className: "success",
      });
    }

    if (errorAssignWork) {
      if (
        errorAssignWork === "Unauthorized" ||
        errorAssignWork === "Access Denied!"
      ) {
        setTimeout(() => {
          history.push("/");
          onLogout();
          addMessage({ auth: true });
        }, 100);
      } else if (errorAssignWork === "Must be have service register") {
        addMessage({
          message: "Bạn chưa chọn dịch vụ",
          className: "error",
        });
      } else {
        addMessage({
          message: errorAssignWork,
          className: "error",
        });
      }
    }
  }, [statusAssignWork, getEmployee, addMessage, errorAssignWork]);

  //Generate list employee
  let listEmployee;
  if (statusGetEmp === "pending") {
    listEmployee = (
      <tbody>
        <tr>
          <td>Loading...</td>
        </tr>
      </tbody>
    );
  } else if (errorGetEmp) {
    if (errorGetEmp === "Unauthorized" || errorGetEmp === "Access Denied!") {
      setTimeout(() => {
        history.push("/");
        onLogout();
        addMessage({ auth: true });
      }, 500);
      return false;
    } else {
      listEmployee = (
        <tbody>
          <tr>
            <td>{errorGetEmp}</td>
          </tr>
        </tbody>
      );
    }
  } else if (
    statusGetEmp === "completed" &&
    (!listEmpData || listEmpData.length === 0)
  ) {
    listEmployee = (
      <tbody>
        <tr>
          <td>Không tìm thấy nhân viên nào trong ngày này.</td>
        </tr>
      </tbody>
    );
  } else {
    const listEmployeeTemp = listEmpData.map((item, key) => {
      return (
        <tr key={key}>
          <td>{item.Emp_Name}</td>
          <td>{item.Distance}</td>
          <td>
            <Button
              onClick={(event) => assignEmpHandler(event, item.Emp_Id)}
              type="submit"
            >
              <FontAwesome icon="user-check" color="green" />
              {" Phân Công"}
            </Button>
          </td>
        </tr>
      );
    });
    listEmployee = <tbody>{listEmployeeTemp}</tbody>;
  }

  const hideAssignEmp = () => {
    assignEmpCtx.hidePopup();
  };

  return (
    <Modal
      onClose={hideAssignEmp}
      customModalClass="assign-emp"
      customBackdropClass="assign-emp"
      title="Phân Công Cho Nhân Viên"
    >
      <form className={classes["assign-emp"]}>
        <div className={classes["cal-ser"]}>
          <div className={classes.calendar}>
            <span>Chọn ngày phân công</span>
            <Calendar
              onChange={onChange}
              value={dateAssign}
              minDate={new Date()}
            />
          </div>
          <div className={classes.services}>
            <span>Chọn dịch vụ</span>
            <select defaultValue={""} required onChange={chooseServicesHandler}>
              <option value="" disabled>
                Chọn dịch vụ
              </option>
              {listServicesOptions}
            </select>
          </div>
        </div>

        <div className={classes.employee}>
          <table>
            <thead>
              <tr>
                <td>Tên Nhân Viên</td>
                <td>Khoảng Cách</td>
                <td></td>
              </tr>
            </thead>
            {listEmployee}
          </table>
        </div>
      </form>
    </Modal>
  );
};

export default AssignEmployee;
