import { Fragment, useContext, useState } from 'react';
import { usePagination } from '../../hooks/use-pagination';
import EmployeeManagerContext from '../../store/employee-manager-context';
import Button from '../UI/Button';
import FontAwesome from '../UI/FontAwesome';
import LoadingSpinner from '../UI/LoadingSpinner';
import { Pagination } from '../UI/Pagination';
import Table from '../UI/Table';
import classes from './EmployeeList.module.css';

const EmployeeList = (props) => {
    const [listEmp, setListEmp] = useState(props.listEmp);

    const perPage = 10;
    const {
        currentList: currentEmps,
        currentPage,
        paginateHandler
    } = usePagination(1, perPage, listEmp);


    const empManagerCtx = useContext(EmployeeManagerContext);

    const editEmpHandler = (emp) => {
        empManagerCtx.showEditForm();

        empManagerCtx.chooseEditEmployee(
            emp.Emp_Id,
            emp.Emp_FirstName,
            emp.Emp_LastName,
            emp.Description,
            emp.Address,
            emp.Location,
            emp.Sex,
            emp.Available,
            emp.Phone,
            emp.BirthDay
        );
    }

    if (!currentEmps || currentEmps.length === 0) {
        return <LoadingSpinner />
    }

    const headers = [
        "STT",
        "Họ Tên",
        "Điện thoại",
        "Địa Chỉ",
        "Giới Thiệu",
        "Ngày Sinh",
        "Trạng Thái",
        "Thao Tác"
    ];

    const addNewEmployee = () => {
        empManagerCtx.showNewForm();
    }

    const body = currentEmps.map((item, key) => {
        return (
            <tr key={key}>
                <td>{key + 1}</td>
                <td>{item.Emp_LastName + ' ' + item.Emp_FirstName}</td>
                <td>{item.Phone}</td>
                <td>{item.Address ?? 'Chưa có'}</td>
                <td>{item.Description ?? 'Chưa có'}</td>
                <td>{item.BirthDay}</td>
                <td>
                    {!item.Available ?
                        <span style={{ color: 'red' }}>Tạm dừng</span> :
                        <span style={{ color: 'green' }}>Hoạt động</span>
                    }
                </td>
                <td>
                    <Button onClick={() => editEmpHandler(item)}>
                        <FontAwesome icon="edit" />
                        {' Chỉnh Sửa'}
                    </Button>
                </td>
            </tr>
        );
    });

    const paginateClick = (page) => {
        paginateHandler(page);
    }

    return (
        <Fragment>
            <Button className="success" onClick={addNewEmployee}>
                <FontAwesome icon="user-plus" />
                {' Thêm Mới Nhân Viên'}
            </Button>
            <Table
                title="Danh Sách Nhân Viên"
                headers={headers}
                body={body}
            />
            <Pagination currentPage={currentPage} listPerPage={perPage} totalList={props.listEmp.length} paginateClick={paginateClick} />
        </Fragment>

    );
}

export default EmployeeList;