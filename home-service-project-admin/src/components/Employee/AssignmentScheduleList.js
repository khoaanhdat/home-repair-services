import { Fragment, useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import classes from './AssignmentScheduleList.module.css';
import Table from '../UI/Table';
import Button from '../UI/Button';
import { usePagination } from '../../hooks/use-pagination';
import LoadingSpinner from '../UI/LoadingSpinner';
import { Pagination } from '../UI/Pagination';
import Modal from '../UI/Modal';
import useHttp from '../../hooks/use-http';
import AuthContext from '../../store/auth-context';
import { ReportWorkComplete } from '../../lib/api';
import { MessageContext } from '../../store/message-context';

const AssignmentScheduleList = (props) => {
    const [isShowConfirm, setIsShowConfirm] = useState(false);
    const [processId, setProcessId] = useState();
    const [customerNameReport, setCustomerNameReport] = useState();
    const [customerAddressReport, setCustomerAddressReport] = useState();

    const history = useHistory();
    const { token, onLogout } = useContext(AuthContext);
    const { addMessage } = useContext(MessageContext);

    const { sendRequest, status, error } = useHttp(ReportWorkComplete);

    useEffect(() => {
        if (status === 'completed' && !error) {
            setIsShowConfirm(false);
            props.reloadHandler();
            addMessage({
                message: "Báo cáo công việc thành công.",
                className: "success"
            });
        }

        if (error) {
            if (error === 'Unauthorized') {
                setTimeout(() => {
                    history.push('/');
                    onLogout();
                    addMessage({ auth: true });
                }, 100);
            } else {
                addMessage({
                    message: error,
                    className: "error"
                });
            }
        }
    }, [status])

    const perPage = 10;
    const {
        currentList: currentAssignmentSchedule,
        currentPage,
        paginateHandler
    } = usePagination(1, perPage, props.listSchedule);

    if (!currentAssignmentSchedule || currentAssignmentSchedule.length === 0) {
        return <LoadingSpinner />
    }

    const paginateClick = (page) => {
        paginateHandler(page);
    }

    const reportWorkComplete = (propcessId, cusName, cusAddress) => {
        setIsShowConfirm(true);
        setProcessId(propcessId);
        setCustomerNameReport(cusName);
        setCustomerAddressReport(cusAddress);
    }

    const confirmHandler = () => {
        sendRequest({
            processId,
            token
        });
    }

    const hideConfirm = () => {
        setIsShowConfirm(false);
    }

    const headers = ["Tên khách hàng", "Địa chỉ", "Số điện thoại", "Dịch vụ", "Thời gian", "Trạng thái", "Báo cáo hoàn thành"];

    const body = currentAssignmentSchedule.map((item, key) => {
        return (
            <tr key={key}>
                <td>{item.cus_Name}</td>
                <td>{item.address}</td>
                <td>{item.cus_Phone}</td>
                <td>{item.service_Name}</td>
                <td>{item.approve_Time}</td>
                <td>
                    {item.status ? <span style={{ color: "green" }}>Hoàn thành</span> : <span style={{ color: "red" }}>Chưa hoàn thành</span>}
                </td>
                <td>
                    {item.status ? <span>Đã báo cáo </span> :
                        <Button
                            onClick={() => reportWorkComplete(item.res_Service_Emp_Id, item.cus_Name, item.address)}>
                            Báo Cáo
                    </Button>}
                </td>
            </tr>
        );
    });

    return (
        <Fragment>
            <Table title="Lịch phân công" customClass="schedule" headers={headers} body={body} />
            <Pagination currentPage={currentPage} listPerPage={perPage} totalList={props.listSchedule.length} paginateClick={paginateClick} />
            {isShowConfirm && <Modal title='Xác nhận hoàn thành công việc này' onClose={hideConfirm}>
                <div className={classes['confirm-info']}>
                    <span>Tên khách hàng: {customerNameReport}</span>
                    <span>Địa chỉ: {customerAddressReport}</span>
                    {!status && <Button onClick={confirmHandler}>Xác Nhận</Button>}
                    {status === 'pending' && <LoadingSpinner />}
                </div>
            </Modal>}
        </Fragment>
    );
}

export default AssignmentScheduleList;