import {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from "react";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import Select from "react-select";

import classes from "./Profile.module.css";
import Input from "../UI/Input";
import InputAddress from "../UI/InputAddress";
import Calendar from "../UI/Calendar";
import Button from "../UI/Button";
import useHttp from "../../hooks/use-http";
import { UpdateEmployee, GetAllCompany } from "../../lib/api";
import { useHistory } from "react-router";
import AuthContext from "../../store/auth-context";
import { MessageContext } from "../../store/message-context";
import LoadingSpinner from "../UI/LoadingSpinner";

const Profile = (props) => {
  const {
    Last_Name,
    First_Name,
    Emp_Description,
    Emp_Address,
    Location,
    Emp_Sex,
    Emp_Avaible,
    Phone,
    Emp_Avt,
    Password,
    Emp_Birthday,
    Company_Id
  } = props.empDetail;


  // Handle upload file image
  const [imagePreview, setImagePreview] = useState(Emp_Avt);
  const [selectedImage, setSelectedImage] = useState(null);
  const fileInputRef = useRef();

  const [isUpdate, setIsUpdate] = useState(false);

  const [empAddress, setEmpAddress] = useState(Emp_Address);
  const [empLocation, setEmpLocation] = useState(Location);
  const [sex, setSex] = useState(Emp_Sex);
  const [available, setAvailable] = useState(Emp_Avaible);
  const [isShowCalendar, setIsShowCalendar] = useState(false);
  const [birthDay, setBirthDay] = useState(new Date(Emp_Birthday));

  const firstNameRef = useRef(First_Name);
  const lastNameRef = useRef(Last_Name);
  const descriptionRef = useRef(Emp_Description);
  const phoneRef = useRef(Phone);
  const passwordRef = useRef(Password);
  const repeatPasswordRef = useRef(Password);

  const handleChanged = (address) => {
    setEmpAddress(address);
  };

  const handleSelected = useCallback((address) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        setEmpAddress(address);

        console.log(latLng);

        let latLngArr = [];
        latLngArr[0] = latLng.lat;
        latLngArr[1] = latLng.lng;
        setEmpLocation(JSON.stringify(latLngArr));
      })
      .catch((error) => console.log("Error", error));
  }, []);

  const focusHandler = (e) => {
    setIsShowCalendar(true);
  };

  const birthDayChangeHandler = (value, event) => {
    setBirthDay(value);

    setTimeout(() => {
      setIsShowCalendar(false);
    }, 200);
  };

  const onChangeSex = (e) => {
    setSex(e.target.value);
  };

  const onChangeAvailable = (e) => {
    setAvailable(e.target.value);
  };

  const updateClick = () => {
    setIsUpdate(true);
  };

  const { sendRequest, status, error } = useHttp(UpdateEmployee);

  const history = useHistory();
  const { token, onLogout, employeeId } = useContext(AuthContext);
  const { addMessage } = useContext(MessageContext);

  const [companyId, setCompanyId] = useState(Company_Id);
  const saveInfoHandler = async (e) => {
    e.preventDefault();

    let imageUrl;
    if (selectedImage === null) {
      imageUrl = Emp_Avt;
    } else {
      const fd = new FormData();
      fd.append("file", selectedImage, selectedImage.name);

      const response = await fetch(
        "https://home-repair-api.herokuapp.com/uploadFile",
        {
          method: "POST",
          body: fd,
        }
      );

      const data = await response.json();
      imageUrl = data.fileDownloadUri;
    }

    sendRequest({
      dataUpdate: {
        Emp_Id: employeeId,
        Last_Name: lastNameRef.current.value,
        First_Name: firstNameRef.current.value,
        Description: descriptionRef.current.value,
        Address: empAddress,
        Location: empLocation,
        Sex: sex,
        Phone: phoneRef.current.value,
        Available: available,
        BirthDay: birthDay,
        Password: passwordRef.current.value,
        Avatar: imageUrl,
        Company_Id: companyId,
      },
      token,
    });
  };

  useEffect(() => {
    if (status === "completed" && !error) {
      addMessage({
        message: "Cập nhật thông tin thành công.",
        className: "success",
      });
    }

    if (error) {
      if (error === "Unauthorized") {
        setTimeout(() => {
          history.push("/");
          onLogout();
          addMessage({ auth: true });
        }, 100);
      } else if (error === "Company is invalid") {
        addMessage({
          message: "Bạn chưa chọn công ty.",
          className: "error",
        });
      } else {
        addMessage({
          message: error,
          className: "error",
        });
      }
    }
  }, [status]);

  let listCompanyGenerate;

  const {
    sendRequest: getAllCompany,
    data: listCompany,
    status: statusGetAllCompany,
    error: errorGetAllCompany,
  } = useHttp(GetAllCompany, true);

  useEffect(() => {
    getAllCompany({ token });
  }, [getAllCompany]);

  const chooseCompany = (companySelected) => {
    setCompanyId(companySelected.value);
  };

  if (statusGetAllCompany === "pending") {
    listCompanyGenerate = <LoadingSpinner />;
  } else if (errorGetAllCompany) {
    if (errorGetAllCompany === "Unauthorized") {
      setTimeout(() => {
        history.push("/");
        onLogout();
        addMessage({ auth: true });
      }, 100);
      return false;
    } else {
      listCompanyGenerate = <span>{errorGetAllCompany}</span>;
    }
  } else if (
    statusGetAllCompany === "completed" &&
    (!listCompany || listCompany.length === 0)
  ) {
    return <span>Không có công ty nào. Xin vui lòng thử lại sau.</span>;
  } else {
    const options = listCompany.map((item) => {
      return { value: item.Company_Id, label: item.Company_Name };
    });

    listCompanyGenerate = (
      <Select required options={options} onChange={chooseCompany} />
    );
  }

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImagePreview((prevState) => (prevState = URL.createObjectURL(img)));
      setSelectedImage(img);
    }
  };

  const pickImageHandler = (e) => {
    e.preventDefault();
    fileInputRef.current.click();
  };

  return (
    <form onSubmit={saveInfoHandler} className={classes.profile}>
      <div className={classes.avatar}>
        <img src={imagePreview} alt={First_Name} />
        {isUpdate && (
          <Fragment>
            <input
              type="file"
              name="myImage"
              label="Chọn hình ảnh"
              required={false}
              onChange={onImageChange}
              ref={fileInputRef}
              style={{ display: "none" }}
            />
            <Button className="pick-image" onClick={pickImageHandler}>
              Chọn ảnh
            </Button>
          </Fragment>
        )}
      </div>
      <div className={classes.info}>
        <Input
          id="lastname"
          label="Tên họ"
          ref={lastNameRef}
          defaultValue={Last_Name}
          required={true}
          disabled={!isUpdate ? true : false}
        />

        <Input
          id="firstname"
          label="Tên"
          ref={firstNameRef}
          defaultValue={First_Name}
          required={true}
          disabled={!isUpdate ? true : false}
        />

        <Input
          id="des"
          label="Giới thiệu"
          ref={descriptionRef}
          defaultValue={Emp_Description}
          required={true}
          disabled={!isUpdate ? true : false}
        />

        <Input id="address" label="Địa chỉ" customClass="address">
          <InputAddress
            address={empAddress}
            handleChanged={handleChanged}
            handleSelected={handleSelected}
            disabled={!isUpdate ? true : false}
          />
        </Input>

        <Input id="sex" label="Giới tính">
          <select
            disabled={!isUpdate ? true : false}
            onChange={onChangeSex}
            id="sex"
            defaultValue={Emp_Sex}
          >
            <option value="0">Nữ</option>
            <option value="1">Nam</option>
          </select>
        </Input>

        <Input id="available" label="Trạng thái">
          <select
            disabled={!isUpdate ? true : false}
            onChange={onChangeAvailable}
            id="available"
            defaultValue={Emp_Address}
          >
            <option value="0">Cáo Bận</option>
            <option value="1">Có Mặt</option>
          </select>
        </Input>

        <Input
          id="phone"
          label="Điện thoại"
          ref={phoneRef}
          type="tel"
          defaultValue={Phone}
          required={true}
          disabled={true}
        />

        <Input id="birthday" label="Ngày Sinh">
          <input
            onFocus={focusHandler}
            id="birthday"
            type="text"
            value={birthDay.toLocaleDateString("en-GB")}
            disabled={!isUpdate ? true : false}
            onChange={() => {}}
          />
          <div className={classes.calendar}>
            {isShowCalendar && (
              <Calendar value={birthDay} onChange={birthDayChangeHandler} />
            )}
          </div>
        </Input>

        <Input
          id="password"
          label="Mật khẩu"
          type="password"
          ref={passwordRef}
          defaultValue={Password}
          required={true}
          disabled={!isUpdate ? true : false}
        />

        {isUpdate && (
          <Input
            id="repassword"
            label="Nhập lại mật khẩu"
            type="password"
            defaultValue={Password}
            ref={repeatPasswordRef}
            required={true}
          />
        )}

        {isUpdate && (
          <Input id="company" label="Chọn công ty làm việc">
            {listCompanyGenerate}
          </Input>
        )}
      </div>

      {!isUpdate && <Button onClick={updateClick}>Sửa Thông Tin</Button>}

      {isUpdate && (
        <Button type="submit" className="success">
          Lưu
        </Button>
      )}
    </form>
  );
};

export default Profile;
