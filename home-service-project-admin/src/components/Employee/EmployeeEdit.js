import { useCallback, useContext, useState, useRef, useEffect } from "react";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";
import { useHistory } from "react-router-dom";

import classes from "./EmployeeEdit.module.css";
import Modal from "../UI/Modal";
import EmployeeManagerContext from "../../store/employee-manager-context";
import Calendar from "../UI/Calendar";
import Button from "../UI/Button";
import InputAddress from "../UI/InputAddress";
import Input from "../UI/Input";
import useInput from "../../hooks/use-input";
import useHttp from "../../hooks/use-http";
import { UpdateEmployee } from "../../lib/api";
import LoadingSpinner from "../UI/LoadingSpinner";
import { MessageContext } from "../../store/message-context";
import FontAwesome from "../UI/FontAwesome";
import AuthContext from "../../store/auth-context";

const EmployeeEdit = (props) => {
  const [isShowCalendar, setIsShowCalendar] = useState(false);

  const [formIsTouched, setFormIsTouched] = useState(false);

  const empManagerCtx = useContext(EmployeeManagerContext);

  const closeEditEmp = () => {
    empManagerCtx.hideEditForm();
  };

  const { dataEditEmp: empEdit } = empManagerCtx.editEmpState;

  const { empId } = empEdit;

  const convertedBirthDay = new Date(
    empEdit.birthDay.replace(/(\d{2})\/(\d{2})\/(\d{4})/, "$2/$1/$3")
  );

  const [birthDay, setBirthDay] = useState(new Date(convertedBirthDay));

  const [empLocation, setEmpLocation] = useState(empEdit.location);

  const [sex, setSex] = useState(empEdit.sex);

  const [available, setAvailable] = useState(empEdit.available);

  const descriptionRef = useRef(empEdit.description);

  const formTouchedHandler = () => {
    setFormIsTouched(true);
  };

  //Valid FirstName
  const {
    value: enteredFirstName,
    isValid: enteredFirstNameIsValid,
    hasError: firstNameInputHasError,
    messageError: firstNameMessageError,
    valueChanged: firstNameChangeHandler,
    valueBlur: firstNameBlurHandler,
    checkIsNotEmpty: firstNameCheckIsNotEmpty,
  } = useInput(empEdit.firstName);

  const firstNameOnChanged = (e) => {
    firstNameChangeHandler(e.target.value);
    firstNameCheckIsNotEmpty(e.target.value);
  };

  const firstNameOnBlur = () => {
    firstNameBlurHandler();
    firstNameCheckIsNotEmpty(enteredFirstName);
  };

  //Valid LastName
  const {
    value: enteredLastName,
    isValid: enteredLastNameIsValid,
    hasError: lastNameInputHasError,
    messageError: lastNameMessageError,
    valueChanged: lastNameChangeHandler,
    valueBlur: lastNameBlurHandler,
    checkIsNotEmpty: lastNameCheckIsNotEmpty,
  } = useInput(empEdit.lastName);

  const lastNameOnChanged = (e) => {
    lastNameChangeHandler(e.target.value);
    lastNameCheckIsNotEmpty(e.target.value);
  };

  const lastNameOnBlur = () => {
    lastNameBlurHandler();
    lastNameCheckIsNotEmpty(enteredLastName);
  };

  // Valid Phone Number
  const {
    value: enteredPhone,
    isValid: enteredPhoneIsValid,
    hasError: phoneInputHasError,
    messageError: phoneMessageError,
    valueChanged: phoneChangeHandler,
    valueBlur: phoneBlurHandler,
    checkIsNotEmpty: phoneCheckIsNotEmpty,
    checkIsNumber: phoneCheckIsNumber,
  } = useInput(empEdit.phone.replaceAll(".", ""));

  const phoneOnChanged = (event) => {
    phoneChangeHandler(event.target.value);
    phoneCheckIsNotEmpty(event.target.value);
    phoneCheckIsNumber(event.target.value);
  };

  const phoneOnBlur = () => {
    phoneBlurHandler();
    phoneCheckIsNotEmpty(enteredPhone);
    phoneCheckIsNumber(enteredPhone);
  };

  //Valid Address
  const {
    value: enteredAddress,
    isValid: enteredAddressIsValid,
    hasError: addressInputHasError,
    messageError: addressMessageError,
    valueChanged: addressChangeHandler,
    valueBlur: addressBlurHandler,
    checkIsNotEmpty: addressCheckIsNotEmpty,
  } = useInput(empEdit.address);

  const addressOnChanged = (address) => {
    addressChangeHandler(address);
    addressCheckIsNotEmpty(address);
  };

  const addressOnBlur = () => {
    addressBlurHandler();
    addressCheckIsNotEmpty(enteredAddress);
  };

  // Send request to server
  const {
    sendRequest: updateEmployee,
    status: statusUpdateEmployee,
    error: errorUpdateEmployee,
  } = useHttp(UpdateEmployee);

  // Add message box
  const { addMessage } = useContext(MessageContext);
  const history = useHistory();
  const { token, onLogout } = useContext(AuthContext);

  useEffect(() => {
    if (statusUpdateEmployee === "completed") {
      props.reload();
      empManagerCtx.hideEditForm();
      addMessage({
        message: "Cập nhật nhân viên thành công.",
        className: "success",
      });
    }

    if (errorUpdateEmployee) {
      if (errorUpdateEmployee === "Unauthorized") {
        setTimeout(() => {
          history.push("/");
          onLogout();
          addMessage({ auth: true });
        }, 100);
      } else {
        addMessage({
          message: errorUpdateEmployee,
          className: "error",
        });
      }
    }
  }, [
    statusUpdateEmployee,
    addMessage,
    props,
    empManagerCtx,
    errorUpdateEmployee,
  ]);

  // Check form is valid
  let formIsValid = false;
  if (
    enteredFirstNameIsValid &&
    enteredLastNameIsValid &&
    enteredPhoneIsValid &&
    enteredAddressIsValid
  ) {
    formIsValid = true;
  }

  const confirmEditProfile = (event) => {
    event.preventDefault();
    if (formIsValid) {
      updateEmployee({
        dataUpdate: {
          Emp_Id: empId,
          Last_Name: enteredLastName,
          First_Name: enteredFirstName,
          Description: descriptionRef.current.value,
          Address: enteredAddress,
          Location: empLocation,
          Sex: sex,
          Phone: enteredPhone,
          Available: available,
          BirthDay: birthDay.toLocaleDateString("en-GB"),
          Company_Id: props.companyId,
        },
        token,
      });
    }
  };

  const birthDayFocusHandler = (e) => {
    setIsShowCalendar(true);
    setFormIsTouched(true);
  };

  const birthDayChangeHandler = (value, event) => {
    setBirthDay(value);

    setTimeout(() => {
      setIsShowCalendar(false);
    }, 200);
  };

  const handleSelected = useCallback((address) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        addressChangeHandler(address);

        let latLngArr = [];
        latLngArr[0] = latLng.lat;
        latLngArr[1] = latLng.lng;
        setEmpLocation(JSON.stringify(latLngArr));
      })
      .catch((error) => console.log("Error", error));
  }, []);

  const onChangeSex = (e) => {
    setSex(e.target.value);
    setFormIsTouched(true);
  };

  const onChangeAvailable = (e) => {
    setAvailable(e.target.value);
    setFormIsTouched(true);
  };

  return (
    <Modal
      customModalClass="edit-emp"
      customBackdropClass="edit-emp"
      title="Cập Nhật Nhân Viên"
      onClose={closeEditEmp}
    >
      <form onSubmit={confirmEditProfile} className={classes["profile"]}>
        <Input
          id="lastname"
          label="Tên họ"
          className={`${lastNameInputHasError ? "invalid" : ""}`}
          defaultValue={enteredLastName}
          valueInputHasError={lastNameInputHasError}
          messageError={lastNameMessageError}
          onChange={lastNameOnChanged}
          onBlur={lastNameOnBlur}
          onFocus={formTouchedHandler}
          required={true}
        />

        <Input
          id="firstname"
          label="Tên"
          className={`${firstNameInputHasError ? "invalid" : ""}`}
          defaultValue={enteredFirstName}
          valueInputHasError={firstNameInputHasError}
          messageError={firstNameMessageError}
          onChange={firstNameOnChanged}
          onBlur={firstNameOnBlur}
          onFocus={formTouchedHandler}
          required={true}
        />

        <Input
          id="des"
          label="Giới thiệu"
          ref={descriptionRef}
          defaultValue={empEdit.description}
          required={true}
        />

        <Input id="address" label="Địa chỉ" customClass="address">
          <InputAddress
            inputClassName={`${addressInputHasError ? "invalid" : ""}`}
            address={enteredAddress}
            valueInputHasError={addressInputHasError}
            messageError={addressMessageError}
            handleChanged={addressOnChanged}
            handleSelected={handleSelected}
            onBlur={addressOnBlur}
            onFocus={formTouchedHandler}
          />
        </Input>

        <Input id="sex" label="Giới tính">
          <select onChange={onChangeSex} id="sex" defaultValue={empEdit.sex}>
            <option value="0">Nữ</option>
            <option value="1">Nam</option>
          </select>
        </Input>

        <Input id="available" label="Trạng thái">
          <select
            onChange={onChangeAvailable}
            id="available"
            defaultValue={empEdit.available}
          >
            <option value="0">Tạm dừng</option>
            <option value="1">Hoạt động</option>
          </select>
        </Input>

        <Input
          id="phone"
          label="Điện thoại"
          className={`${phoneInputHasError ? "invalid" : ""}`}
          defaultValue={enteredPhone}
          valueInputHasError={phoneInputHasError}
          messageError={phoneMessageError}
          onChange={phoneOnChanged}
          onBlur={phoneOnBlur}
          onFocus={formTouchedHandler}
          required={true}
        />

        <Input id="birthday" label="Ngày Sinh">
          <input
            onFocus={birthDayFocusHandler}
            id="birthday"
            type="text"
            value={birthDay.toLocaleDateString("en-GB")}
            onChange={() => {}}
          />
          <div className={classes.calendar}>
            {isShowCalendar && (
              <Calendar value={birthDay} onChange={birthDayChangeHandler} />
            )}
          </div>
        </Input>

        {!statusUpdateEmployee && (
          <Button
            type="submit"
            className="warning"
            disabled={!(formIsValid && formIsTouched)}
          >
            <FontAwesome icon="check" />
            {" Cập Nhật"}
          </Button>
        )}
        {statusUpdateEmployee === "pending" && <LoadingSpinner />}
      </form>
    </Modal>
  );
};

export default EmployeeEdit;
