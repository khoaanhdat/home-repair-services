import { useCallback, useContext, useEffect, useRef, useState } from "react";
import { geocodeByAddress, getLatLng } from "react-places-autocomplete";

import EmployeeManagerContext from "../../store/employee-manager-context";
import Modal from "../UI/Modal";
import Input from "../UI/Input";
import InputAddress from "../UI/InputAddress";
import Button from "../UI/Button";
import Calendar from "../UI/Calendar";
import classes from "./NewEmployee.module.css";
import useHttp from "../../hooks/use-http";
import { SignUp } from "../../lib/api";
import LoadingSpinner from "../UI/LoadingSpinner";
import { MessageContext } from "../../store/message-context";
import FontAwesome from "../UI/FontAwesome";

const NewEmployee = (props) => {
  const [isShowCalendar, setIsShowCalendar] = useState(false);

  const lastNameRef = useRef(null);
  const firstNameRef = useRef(null);
  const descriptionRef = useRef(null);
  const phoneRef = useRef(null);
  const passwordRef = useRef(null);

  const [address, setAddress] = useState("");
  const [location, setLocation] = useState(null);
  const [sex, setSex] = useState(null);
  const [available, setAvailable] = useState(null);
  const [birthDay, setBirthDay] = useState(new Date());

  const empManagerCtx = useContext(EmployeeManagerContext);

  const closeNewEmp = () => {
    empManagerCtx.hideNewForm();
  };

  // Add new Employee
  const { sendRequest, status: statusAddNewEmployee, error } = useHttp(SignUp);

  const confirmAddNewEmployee = (e) => {
    e.preventDefault();

    sendRequest({
      Last_Name: lastNameRef.current.value,
      First_Name: firstNameRef.current.value,
      Description: descriptionRef.current.value,
      Address: address,
      Location: location,
      Sex: sex,
      Phone: phoneRef.current.value,
      Available: available,
      Birth_Date: birthDay.toLocaleDateString("en-GB"),
      Password: passwordRef.current.value,
      Role: 2,
      Company_Id: props.companyId,
    });
  };

  //Add message box
  const { addMessage } = useContext(MessageContext);
  useEffect(() => {
    if (statusAddNewEmployee === "completed" && !error) {
      props.reloadData();
      empManagerCtx.hideNewForm();
      addMessage({
        message: "Thêm nhân viên thành công.",
        className: "success",
      });
    }
  }, [statusAddNewEmployee, addMessage, empManagerCtx]);

  const onChangeSex = (e) => {
    setSex(e.target.value);
  };

  const onChangeAvailable = (e) => {
    setAvailable(e.target.value);
  };

  const focusHandler = (e) => {
    setIsShowCalendar(true);
  };

  const birthDayChangeHandler = (value, event) => {
    setBirthDay(value);

    setTimeout(() => {
      setIsShowCalendar(false);
    }, 200);
  };

  const handleChanged = (address) => {
    setAddress(address);
  };

  const handleSelected = useCallback((address) => {
    geocodeByAddress(address)
      .then((results) => getLatLng(results[0]))
      .then((latLng) => {
        setAddress(address);
        let latLngArr = [];
        latLngArr[0] = latLng.lat;
        latLngArr[1] = latLng.lng;
        setLocation(JSON.stringify(latLngArr));
      })
      .catch((error) => console.log("Error", error));
  }, []);

  return (
    <Modal
      title="Thêm Mới Nhân Viên"
      customModalClass="new-emp"
      customBackdropClass="new-emp"
      onClose={closeNewEmp}
    >
      <form onSubmit={confirmAddNewEmployee} className={classes["profile"]}>
        <Input
          id="lastname"
          label="Tên họ"
          placeholder="Nhập tên họ"
          ref={lastNameRef}
          required={true}
        />

        <Input
          id="firstname"
          label="Tên"
          placeholder="Nhập tên"
          ref={firstNameRef}
          required={true}
        />

        <Input
          id="des"
          label="Giới thiệu"
          placeholder="Nhập giới thiệu"
          ref={descriptionRef}
          required={true}
        />

        <Input id="address" label="Địa chỉ" customClass="address">
          <InputAddress
            className="profile"
            address={address}
            handleChanged={handleChanged}
            handleSelected={handleSelected}
          />
        </Input>

        <Input id="sex" label="Giới tính">
          <select onChange={onChangeSex} id="sex" defaultValue={""} required>
            <option disabled value="">
              Chọn giới tính
            </option>
            <option value="0">Nữ</option>
            <option value="1">Nam</option>
          </select>
        </Input>

        <Input id="available" label="Trạng thái">
          <select
            onChange={onChangeAvailable}
            id="available"
            defaultValue={""}
            required
          >
            <option disabled value="">
              Chọn trạng thái
            </option>
            <option value="0">Cáo Bận</option>
            <option value="1">Có Mặt</option>
          </select>
        </Input>

        <Input
          id="phone"
          label="Điện thoại"
          placeholder="Nhập điện thoại"
          ref={phoneRef}
          required={true}
        />

        <Input
          id="password"
          type="password"
          label="Mật khẩu"
          placeholder="Nhập mật khẩu"
          ref={passwordRef}
          required={true}
        />

        <Input id="birthday" label="Ngày Sinh">
          <input
            onFocus={focusHandler}
            id="birthday"
            type="text"
            value={birthDay.toLocaleDateString("en-GB")}
            onChange={() => {}}
          />
          <div className={classes.calendar}>
            {isShowCalendar && (
              <Calendar value={birthDay} onChange={birthDayChangeHandler} />
            )}
          </div>
        </Input>
        {error && <p className={classes.error}>{error}</p>}
        {statusAddNewEmployee !== "pending" && (
          <Button type="submit" className="success">
            <FontAwesome icon="check" />
            {" Thêm Mới"}
          </Button>
        )}
        {statusAddNewEmployee === "pending" && <LoadingSpinner />}
      </form>
    </Modal>
  );
};

export default NewEmployee;
