import { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import AuthContext from '../../store/auth-context';
import { MessageContext } from '../../store/message-context';
import useInput from '../../hooks/use-input';
import useHttp from '../../hooks/use-http';
import { LogIn } from '../../lib/api';
import classes from './Login.module.css';
import LoadingSpinner from '../UI/LoadingSpinner';

const Login = (props) => {
    // Phone Number
    const {
        value: enteredPhone,
        isValid: enteredPhoneIsValid,
        hasError: phoneInputHasError,
        messageError: phoneMessageError,
        valueChanged: phoneChangeHandler,
        valueBlur: phoneBlurHandler,
        checkIsNotEmpty: phoneCheckIsNotEmpty,
        checkIsNumber: phoneCheckIsNumber,
        checkIsPhoneNumber
    } = useInput();

    const phoneOnChanged = (event) => {
        phoneChangeHandler(event.target.value);
        phoneCheckIsNotEmpty(event.target.value);
        phoneCheckIsNumber(event.target.value);
        checkIsPhoneNumber(event.target.value);
    }

    const phoneOnBlur = () => {
        phoneBlurHandler();
        phoneCheckIsNotEmpty(enteredPhone);
        phoneCheckIsNumber(enteredPhone);
        checkIsPhoneNumber(enteredPhone);
    }

    //Validate Password
    const {
        value: enteredPassword,
        isValid: enteredPasswordIsValid,
        hasError: passwordInputHasError,
        messageError: passwordMessageError,
        valueChanged: passwordChangeHandler,
        valueBlur: passwordBlurHandler,
        checkIsNotEmpty: passwordCheckIsNotEmpty,
    } = useInput();

    const passwordOnChanged = (event) => {
        passwordChangeHandler(event.target.value);
        passwordCheckIsNotEmpty(event.target.value);
    }

    const passwordOnBlur = () => {
        passwordBlurHandler(enteredPassword);
        passwordCheckIsNotEmpty(enteredPassword);
    }

    //Valid Form
    let formIsValid = false;
    if (enteredPasswordIsValid && enteredPhoneIsValid) {
        formIsValid = true;
    }

    const { onLogin } = useContext(AuthContext);

    //Handler send request Login to Server
    const { sendRequest: logInRequest, data, error, status } = useHttp(LogIn);

    const history = useHistory();

    //Show message Box
    const { addMessage } = useContext(MessageContext);

    //Submit login
    const logInHandler = () => {
        if (formIsValid) {
            logInRequest({
                Phone: enteredPhone,
                Password: enteredPassword
            });
        }
    }

    useEffect(() => {
        if (status === 'completed' && !error) {
            if (data.roleId === 1 || data.roleId === 2) {
                onLogin({
                    token: data.idToken, 
                    expiresIn: data.expiresIn,
                    role: data.roleId, 
                    companyId: data.companyId ?? null,
                    empId: data.employeeId ?? null
                });

                history.replace('/');
                addMessage({
                    message: "Đăng nhập thành công.",
                    className: "success"
                });

            } else {
                addMessage({
                    message: "Bạn không có quyền sử dụng tài khoản này. Xin vui lòng kiểm tra lại.",
                    className: "error"
                });
            }
        }
    }, [status, error, onLogin, data, history]);

    return (
        <div className={`${classes["sign-in-htm"]} ${props.show ? classes.show : ''}`}>
            <div className={classes.group}>
                <label htmlFor="phone" className={classes.label}>Số điện thoại</label>
                <input
                    id="phone"
                    type="text"
                    className={`${classes.input} ${phoneInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredPhone}
                    onChange={phoneOnChanged}
                    onBlur={phoneOnBlur}
                />
                {phoneInputHasError && <p className={classes.error}>{phoneMessageError}</p>}
            </div>

            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Mật khẩu</label>
                <input
                    id="pass"
                    type="password"
                    className={`${classes.input} ${passwordInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredPassword}
                    onChange={passwordOnChanged}
                    onBlur={passwordOnBlur}
                />
                {passwordInputHasError && <p className={classes.error}>{passwordMessageError}</p>}
            </div>
            {error && <p className={classes.error}>{error}</p>}
            <div className={classes.group}>
                {status !== 'pending' && (
                    <input
                        disabled={!formIsValid}
                        onClick={logInHandler}
                        type="button"
                        className={classes.button}
                        defaultValue="Đăng Nhập"
                    />
                )}
                {status === 'pending' && <LoadingSpinner />}
            </div>
        </div>
    );
}

export default Login;