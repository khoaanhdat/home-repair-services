import { Fragment, useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import classes from './StatisticsForm.module.css';
import Input from '../UI/Input';
import Button from '../UI/Button';
import Table from '../UI/Table';
import FontAwesome from '../UI/FontAwesome';
import Chart from '../UI/Chart';
import { usePagination } from '../../hooks/use-pagination';
import LoadingSpinner from '../UI/LoadingSpinner';
import { Pagination } from '../UI/Pagination';
import useHttp from '../../hooks/use-http';
import { RevenueStatisticsByCompanyId, WorkStatisticsByCompanyId } from '../../lib/api';
import AuthContext from '../../store/auth-context';
import { MessageContext } from '../../store/message-context';

const StatisticsForm = (props) => {
    const { companyId, token, onLogout } = useContext(AuthContext);

    const [type, setType] = useState(null);

    const [isShowResult, setIsShowResult] = useState(false);

    const [fromMonth, setFromMonth] = useState('1');
    const [toMonth, setToMonth] = useState('1');
    const [fromMonthYear, setFromMonthYear] = useState('2021');
    const [toMonthYear, setToMonthYear] = useState('2021');

    const [yearQuarter, setYearQuarter] = useState('2021');

    const [fromYear, setFromYear] = useState('2021');
    const [toYear, setToYear] = useState('2021');

    const onChangeType = (e) => {
        setType(e.target.value);
        setIsShowResult(false);
    }

    const month = (state, changeFn, id) => {
        return (
            <select id={id} defaultValue={state} onChange={changeFn}>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
            </select>
        );
    }

    const years = (state, changeFn) => {
        return (
            <select defaultValue={state} onChange={changeFn}>
                <option value="2016">2016</option>
                <option value="2017">2017</option>
                <option value="2018">2018</option>
                <option value="2019">2019</option>
                <option value="2020">2020</option>
                <option value="2021">2021</option>
                <option value="2022">2022</option>
            </select>
        );
    }

    const fromMonthChange = (e) => {
        setFromMonth(e.target.value);
    }

    const toMonthChange = (e) => {
        setToMonth(e.target.value);
    }

    const fromMonthYearChange = (e) => {
        setFromMonthYear(e.target.value);
    }

    const toMonthYearChange = (e) => {
        setToMonthYear(e.target.value);
    }

    const yearQuarterChange = (e) => {
        setYearQuarter(e.target.value);
    }

    const fromYearChange = (e) => {
        setFromYear(e.target.value);
    }

    const toYearChange = (e) => {
        setToYear(e.target.value);
    }

    let headerFirst;

    const byStatistic = () => {
        if (type === '1') {
            headerFirst = 'Tháng';
            return (
                <Fragment>
                    <span className={classes.label}>Thống kê theo tháng</span>
                    <div>
                        <span>Từ: </span>
                        {month(fromMonth, fromMonthChange, "fromMonth")} {years(fromMonthYear, fromMonthYearChange)}
                        <span> - </span>
                        {month(toMonth, toMonthChange, "toMonth")} {years(toMonthYear, toMonthYearChange)}
                    </div>
                </Fragment>
            );
        } else if (type === '2') {
            headerFirst = 'Quý';
            return (
                <Fragment>
                    <span className={classes.label}>Thống kê theo qúy</span>
                    <div>
                        {years(yearQuarter, yearQuarterChange)}
                    </div>
                </Fragment>
            );
        } else if (type === '3') {
            headerFirst = 'Năm';
            return (
                <Fragment>
                    <span className={classes.label}>Thống kê theo năm</span>
                    <div>
                        <span>Từ: </span>
                        {years(fromYear, fromYearChange)}
                        <span> - </span>
                        {years(toYear, toYearChange)}
                    </div>
                </Fragment>
            );
        }
    }

    const history = useHistory();
    const { addMessage } = useContext(MessageContext);

    // Handle send request get WorkStatistics
    const {
        sendRequest: getWorkStatistics,
        data: resultsWorkStatistics,
        error: errorWorkStatistics,
        status: statusWorkStatistics
    } = useHttp(WorkStatisticsByCompanyId, true);


    // Handle send request get RevenueStatistics
    const {
        sendRequest: getRevenueStatistics,
        data: resultsRevenueStatistics,
        error: errorRevenueStatistics,
        status: statusRevenueStatistics
    } = useHttp(RevenueStatisticsByCompanyId, true);

    const statisticSubmit = (e) => {
        e.preventDefault();
        let fromDate, toDate;

        if (type === '1') {
            if (fromMonthYear === toMonthYear) {
                if (fromMonth > toMonth) {
                    document.getElementById('fromMonth').focus();
                    return;
                }
            }

            if (fromMonthYear > toMonthYear) {
                document.getElementById('fromMonth').focus();
                return;
            }

            fromDate = fromMonth + "/" + fromMonthYear;
            toDate = toMonth + "/" + toMonthYear;
        } else if (type === '2') {
            fromDate = null;
            toDate = yearQuarter;
        } else if (type === '3') {
            fromDate = fromYear;
            toDate = toYear;
        }

        getRevenueStatistics({
            companyId,
            fromDate,
            toDate,
            token
        });

        getWorkStatistics({
            companyId,
            fromDate,
            toDate,
            token
        });

        setIsShowResult(true);
    };

    let resultsRevenueStatisticsGeneration;
    if (statusRevenueStatistics === 'pending') {
        resultsRevenueStatisticsGeneration = <LoadingSpinner />;
    } else if (errorRevenueStatistics) {
        if (errorRevenueStatistics === 'Unauthorized') {
            setTimeout(() => {
                history.push('/');
                onLogout();
                addMessage({ auth: true });
            }, 500);
        } else {
            resultsRevenueStatisticsGeneration = <span>{errorRevenueStatistics}</span>;
        }
    } else if (statusRevenueStatistics === 'completed' && (!resultsRevenueStatistics || resultsRevenueStatistics.length === 0)) {
        resultsRevenueStatisticsGeneration = <span>Không thể xuất dữ liệu</span>
    } else {
        resultsRevenueStatisticsGeneration = resultsRevenueStatistics.map((resultItem, key) => {
            let dataAmountCustomer = [];
            let dataRevenue = [];
            let labelName = [];

            const labelTemp = Object.keys(resultItem.Result[0])[0]; console.log(resultItem);
            resultItem.Result.map((item) => {
                if (labelTemp === 'Month') {
                    labelName.push("Tháng " + item.Month);
                } else if (labelTemp === 'Quarter') {
                    labelName.push("Qúy " + item.Quarter)
                } else if (labelTemp === 'Year') {
                    labelName.push("Năm " + item.Year);
                }
                dataRevenue.push(item.Revenue);
                dataAmountCustomer.push(item.Amount_Customer)
            });

            return (
                <div key={key} className={classes.revenue}>
                    <span className={classes.title}>Năm {resultItem.Year}</span>
                    <Chart
                        title="Biếu Đồ Thống Kế Doanh Thu"
                        labels={labelName}
                        label="Doanh thu theo tháng (triệu đồng)"
                        dataArray={dataRevenue}
                    />
                    <br />
                    <Chart
                        title="Biếu Đồ Thống Kế Số Lượng Khách Hàng"
                        labels={labelName}
                        label="Số lượng khách hàng theo tháng (người)"
                        dataArray={dataAmountCustomer}
                    />
                    <br />
                    <br />
                </div>
            );
        });
    }


    //Add pagination
    const perPage = 5;
    const {
        currentList: currentResultEmployee,
        currentPage,
        paginateHandler
    } = usePagination(1, perPage, resultsWorkStatistics);

    let resultsWorkStatisticsGeneration;
    if (statusWorkStatistics === 'pending') {
        resultsWorkStatisticsGeneration = <LoadingSpinner />;
    } else if (errorWorkStatistics) {
        if (errorWorkStatistics === 'Unauthorized') {
            setTimeout(() => {
                history.push('/');
                onLogout();
                addMessage({ auth: true });
            }, 500);
        } else {
            resultsWorkStatisticsGeneration = <span>{errorWorkStatistics}</span>;
        }
    } else if (statusWorkStatistics === 'completed' && (!resultsWorkStatistics || resultsWorkStatistics.length === 0)) {
        resultsWorkStatisticsGeneration = <span>Không có dữ liệu để thống kê.</span>
    } else {
        if (!currentResultEmployee || currentResultEmployee.length === 0) {
            return <LoadingSpinner />
        }

        const paginateClick = (page) => {
            paginateHandler(page);
        }

        const bodyEmployeeWork = currentResultEmployee.map((item, key) => {
            return (
                <tr key={key}>
                    <td>{item.Emp_Name}</td>
                    <td>{item.Amount_Work}</td>
                </tr>
            );
        });

        resultsWorkStatisticsGeneration = (
            <Fragment>
                <Table
                    headers={['Tên nhân viên', 'Số lượng công việc hoàn thành']}
                    body={bodyEmployeeWork}
                    customClass="statistics"
                />
                <Pagination className="statistics" currentPage={currentPage} listPerPage={perPage} totalList={resultsWorkStatistics.length} paginateClick={paginateClick} />
            </Fragment>
        )
    }

    return (
        <div className={classes['statistics']}>
            <form onSubmit={statisticSubmit}>
                <span className={classes.title}>Thống Kê</span>
                <div className={classes.type}>
                    <Input label="Chọn loại thống kê" id="typeStatistics">
                        <select defaultValue={''} onChange={onChangeType} required>
                            <option disabled value="">Chọn loại thống kê</option>
                            <option value="1">Tháng</option>
                            <option value="2">Quý</option>
                            <option value="3">Năm</option>
                        </select>
                    </Input>
                </div>
                <div className={classes.date}>
                    {byStatistic()}
                </div>

                <div className={classes.actions}>
                    <Button type="submit">
                        <FontAwesome icon="search-dollar" />
                        Thống Kê
                    </Button>
                </div>
            </form>

            {isShowResult && (
                <div className={classes.results}>
                    {resultsRevenueStatisticsGeneration}

                    <div className={classes.employee}>
                        <span>Công Việc Nhân Viên</span>
                            {resultsWorkStatisticsGeneration}
                    </div>
                </div>
            )}
        </div>
    );
}

export default StatisticsForm;