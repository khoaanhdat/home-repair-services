import Table from '../UI/Table';
import classes from './PostList.module.css';
import Button from '../UI/Button';
import { Fragment, useContext, useEffect } from 'react';
import AssignEmployeeContext from '../../store/assign-employee-context';
import { usePagination } from '../../hooks/use-pagination';
import { Pagination } from '../UI/Pagination';
import LoadingSpinner from '../UI/LoadingSpinner';
import FontAwesome from '../UI/FontAwesome';

const PostList = (props) => {
    const perPage = 10;
    const {
        currentList: currentPosts,
        currentPage,
        paginateHandler
    } = usePagination(1, perPage, props.listPost);

    const assignEmpCtx = useContext(AssignEmployeeContext);

    const choosePostHandler = (postId, customerId, address, location, dateBooked, quantity) => {
        assignEmpCtx.choosePost(postId, customerId, address, location, dateBooked, quantity);
        assignEmpCtx.showPopup();
    }

    const headers = [
        "Stt",
        "Tên Khách Hàng",
        "Mô Tả",
        "Loại Dịch Vụ",
        "Định Lượng",
        "Địa Chỉ",
        "Ngày Đăng Ký",
        "Hành Động"
    ];

    if (!currentPosts || currentPosts.length === 0) {
        return <LoadingSpinner />
    }

    const body = currentPosts.map((item, key) => {
        return (
            <tr key={key}>
                <td>{key + 1}</td>
                <td>{item.Customer_Name}</td>
                <td>{item.Description}</td>
                <td>{item.Type_Name}</td>
                <td>{item.Quantity}</td>
                <td>{item.Address}</td>
                <td>{item.Book_Date}</td>
                <td>
                    <Button onClick={
                        () => choosePostHandler(
                            item.Post_Id,
                            item.Customer_Id,
                            item.Address,
                            item.Location,
                            item.Book_Date,
                            item.Quantity
                        )
                    }>
                        <FontAwesome icon="share-square" size="2x" color="#44b844" />
                    </Button>
                </td>
            </tr>
        );
    });

    const paginateClick = (page) => {
        paginateHandler(page);
    }

    return (
        <Fragment>
            <Table body={body} headers={headers} title="Danh Sách Bài Đăng"></Table>
            <Pagination currentPage={currentPage} listPerPage={perPage} totalList={props.listPost.length} paginateClick={paginateClick} />
        </Fragment>
    );
}

export default PostList;