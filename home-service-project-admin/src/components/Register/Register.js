import { useContext, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import useInput from '../../hooks/use-input';
import classes from './Register.module.css';
import { SignUp } from '../../lib/api';
import useHttp from '../../hooks/use-http';
import useOTP from '../../hooks/use-otp';
import Button from '../UI/Button';
import { MessageContext } from '../../store/message-context';
import LoadingSpinner from '../UI/LoadingSpinner';
import AuthContext from '../../store/auth-context';

const Register = (props) => {
    //Validate Company Name
    const {
        value: enteredCompany,
        isValid: enteredCompanyIsValid,
        hasError: companyInputHasError,
        messageError: companyMessageError,
        valueChanged: companyChangeHandler,
        valueBlur: companyBlurHandler,
        checkIsNotEmpty: companyCheckIsNotEmpty,
    } = useInput();

    const companyOnChanged = (event) => {
        companyChangeHandler(event.target.value);
        companyCheckIsNotEmpty(event.target.value);
    }

    const companyOnBlur = () => {
        companyBlurHandler(enteredCompany);
        companyCheckIsNotEmpty(enteredCompany);
    }

    // Phone Number
    const {
        value: enteredPhone,
        isValid: enteredPhoneIsValid,
        hasError: phoneInputHasError,
        messageError: phoneMessageError,
        valueChanged: phoneChangeHandler,
        valueBlur: phoneBlurHandler,
        checkIsNotEmpty: phoneCheckIsNotEmpty,
        checkIsNumber: phoneCheckIsNumber,
        checkIsPhoneNumber
    } = useInput();

    const phoneOnChanged = (event) => {
        phoneChangeHandler(event.target.value);
        phoneCheckIsNotEmpty(event.target.value);
        phoneCheckIsNumber(event.target.value);
        checkIsPhoneNumber(event.target.value);
    }

    const phoneOnBlur = () => {
        phoneBlurHandler();
        phoneCheckIsNotEmpty(enteredPhone);
        phoneCheckIsNumber(enteredPhone);
        checkIsPhoneNumber(enteredPhone);
    }

    // Validate OTP
    const phoneFormatted = enteredPhone.replace('0', '+84');

    const { otpIsValid, errorMessage, isSendOTP, otpOnChange, onGetOTPSubmit } = useOTP(phoneFormatted);

    //Validate Password
    const {
        value: enteredPassword,
        isValid: enteredPasswordIsValid,
        hasError: passwordInputHasError,
        messageError: passwordMessageError,
        valueChanged: passwordChangeHandler,
        valueBlur: passwordBlurHandler,
        checkIsNotEmpty: passwordCheckIsNotEmpty,
    } = useInput();

    const passwordOnChanged = (event) => {
        passwordChangeHandler(event.target.value);
        passwordCheckIsNotEmpty(event.target.value);
    }

    const passwordOnBlur = () => {
        passwordBlurHandler(enteredPassword);
        passwordCheckIsNotEmpty(enteredPassword);
    }

    // Validate Repeat Password
    const {
        value: enteredRePassword,
        isValid: enteredRePasswordIsValid,
        hasError: rePasswordInputHasError,
        messageError: rePasswordMessageError,
        valueChanged: rePasswordChangeHandler,
        valueBlur: rePasswordBlurHandler,
        checkIsNotEmpty: rePasswordCheckIsNotEmpty,
    } = useInput();

    const rePasswordOnChanged = (event) => {
        rePasswordChangeHandler(event.target.value);
        rePasswordCheckIsNotEmpty(event.target.value);
    }

    const rePasswordOnBlur = () => {
        rePasswordBlurHandler(enteredRePassword);
        rePasswordCheckIsNotEmpty(enteredRePassword);
    }

    //check match Password
    let rePasswordIsValid = false;
    if (enteredRePassword === enteredPassword) {
        rePasswordIsValid = true;
    }

    //Generate repassword message error
    let rePasswordErrorText = '';
    if (rePasswordInputHasError) {
        rePasswordErrorText = rePasswordMessageError;
    } else if (!rePasswordIsValid && enteredRePassword.length > 0) {
        rePasswordErrorText = 'Mật khẩu không trùng khớp';
    }

    //Validate Tax code
    const {
        value: enteredTax,
        isValid: enteredTaxIsValid,
        hasError: taxInputHasError,
        messageError: taxMessageError,
        valueChanged: taxChangeHandler,
        valueBlur: taxBlurHandler,
        checkIsNotEmpty: taxCheckIsNotEmpty,
    } = useInput();

    const taxOnChanged = (event) => {
        taxChangeHandler(event.target.value);
        taxCheckIsNotEmpty(event.target.value);
    }

    const taxOnBlur = () => {
        taxBlurHandler(enteredTax);
        taxCheckIsNotEmpty(enteredTax);
    }

    // Check form is valid
    var formIsValid = false;
    if (enteredCompanyIsValid &&
        enteredPhoneIsValid &&
        enteredPasswordIsValid &&
        enteredTaxIsValid &&
        otpIsValid &&
        (enteredRePasswordIsValid && rePasswordIsValid)) {
        formIsValid = true;
    }

    //Handler send server
    const { sendRequest: signUpRequest, data, status: statusSignUp, error } = useHttp(SignUp);
    const { onLogin } = useContext(AuthContext);
    const history = useHistory();

    const { addMessage } = useContext(MessageContext);

    useEffect(() => {
        if (statusSignUp === 'completed' && !error) {
            onLogin({
                token: data.idToken, 
                expiresIn: data.expiresIn,
                role: data.roleId, 
                companyId: data.companyId ?? null,
                empId: data.empId ?? null
            }); // 1 - Company
            history.replace('/');
            addMessage({message: "Đăng ký thành công", className: "success"});
        } else if (error) {
            addMessage({message: error, className: "error"});
            
            setTimeout(() => {
                history.go(0);
            }, 3000);
        }
    }, [statusSignUp, error]);

    //Confirm submit Register
    const registerSubmit = (e) => {
        e.preventDefault();
        if (formIsValid) {
            signUpRequest({
                Company_Name: enteredCompany,
                Tax_Code: enteredTax,
                Phone: enteredPhone,
                Password: enteredPassword,
                Role: 1 //Company
            });
        }
    }
    
    return (
        <form onSubmit={registerSubmit} className={`${classes["sign-up-htm"]} ${props.show ? classes.show : ''}`}>
            <div className={classes.group}>
                <label htmlFor="user" className={classes.label}>Tên công ty</label>
                <input
                    id="user"
                    type="text"
                    className={`${classes.input} ${companyInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredCompany}
                    onChange={companyOnChanged}
                    onBlur={companyOnBlur}
                />
                {companyInputHasError && <p className={classes.error}>{companyMessageError}</p>}
            </div>

            <div className={classes.group}>
                <label htmlFor="phone" className={classes.label}>Số điện thoại</label>
                <input
                    id="phone"
                    type="tel"
                    className={`${classes.input} ${phoneInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredPhone}
                    onChange={phoneOnChanged}
                    onBlur={phoneOnBlur}
                />
                {phoneInputHasError && <p className={classes.error}>{phoneMessageError}</p>}
            </div>

            <div className={`${classes.group} ${classes.otp}`}>
                <div id="get-otp-button"></div>
                <label htmlFor="phone" className={classes.label}>OTP</label>
                <div className={classes['input-otp']}>
                    <input
                        id="otp"
                        type="text"
                        className={`${classes.input}`}
                        disabled={(otpIsValid || !enteredPhoneIsValid)}
                        onChange={otpOnChange}
                    />
                    <p className={classes['error']}>{errorMessage}</p>
                </div>

                <Button onClick={onGetOTPSubmit} disabled={(isSendOTP || !enteredPhoneIsValid)}>Lấy OTP</Button>
            </div>

            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Mật khẩu</label>
                <input
                    id="pass"
                    type="password"
                    className={`${classes.input} ${passwordInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredPassword}
                    onChange={passwordOnChanged}
                    onBlur={passwordOnBlur}
                />
                {passwordInputHasError && <p className={classes.error}>{passwordMessageError}</p>}
            </div>

            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Nhập lại mật khẩu</label>
                <input
                    id="repass"
                    type="password"
                    className={`${classes.input} ${rePasswordErrorText ? classes.invalid : ''}`}
                    defaultValue={enteredRePassword}
                    onChange={rePasswordOnChanged}
                    onBlur={rePasswordOnBlur}
                />
                {rePasswordErrorText && <p className={classes.error}>{rePasswordErrorText}</p>}
            </div>

            <div className={classes.group}>
                <label htmlFor="tax" className={classes.label}>Mã số thuế</label>
                <input
                    id="tax"
                    type="text"
                    className={`${classes.input} ${taxInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredTax}
                    onChange={taxOnChanged}
                    onBlur={taxOnBlur}
                />
                {taxInputHasError && <p className={classes.error}>{taxMessageError}</p>}
            </div>
            {error && <p className={classes.error}>{error}</p>}
            {statusSignUp !== 'pending' && (
                <div className={classes.group}>
                    <input disabled={!formIsValid} type="submit" className={classes.button} value="Đăng Ký" onChange={() => { }} />
                </div>
            )}
            {statusSignUp === 'pending' && (
                <LoadingSpinner />
            )}

            <div className={classes.hr} />
            <div className={classes["foot-lnk"]}>
                <label htmlFor="tab-1">Bạn đã có tài khoản ?</label>
            </div>
        </form>
    );
}

export default Register;