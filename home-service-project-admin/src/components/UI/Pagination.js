import React, { useCallback } from 'react';

import classes from './Pagination.module.css';

export const Pagination = ({ listPerPage, totalList, paginateClick, currentPage, className }) => {
    const pageNumbers = [];

    for (let i = 1; i <= Math.ceil(totalList / listPerPage); i++) {
        pageNumbers.push(i);
    }

    const paginateClickHandler = useCallback((e, number) => {
        e.preventDefault();

        paginateClick(number);
    }, [paginateClick]);

    return (
        <div className={`${classes.pagination} ${className ? classes[className] : ''}`}>
            {pageNumbers.map(number => (
                <a key={number} onClick={(e) => paginateClickHandler(e, number)} href="#" className={`${currentPage === number ? classes.active : ''}`}>
                    {number}
                </a>
            ))}
        </div>
    )
}
