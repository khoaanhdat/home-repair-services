import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const FontAwesome = ({icon, size, pulse, color}) => {
    return (
        <FontAwesomeIcon icon={icon} size={size} pulse={pulse} color={color} />
    );
}

export default FontAwesome;
