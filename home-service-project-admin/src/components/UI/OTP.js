import React from 'react';

import Button from './Button.js';
import InputAuth from './InputAuth.js';
import classes from './OTP.module.css';

const OTP = (props) => {
    return (
        <div className={classes['otp-wrapper']}>            
            <InputAuth
                label="OTP"
                placeholder="Nhập OTP"
                input={{
                    id: "otp",
                    type: "number"
                }}
                onChange={props.onChange}
                disabled={props.inputDisabled}
            />
            <Button onClick={props.onClick} className="otp" disabled={props.disabled}>Lấy OTP</Button>
        </div>
    );
}

export default OTP;
