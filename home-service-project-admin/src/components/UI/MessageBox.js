import React, { Fragment } from 'react';

import classes from './MessageBox.module.css';

const MessageBox = ({ message, className, isShow }) => {
    const swal2Class = 'swal2-' + className;
    return (
        <div className={`${classes.message} ${classes[className]} ${isShow ? classes.show : ''}`}>
            <div className={`${classes['swal2-icon']} ${classes[swal2Class]} ${isShow ? classes['swal2-icon-show'] : ''}`}>
                {className === 'success' && (
                    <Fragment>
                        <div className={classes['swal2-' + className + '-circular-line-left']}></div>
                        <span className={classes['swal2-' + className + '-line-tip']}></span>
                        <span className={classes['swal2-' + className + '-line-long']}></span>
                        <div className={classes['swal2-' + className + '-ring']}></div>
                        <div className={classes['swal2-' + className + '-fix']}></div>
                        <div className={classes['swal2-' + className + '-circular-line-right']} style={{ backgroundColor: "white" }}></div>
                    </Fragment>
                )}
                {className === 'error' && (
                    <div className={classes["swal2-icon-content"]}>!</div>
                )}
            </div>
            <span>{message}</span>
        </div>
    );
}

export default MessageBox;
