import CalendarLib from 'react-calendar';

import './Calendar.css';

const Calendar = (props) => {
    return (
        <CalendarLib minDate={props.minDate} onChange={props.onChange} value={props.value} />
    );
}

export default Calendar;