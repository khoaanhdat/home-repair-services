import { forwardRef } from 'react';
import classes from './Input.module.css';

const Input = forwardRef((props, ref) => {
    return (
        <div className={`${classes['group-input']} ${props.customClass ? classes[props.customClass] : ''}`}>
            <label htmlFor={props.id}>{props.label}</label>
            {props.children ??
                <input
                    ref={ref}
                    id={props.id}
                    type={props.type ?? "text"}
                    defaultValue={props.defaultValue}
                    value={props.value}
                    pattern={props.pattern}
                    placeholder={props.placeholder}
                    required={props.required}
                    disabled={props.disabled}
                    onChange={props.onChange}
                    onBlur={props.onBlur}
                    onFocus={props.onFocus}
                    className={classes[props.className]}
                />
            }
            {props.valueInputHasError && <p className={classes.error}>{props.messageError}</p>}
        </div>
    );
});

export default Input;