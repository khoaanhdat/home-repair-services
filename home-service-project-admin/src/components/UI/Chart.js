import React from 'react';
import { Line } from 'react-chartjs-2';

import classes from './Chart.module.css';

const Chart = ({title, labels, label, dataArray}) => {
    const data = {
        labels: labels, //['January', 'February', 'March', 'April', 'May', 'June', 'July']
        datasets: [
            {
                label: label,
                fill: true,
                lineTension: 0.3,
                backgroundColor: 'rgba(75,192,192,0.4)',
                borderColor: 'rgba(75,192,192,1)',
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: 'rgb(252, 94, 16)',
                pointBackgroundColor: '#fff',
                pointBorderWidth: 5,
                pointHoverRadius: 10,
                pointHoverBackgroundColor: 'red',
                pointHoverBorderColor: 'rgba(220,220,220,1)',
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: dataArray //[1,2,3,4,5]
            }
        ]
    };

    return (
        <div className={classes.chart}>
            <h2>{title}</h2>
            <Line data={data} />
        </div>
    );
}

export default Chart;
