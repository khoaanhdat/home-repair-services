import { Fragment } from 'react';

import classes from './Table.module.css';

const Table = (props) => {
    const headerArr = props.headers.map((item, key) => {
        return (<th key={key}><h1>{item}</h1></th>);
    });

    return (
        <Fragment>
            <h1 className={classes.title}><span className={classes.blue}>{props.title}</span></h1>
            <table className={`${classes.container} ${props.customClass ? classes[props.customClass] : ''}`}>
                <thead>
                    <tr>
                        {headerArr}
                    </tr>
                </thead>
                <tbody>
                    {props.body}
                </tbody>
            </table>
        </Fragment>
    );
}

export default Table;