import { Fragment } from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';

import classes from './InputAddress.module.css';

const InputAddress = (props) => {
    const searchOptions = {
        location: new window.google.maps.LatLng(10.762622, 106.660172),
        radius: 1
    }

    const focusInput = (e) => {
        e.target.select();
        if (props.onFocus) {
            props.onFocus();
        }
    }

    return (
        <Fragment>
            <PlacesAutocomplete
                value={props.address}
                onChange={props.handleChanged}
                onSelect={props.handleSelected}
                searchOptions={searchOptions}
                shouldFetchSuggestions={props.address.length > 2}
                debounce={500}
            >
                {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                    <div className={`${classes['search-input']} ${classes[props.className]}`}>
                        <input
                            onFocus={focusInput}
                            {...getInputProps({
                                placeholder: 'Nhập địa chỉ ...'
                            })}
                            className={classes[props.inputClassName]}
                            disabled={props.disabled}
                        />

                        <div className={classes["autocomplete-dropdown-container"]}>
                            {loading && <div>Loading...</div>}
                            {suggestions.map(suggestion => {
                                const className = suggestion.active
                                    ? 'suggestion-item--active'
                                    : 'suggestion-item';
                                // inline style for demonstration purpose
                                const style = suggestion.active
                                    ? { backgroundColor: '#fafafa', cursor: 'pointer' }
                                    : { backgroundColor: '#ffffff', cursor: 'pointer' };
                                return (
                                    <div key={suggestion.placeId}
                                        {...getSuggestionItemProps(suggestion, {
                                            className,
                                            style,
                                        })}
                                    >
                                        <span>{suggestion.description}</span>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                )}
            </PlacesAutocomplete>
            {props.valueInputHasError && <p className={classes.error}>{props.messageError}</p>}
        </Fragment>
    );
}

export default InputAddress;