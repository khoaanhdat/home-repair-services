import React, { useContext, useEffect } from 'react';
import useHttp from '../../hooks/use-http';

import useInput from '../../hooks/use-input';
import useOTP from '../../hooks/use-otp';
import { SignUp } from '../../lib/api';
import classes from './RegisterEmployee.module.css';
import Button from '../UI/Button';
import LoadingSpinner from '../UI/LoadingSpinner';
import { MessageContext } from '../../store/message-context';

const RegisterEmployee = (props) => {
    //Validate Last Name
    const {
        value: enteredLastName,
        isValid: enteredLastNameIsValid,
        hasError: lastNameInputHasError,
        messageError: lastNameMessageError,
        valueChanged: lastNameChangeHandler,
        valueBlur: lastNameBlurHandler,
        checkIsNotEmpty: lastNameCheckIsNotEmpty,
    } = useInput();

    const lastNameOnChanged = (event) => {
        lastNameChangeHandler(event.target.value);
        lastNameCheckIsNotEmpty(event.target.value);
    }

    const lastNameOnBlur = () => {
        lastNameBlurHandler(enteredLastName);
        lastNameCheckIsNotEmpty(enteredLastName);
    }

    //Validate First Name
    const {
        value: enteredFirstName,
        isValid: enteredFirstNameIsValid,
        hasError: firstNameInputHasError,
        messageError: firstNameMessageError,
        valueChanged: firstNameChangeHandler,
        valueBlur: firstNameBlurHandler,
        checkIsNotEmpty: firstNameCheckIsNotEmpty,
    } = useInput();

    const firstNameOnChanged = (event) => {
        firstNameChangeHandler(event.target.value);
        firstNameCheckIsNotEmpty(event.target.value);
    }

    const firstNameOnBlur = () => {
        firstNameBlurHandler(enteredFirstName);
        firstNameCheckIsNotEmpty(enteredFirstName);
    }

    // Phone Number
    const {
        value: enteredPhone,
        isValid: enteredPhoneIsValid,
        hasError: phoneInputHasError,
        messageError: phoneMessageError,
        valueChanged: phoneChangeHandler,
        valueBlur: phoneBlurHandler,
        checkIsNotEmpty: phoneCheckIsNotEmpty,
        checkIsNumber: phoneCheckIsNumber,
        checkIsPhoneNumber
    } = useInput();

    const phoneOnChanged = (event) => {
        phoneChangeHandler(event.target.value);
        phoneCheckIsNotEmpty(event.target.value);
        phoneCheckIsNumber(event.target.value);
        checkIsPhoneNumber(event.target.value);
    }

    const phoneOnBlur = () => {
        phoneBlurHandler();
        phoneCheckIsNotEmpty(enteredPhone);
        phoneCheckIsNumber(enteredPhone);
        checkIsPhoneNumber(enteredPhone);
    }

    // Validate OTP
    const phoneFormatted = enteredPhone.replace('0', '+84');

    const { otpIsValid, errorMessage, isSendOTP, otpOnChange, onGetOTPSubmit } = useOTP(phoneFormatted);

    //Validate Password
    const {
        value: enteredPassword,
        isValid: enteredPasswordIsValid,
        hasError: passwordInputHasError,
        messageError: passwordMessageError,
        valueChanged: passwordChangeHandler,
        valueBlur: passwordBlurHandler,
        checkIsNotEmpty: passwordCheckIsNotEmpty,
    } = useInput();

    const passwordOnChanged = (event) => {
        passwordChangeHandler(event.target.value);
        passwordCheckIsNotEmpty(event.target.value);
    }

    const passwordOnBlur = () => {
        passwordBlurHandler(enteredPassword);
        passwordCheckIsNotEmpty(enteredPassword);
    }

    // Validate Repeat Password
    const {
        value: enteredRePassword,
        isValid: enteredRePasswordIsValid,
        hasError: rePasswordInputHasError,
        messageError: rePasswordMessageError,
        valueChanged: rePasswordChangeHandler,
        valueBlur: rePasswordBlurHandler,
        checkIsNotEmpty: rePasswordCheckIsNotEmpty,
    } = useInput();

    const rePasswordOnChanged = (event) => {
        rePasswordChangeHandler(event.target.value);
        rePasswordCheckIsNotEmpty(event.target.value);
    }

    const rePasswordOnBlur = () => {
        rePasswordBlurHandler(enteredRePassword);
        rePasswordCheckIsNotEmpty(enteredRePassword);
    }

    //check match Password
    let rePasswordIsValid = false;
    if (enteredRePassword === enteredPassword) {
        rePasswordIsValid = true;
    }

    //Generate repassword message error
    let rePasswordErrorText = '';
    if (rePasswordInputHasError) {
        rePasswordErrorText = rePasswordMessageError;
    } else if (!rePasswordIsValid && enteredRePassword.length > 0) {
        rePasswordErrorText = 'Mật khẩu không trùng khớp';
    }

    // Check form is valid
    let formIsValid = false;
    if (enteredLastNameIsValid &&
        enteredFirstNameIsValid &&
        enteredPhoneIsValid &&
        otpIsValid &&
        enteredPasswordIsValid &&
        (enteredRePasswordIsValid && rePasswordIsValid)) {
        formIsValid = true;
    }

    //Handler send server
    const { sendRequest: signUpRequest, status: statusSignUp, error } = useHttp(SignUp);

    const { addMessage } = useContext(MessageContext);

    useEffect(() => {
        if (statusSignUp === 'completed') {
            props.showSignInTab();
            addMessage({
                message: "Đăng ký thành công.",
                className: "success"
            });
        }
    }, [statusSignUp, error]);

    //Confirm submit Register
    const registerSubmit = (e) => {
        e.preventDefault();

        if (formIsValid) {
            signUpRequest({
                Last_Name: enteredLastName,
                First_Name: enteredFirstName,
                Phone: enteredPhone,
                Password: enteredPassword,
                Role: 2 // employee
            });
        }
    }

    return (
        <form onSubmit={registerSubmit} className={`${classes["sign-up-htm"]} ${props.show ? classes.show : ''}`}>
            <div className={classes.group}>
                <label htmlFor="user" className={classes.label}>Tên họ & đệm</label>
                <input
                    id="user"
                    type="text"
                    className={`${classes.input} ${lastNameInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredLastName}
                    onChange={lastNameOnChanged}
                    onBlur={lastNameOnBlur}
                />
                {lastNameInputHasError && <p className={classes.error}>{lastNameMessageError}</p>}
            </div>

            <div className={classes.group}>
                <label htmlFor="user" className={classes.label}>Tên</label>
                <input
                    id="user"
                    type="text"
                    className={`${classes.input} ${firstNameInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredFirstName}
                    onChange={firstNameOnChanged}
                    onBlur={firstNameOnBlur}
                />
                {firstNameInputHasError && <p className={classes.error}>{firstNameMessageError}</p>}
            </div>

            <div className={classes.group}>
                <label htmlFor="phone" className={classes.label}>Số điện thoại</label>
                <input
                    id="phone"
                    type="tel"
                    className={`${classes.input} ${phoneInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredPhone}
                    onChange={phoneOnChanged}
                    onBlur={phoneOnBlur}
                />
                {phoneInputHasError && <p className={classes.error}>{phoneMessageError}</p>}
            </div>

            <div className={`${classes.group} ${classes.otp}`}>
                <div id="get-otp-button"></div>
                <label htmlFor="phone" className={classes.label}>OTP</label>
                <div className={classes['input-otp']}>
                    <input
                        id="otp"
                        type="text"
                        className={`${classes.input}`}
                        disabled={otpIsValid || !enteredPhoneIsValid}
                        onChange={otpOnChange}
                    />
                    <p className={classes['error']}>{errorMessage}</p>
                </div>
                <Button onClick={onGetOTPSubmit} disabled={isSendOTP || !enteredPhoneIsValid}>Lấy OTP</Button>
            </div>

            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Mật khẩu</label>
                <input
                    id="pass"
                    type="password"
                    className={`${classes.input} ${passwordInputHasError ? classes.invalid : ''}`}
                    defaultValue={enteredPassword}
                    onChange={passwordOnChanged}
                    onBlur={passwordOnBlur}
                />
                {passwordInputHasError && <p className={classes.error}>{passwordMessageError}</p>}
            </div>

            <div className={classes.group}>
                <label htmlFor="pass" className={classes.label}>Nhập lại mật khẩu</label>
                <input
                    id="repass"
                    type="password"
                    className={`${classes.input} ${rePasswordErrorText ? classes.invalid : ''}`}
                    defaultValue={enteredRePassword}
                    onChange={rePasswordOnChanged}
                    onBlur={rePasswordOnBlur}
                />
                {rePasswordErrorText && <p className={classes.error}>{rePasswordErrorText}</p>}
            </div>

            {!statusSignUp && (
                <div className={classes.group}>
                    <input disabled={!formIsValid} type="submit" className={classes.button} value="Đăng Ký" onChange={() => { }} />
                </div>
            )}
            {statusSignUp === 'pending' && (
                <LoadingSpinner />
            )}
            <div className={classes.hr} />
            <div className={classes["foot-lnk"]}>
                <label htmlFor="tab-1">Bạn đã có tài khoản ?</label>
            </div>
        </form>
    );
}

export default RegisterEmployee;
