import { Fragment, useContext, useRef, useState } from 'react';
import AuthContext from '../../../store/auth-context';

import Modal from '../../UI/Modal';
import classes from './Auth.module.css';
import Register from '../../Register/Register';
import RegisterEmployee from '../../RegisterEmployee/RegisterEmployee';
import Login from '../../Login/Login';
import { MessageContext } from '../../../store/message-context';
import MessageBox from '../../UI/MessageBox';

const Auth = (props) => {
    const [onSignIn, setOnSignIn] = useState(true);
    const [onSignUp, setOnSignUp] = useState(false);
    const [onSignUpEmp, setOnSignUpEmp] = useState(false);

    const [selectedTab, setSelectedTab] = useState('sign-in');

    const phoneLoginRef = useRef();
    const passwordLoginRef = useRef();

    const authCtx = useContext(AuthContext);

    const loginHandler = (event) => {
        event.preventDefault();

        const phone = phoneLoginRef.current.value;
        const password = passwordLoginRef.current.value;

        authCtx.onLogin(phone, password);
        console.log(authCtx);
    }

    const tabAuthChangedHandler = (e) => {
        if (e.target.value === "sign-in") {
            setSelectedTab('sign-in')
            setOnSignIn(true);
            setOnSignUp(false);
            setOnSignUpEmp(false);
        } else if (e.target.value === "sign-up") {
            setSelectedTab('sign-up');
            setOnSignIn(false);
            setOnSignUp(true);
            setOnSignUpEmp(false);
        } else {
            setSelectedTab('sign-up-emp');
            setOnSignIn(false);
            setOnSignUp(false);
            setOnSignUpEmp(true);
        }
    }

    const showSignInTab = () => {
        setSelectedTab('sign-in');
        setOnSignIn(true);
        setOnSignUp(false);
        setOnSignUpEmp(false);
    }

    const messageCtx = useContext(MessageContext);
    const {isShow, message, className} = messageCtx;

    return (
        <Fragment>
            <MessageBox isShow={isShow} message={message} className={className} />
            <Modal customModalClass="auth">
                <div className={classes["login-wrap"]}>
                    <div className={classes["login-html"]} >
                        <div>
                            <input onChange={tabAuthChangedHandler} value="sign-in" id="tab-1" type="radio" name="tab" className={classes["sign-in"]} checked={selectedTab === 'sign-in'} /><label htmlFor="tab-1" className={classes.tab}>Đăng Nhập</label>
                            <input onChange={tabAuthChangedHandler} value="sign-up" id="tab-2" type="radio" name="tab" className={classes["sign-up"]} checked={selectedTab === 'sign-up'} /><label htmlFor="tab-2" className={classes.tab}>Đăng Ký</label>

                            <input onChange={tabAuthChangedHandler} value="sign-up-emp" id="tab-3" type="radio" name="tab" className={classes["sign-up"]} checked={selectedTab === 'sign-up-emp'} />
                            <label htmlFor="tab-3" className={classes.tab}>Đăng Ký Dành Cho Thợ</label>
                        </div>

                        <div className={classes["login-form"]}>
                            <Login show={onSignIn} />
                            <Register showSignInTab={showSignInTab} show={onSignUp} />
                            <RegisterEmployee showSignInTab={showSignInTab} show={onSignUpEmp} />
                        </div>
                    </div>
                </div>
            </Modal>
        </Fragment>
    );
}

export default Auth;