import classes from './MainHeader.module.css';
import Navigation from './Navigation';

const MainHeader = (props) => {
    return (
        <div className={classes['header']}>
            <Navigation />
        </div>
    );
}

export default MainHeader;