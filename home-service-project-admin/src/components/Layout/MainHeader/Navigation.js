import { Fragment, useContext } from 'react';
import { Link } from 'react-router-dom';

import classes from './Navigation.module.css';
import AuthContext from '../../../store/auth-context';

const Navigation = (props) => {
    const { onLogout } = useContext(AuthContext);

    return (
        <Fragment>
            <div className={classes['profile_info']}>
                <img src="https://demo.dashboardpack.com/sales-html/img/client_img.png" alt="" />
                <div className={classes['profile_info_inner']}>
                    <div className={classes['profile_author_name']}>
                        <h5>Khoa Tran</h5>
                    </div>

                    <div className={classes['profile_info_details']}>
                        <a onClick={onLogout} href="">Đăng Xuất</a>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Navigation;