import { Fragment, useContext } from 'react';
import { MessageContext } from '../../store/message-context';
import MessageBox from '../UI/MessageBox';

import classes from './Layout.module.css';
import MainHeader from './MainHeader/MainHeader';
import SideBar from './SideBar/SideBar';

const Layout = (props) => {
    const messageCtx = useContext(MessageContext);
    const { isShow, message, className } = messageCtx;

    return (
        <Fragment>
            <SideBar />
            <div className={classes['page-wrapper']}>
                <MessageBox isShow={isShow} message={message} className={className} />
                <MainHeader />
                <div className={classes['main-content']}>
                    {props.children}
                </div>
            </div>
        </Fragment>
    );
}

export default Layout;