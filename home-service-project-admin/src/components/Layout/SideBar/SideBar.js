import { Fragment, useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import classes from './SideBar.module.css';
import logo from '../../../assets/image/logo.png';
import FontAwesomeIcon from '../../UI/FontAwesome';
import AuthContext from '../../../store/auth-context';

const SideBar = (props) => {
    const { role } = useContext(AuthContext);

    const sidebarRole = () => {
        if (role === 1) {
            return (
                <Fragment>
                    <li>
                        <FontAwesomeIcon icon='list-ul' size="2x" color="#fc5e10" />
                        <NavLink exact activeClassName={classes.active} to='/'>Danh Sách Bài Đăng</NavLink>
                    </li>

                    <li>
                        <FontAwesomeIcon icon='users' size="2x" color="#fc5e10" />
                        <NavLink activeClassName={classes.active} to='/employee-manager'>Quản Lý Nhân Viên</NavLink>
                    </li>

                    <li>
                        <FontAwesomeIcon icon='laptop-house' size="2x" color="#fc5e10" />
                        <NavLink activeClassName={classes.active} to='/services-manager'>Quản Lý Dịch Vụ</NavLink>
                    </li>

                    <li>
                        <FontAwesomeIcon icon='chart-bar' size="2x" color="#fc5e10" />
                        <NavLink activeClassName={classes.active} to='/statistics'>Thống Kê</NavLink>
                    </li>
                </Fragment>
            );
        } else if (role === 2) {
            return (
                <Fragment>
                    <li>
                        <NavLink exact activeClassName={classes.active} to='/'>Lịch Phân Công</NavLink>
                    </li>

                    <li>
                        <NavLink activeClassName={classes.active} to='/account'>Thông Tin Cá Nhân</NavLink>
                    </li>

                    <li>
                        <NavLink activeClassName={classes.active} to='/emp-statistics'>Thống Kê</NavLink>
                    </li>
                </Fragment>
            );

        }
    }

    return (
        <nav className={classes.sidebar}>
            <div className={classes.logo}>
                <Link to="/">
                    <img height="50" src={logo} alt="" />
                </Link>
            </div>
            <ul className={classes.menu}>
                {sidebarRole()}
            </ul>
        </nav>
    );
}

export default SideBar;