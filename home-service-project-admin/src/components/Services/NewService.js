import { useContext, useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";

import classes from "./NewService.module.css";
import Modal from "../UI/Modal";
import Button from "../UI/Button";
import Input from "../UI/Input";
import {
  GetListTypeService,
  GetListUnit,
  AddTypeServices,
} from "../../lib/api";
import useHttp from "../../hooks/use-http";
import LoadingSpinner from "../UI/LoadingSpinner";
import FontAwesome from "../UI/FontAwesome";
import AuthContext from "../../store/auth-context";
import { MessageContext } from "../../store/message-context";

const NewService = (props) => {
  // Check unauthorized
  const history = useHistory();
  const { token, onLogout } = useContext(AuthContext);
  const { addMessage } = useContext(MessageContext);

  const {
    sendRequest: getTypeServices,
    status: statusGetTypeServices,
    data: typeServices,
    error: errorGetTypeServices,
  } = useHttp(GetListTypeService, true);

  useEffect(() => {
    getTypeServices({ token });
  }, [getTypeServices]);

  const {
    sendRequest: getUnits,
    status: statusGetUnits,
    data: units,
    error: errorGetUnits,
  } = useHttp(GetListUnit, true);

  useEffect(() => {
    getUnits({ token });
  }, [getUnits]);

  const {
    sendRequest: addNewTypeService,
    status: statusAddNewTypeService,
    error: errorAddNewTypeService,
  } = useHttp(AddTypeServices);

  useEffect(() => {
    if (statusAddNewTypeService === "completed" && !errorAddNewTypeService) {
      getTypeServices({ token });
      setIsShowAddNewTypeService(false);
      setSelectedImage(null);
      setImagePreview(null);

      addMessage({
        message: "Thêm loại dịch vụ thành công",
        className: "success",
      });
    } else if (errorAddNewTypeService) {
      if (errorAddNewTypeService === "Unauthorized") {
        history.push("/");
        onLogout();
        addMessage({ auth: true });
      } else {
        addMessage({
          message: errorAddNewTypeService,
          className: "error",
        });
      }
    }
  }, [statusAddNewTypeService, errorAddNewTypeService, getTypeServices]);

  const serviceNameRef = useRef();
  const priceRef = useRef();
  const typeServiceRef = useRef();

  const [typeServiceNameSelected, setTypeServiceNameSelected] = useState("");
  const [typeServiceIdSelected, setTypeServiceIdSelected] = useState();

  const [unitService, setUnitService] = useState("");

  const [unit, setUnit] = useState(null);

  const [imagePreview, setImagePreview] = useState(null);

  const [selectedImage, setSelectedImage] = useState(null);

  const [isShowAddNewTypeService, setIsShowAddNewTypeService] = useState(false);

  let typeServiceRenderHtml;
  if (statusGetTypeServices === "pending") {
    typeServiceRenderHtml = <option>Loading...</option>;
  } else if (errorGetTypeServices) {
    if (
      errorGetTypeServices === "Unauthorized" ||
      errorGetTypeServices === "Access Denied!"
    ) {
      history.push("/");
      onLogout();
      addMessage({ auth: true });
      return false;
    } else {
      addMessage({
        message: errorGetTypeServices,
        className: "error",
      });
    }
  } else if (
    statusGetTypeServices === "completed" &&
    (!typeServices || typeServices.length === 0)
  ) {
    addMessage({
      message: "Không tồn tại loại dịch vụ nào",
      className: "error",
    });
  } else {
    typeServiceRenderHtml = typeServices.map((item, key) => {
      return (
        <option key={key} value={item.Type_Id} unitid={item.Unit_Id}>
          {item.Type_Name}
        </option>
      );
    });
  }

  //Render list Units
  let unitsRenderHtml;
  if (statusGetUnits === "pending") {
    unitsRenderHtml = <option>Loading...</option>;
  } else if (errorGetUnits) {
    if (
      errorGetUnits === "Unauthorized" ||
      errorGetUnits === "Access Denied!"
    ) {
      history.push("/");
      onLogout();
      addMessage({ auth: true });
      return false;
    } else {
      addMessage({
        message: errorGetUnits,
        className: "error",
      });
    }
  } else if (statusGetUnits === "completed" && (!units || units.length === 0)) {
    addMessage({
      message: "Không tồn tại đơn vị nào",
      className: "error",
    });
  } else {
    unitsRenderHtml = units.map((item, key) => {
      return (
        <option key={key} value={item.Unit_Id}>
          {item.Name}
        </option>
      );
    });
  }

  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImagePreview((prevState) => (prevState = URL.createObjectURL(img)));
      setSelectedImage(img);
    }
  };

  const addTypeService = async (e) => {
    e.preventDefault();

    const enteredTypeService = typeServiceRef.current.value;

    if (enteredTypeService === null || enteredTypeService === "") {
      typeServiceRef.current.focus();
      return;
    }

    if (unit) {
      // Upload Image
      const fd = new FormData();
      fd.append("file", selectedImage, selectedImage.name);

      const response = await fetch(
        "https://home-repair-api.herokuapp.com/uploadFile",
        {
          method: "POST",
          body: fd,
        }
      );

      const data = await response.json();

      addNewTypeService({
        newTypeServiceData: {
          Name: typeServiceRef.current.value,
          Image: data.fileDownloadUri,
          Unit_Id: unit,
        },
        token,
      });

      setIsShowAddNewTypeService(false);
    } else {
      document.getElementById("unit").focus();
    }
  };

  const addNewServiceHandler = (e) => {
    e.preventDefault();
    const date = new Date();
    const dateFormat = date.toLocaleDateString("en-GB");

    props.addNewService({
      serviceId: 0,
      Service_Name: serviceNameRef.current.value,
      Type_Id: typeServiceIdSelected,
      Type_Name: typeServiceNameSelected,
      Price: priceRef.current.value,
      Create_Date: dateFormat,
      Update_Date: dateFormat,
    });
  };

  const toggleNewTypeService = () => {
    setIsShowAddNewTypeService((prevState) => (prevState = !prevState));
  };

  const chooseTypeServicesHandler = (e) => {
    const unitId = e.target.options[e.target.selectedIndex].getAttribute("unitid");

    let index = e.nativeEvent.target.selectedIndex;
    setTypeServiceNameSelected(e.nativeEvent.target[index].text);

    setTypeServiceIdSelected(e.target.value);

    const a = units.filter((item) => item.Unit_Id === +unitId);
    setUnitService(a[0].Name + ` (${a[0].Unit})`);
  };

  const chooseUnitHandler = (e) => {
    setUnit(e.target.value);
  };

//   const chooseServicesHandler = (e) => {
//     setServiceId(e.target.value);
//     setPrice(e.target.options[e.target.selectedIndex].getAttribute("price"));
//   };

  return (
    <Modal
      title="Thêm Mới Dịch Vụ"
      customModalClass="new-service"
      customBackdropClass="new-service"
      onClose={props.hideAddService}
    >
      <form onSubmit={addNewServiceHandler} className={classes["profile"]}>
        <Input
          id="servivename"
          label="Tên dịch vụ"
          ref={serviceNameRef}
          required={true}
        />

        <Input id="typeService" label="Loại dịch vụ">
          <div className={classes["type-services"]}>
            <select
              defaultValue={""}
              required
              onChange={chooseTypeServicesHandler}
            >
              <option value="" disabled>
                Chọn loại dịch vụ
              </option>
              {typeServiceRenderHtml}
            </select>

            <Button onClick={toggleNewTypeService}>
              {isShowAddNewTypeService ? "Đóng" : "Thêm Mới"}
            </Button>

            {isShowAddNewTypeService && (
              <div className={classes["add-type"]}>
                <Input
                  id="newtype"
                  ref={typeServiceRef}
                  label="Tên loại dịch vụ"
                  required={true}
                />

                <Input id="newunit" label="Đơn vị">
                  <select
                    id="unit"
                    defaultValue={""}
                    required
                    onChange={chooseUnitHandler}
                  >
                    <option value="" disabled>
                      Chọn đơn vị
                    </option>
                    {unitsRenderHtml}
                  </select>
                </Input>

                <Input
                  type="file"
                  name="myImage"
                  onChange={onImageChange}
                  label="Chọn hình ảnh"
                  required={true}
                />

                <img width="120" src={imagePreview} />
                {statusAddNewTypeService !== "pending" && (
                  <Button onClick={addTypeService}>Lưu</Button>
                )}
                {statusAddNewTypeService === "pending" && <LoadingSpinner />}
              </div>
            )}
          </div>
        </Input>

        <Input
          id="unit"
          label="Đơn vị"
          value={unitService}
          required={true}
          disabled={true}
          onChange={() => {}}
        />

        <Input
          id="price"
          label="Giá dịch vụ"
          type="number"
          ref={priceRef}
          required={true}
        />

        {!props.isLoading && (
          <Button type="submit" className="success">
            <FontAwesome icon="check" />
            {" Thêm Mới"}
          </Button>
        )}
        {props.isLoading && <LoadingSpinner />}
      </form>
    </Modal>
  );
};

export default NewService;
