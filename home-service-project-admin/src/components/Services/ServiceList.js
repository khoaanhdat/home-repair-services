import classes from './ServiceList.module.css';
import Table from '../UI/Table';
import Button from '../UI/Button';
import { Fragment } from 'react';
import { usePagination } from '../../hooks/use-pagination';
import { Pagination } from '../UI/Pagination';
import LoadingSpinner from '../UI/LoadingSpinner';
import FontAwesome from '../UI/FontAwesome';

const ServiceList = (props) => {
    const perPage = 10;
    const {
        currentList: currentPosts,
        currentPage,
        paginateHandler
    } = usePagination(1, perPage, props.listServices);

    const headers = [
        "STT",
        "Tên Dịch Vu",
        "Loại Dịch Vụ",
        "Giá",
        "Ngày Tạo Mới",
        "Ngày Cập Nhật"
    ];

    if (!currentPosts || currentPosts.length === 0) {
        return <LoadingSpinner />
    }

    const body = currentPosts.map((item, key) => {
        return (
            <tr key={key}>
                <td>{key + 1}</td>
                <td>{item.Service_Name}</td>
                <td>{item.Type_Name}</td>
                <td>{item.Price.toLocaleString('it-IT')} đ</td>
                <td>{item.Create_Date}</td>
                <td>{item.Update_Date}</td>
            </tr>
        );
    });

    const paginateClick = (page) => {
        paginateHandler(page);
    }

    return (
        <Fragment>
            <div className={classes.services}>
                <Table
                    title="Danh Sách Dịch Vụ"
                    headers={headers}
                    body={body}
                    customClass="services"
                />
                <Button onClick={props.showAddService} className="success">
                    <FontAwesome icon={["far", "calendar-plus"]} />
                    {' Thêm Mới Dịch Vụ'}
                </Button>
            </div>
            <Pagination currentPage={currentPage} listPerPage={perPage} totalList={props.listServices.length} paginateClick={paginateClick} />
        </Fragment>
    );
}

export default ServiceList;