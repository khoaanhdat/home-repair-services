import { Route, Switch } from 'react-router';

import './App.css';
import Layout from './components/Layout/Layout';
import NotFound from './pages/NotFound';
import PostManager from './pages/PostManager';
import EmployeeManager from './pages/EmployeeManager';
import ServicesManager from './pages/ServicesManager';
import StatisticsManager from './pages/StatisticsManager';
import AssignmentSchedule from './pages/AssignmentSchedule';
import { Fragment, useContext } from 'react';
import Auth from './components/Layout/Auth/Auth';
import AuthContext from './store/auth-context';
import AssignEmployeeProvider from './store/AssignEmployeeProvider';
import EmployeeManagerProvider from './store/EmployeeManagerProvider';
import EmployeeProfile from './pages/EmployeeProfile';
import EmployeeStatistics from './pages/EmployeeStatistics';

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faListUl,
  faUsers,
  faLaptopHouse,
  faChartBar,
  faEdit,
  faShareSquare,
  faUserCheck,
  faUserPlus,
  faCheck,
  faSearchDollar
} from '@fortawesome/free-solid-svg-icons';

import {
  faCalendarPlus,
  faTimesCircle
} from '@fortawesome/free-regular-svg-icons';

library.add(
  faListUl,
  faUsers,
  faLaptopHouse,
  faChartBar,
  faEdit,
  faShareSquare,
  faUserCheck,
  faTimesCircle,
  faUserPlus,
  faCheck,
  faCalendarPlus,
  faSearchDollar
);

function App() {
  // const headers = new Headers();
  // headers.append('Accss-Control-Allow-Origin', 'http://localhost:3001');
  // headers.append('Access-Control-Allow-Credentials', 'true');
  // headers.append('GET', 'POST', 'OPTIONS');

  const { role, isLoggedIn } = useContext(AuthContext);

  const rolePage = () => {
    if (isLoggedIn) {
      let routeRole;
      //Company
      if (role === 1) {
        routeRole = <Switch>
          <Route path='/' exact>
            <AssignEmployeeProvider>
              <PostManager />
            </AssignEmployeeProvider>
          </Route>

          <Route path='/employee-manager' exact>
            <EmployeeManagerProvider>
              <EmployeeManager />
            </EmployeeManagerProvider>
          </Route>

          <Route path='/services-manager' exact>
            <ServicesManager />
          </Route>

          <Route path='/statistics' exact>
            <StatisticsManager />
          </Route>

          <Route path='*'>
            <NotFound />
          </Route>
        </Switch>
      }

      //Employee
      if (role === 2) {
        routeRole = <Switch>
          <Route path='/' exact>
            <AssignmentSchedule />
          </Route>

          <Route path='/account' exact>
            <EmployeeProfile />
          </Route>

          <Route path='/emp-statistics' exact>
            <EmployeeStatistics />
          </Route>

          <Route path='*'>
            <NotFound />
          </Route>
        </Switch>
      }
      return (
        <Layout>
          {routeRole}
        </Layout>
      );
    } else {
      return (
        <Auth />
      );
    }
  }

  return (
    <Fragment>
      {rolePage()}
    </Fragment>
  );
}

export default App;
