import { useCallback, useEffect, useState } from 'react';
import firebase from '../lib/firebase';

const useOTP = (phoneNumber) => {
    const [otp, setOtp] = useState("");
    const [isSendOTP, setIsSendOTP] = useState(false);
    const [otpIsValid, setOtpIsValid] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const otpOnChange = useCallback((e) => {
        setTimeout(() => {
            setOtp(e.target.value);
        }, 200);
    });

    const configureCaptcha = () => {
        var element = document.getElementById('get-otp-button');
        if (element === null || element.value == '') {
            var str = '<div id="get-otp-button"></div>';

            var temp = document.createElement('div');
            temp.innerHTML = str;
            document.getElementById('recaptcha').appendChild(temp.firstChild);
        }

        window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('get-otp-button', {
            'size': 'invisible',
            'callback': (response) => {
                // reCAPTCHA solved, allow signInWithPhoneNumber.
                onGetOTPSubmit();
            }
        });
    }

    const onGetOTPSubmit = (e) => {
        e.preventDefault();

        configureCaptcha();

        const appVerifier = window.recaptchaVerifier;

        firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier)
            .then((confirmationResult) => {
                // SMS sent. Prompt user to type the code from the message, then sign the
                // user in with confirmationResult.confirm(code).
                window.confirmationResult = confirmationResult;
                setIsSendOTP(true);
            }).catch((error) => {
                // Error; SMS not sent
                console.log(error);
                setIsSendOTP(false);
            });
    }

    useEffect(() => {
        if (otp.trim().length === 6 && isSendOTP) {
            window.confirmationResult.confirm(otp).then((result) => {
                // User signed in successfully.
                // const user = result.user;
                setOtpIsValid(true);
                setErrorMessage('');
            }).catch((error) => {
                // User couldn't sign in (bad verification code?)
                console.log(error);
                setErrorMessage('Mã OTP không chính xác.');
            });
        }
    }, [otp, isSendOTP]);

    return {
        otpIsValid,
        isSendOTP,
        errorMessage,
        otpOnChange,
        onGetOTPSubmit
    }
}

export default useOTP;