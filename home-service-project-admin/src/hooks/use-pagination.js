import { useEffect, useState } from "react";

export const usePagination = (initCurrentPage, listPerPage, listData) => {
    const [currentList, setCurrentList] = useState(null);
    const [currentPage, setCurrentPage] = useState(initCurrentPage);

    useEffect(() => {
        if (listData && listData.length > 0) {
            const indexOfLastList = currentPage * listPerPage;
            const indexOfFirstList = indexOfLastList - listPerPage;

            setCurrentList(listData.slice(indexOfFirstList, indexOfLastList));
        }
    }, [listData, currentPage, listPerPage]);

    const paginateHandler = (page) => setCurrentPage(page);

    return {
        currentList,
        currentPage,
        paginateHandler
    }
}
