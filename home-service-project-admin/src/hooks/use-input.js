import { useReducer } from "react";

const EMPTY_MESSAGE_ERROR = 'Vui lòng không để trống ô này';
const FORMAT_MESSAGE_ERROR = 'Vui lòng nhập đúng định dạng.';
const PHONE_MESSAGE_ERROR = 'Số điện thoại không hợp lệ.';

const initInputState = {
    value: '',
    isTouched: false,
    isValid: false,
    messageError: ''
};

const inputReducer = (state, action) => {
    if (action.type === 'ON_CHANGE') {
        return {
            ...state,
            value: action.value,
            isTouched: state.isTouched
        }
    }

    if (action.type === 'ON_BLUR') {
        return {
            ...state,
            isTouched: true
        }
    }

    if (action.type === 'CHECK_EMPTY') {
        if (action.value.trim() === '') {
            return {
                ...state,
                isValid: false,
                messageError: EMPTY_MESSAGE_ERROR
            }
        }
        return {...state, isTouched: true, isValid: true};
    }

    if (action.type === 'CHECK_NUMBER') {
        if (action.value.trim() !== '') {            
            if (isNaN(action.value)) {
                return {
                    ...state,
                    isValid: false,
                    messageError: FORMAT_MESSAGE_ERROR
                }
            }
            return {
                ...state,
                isValid: true,
                messageError: ''
            };
        }
        return {...state, isTouched: true};
    }

    if (action.type === 'CHECK_PHONE') {
        const phone = action.value.trim();
        var vnf_regex = /((09|03|07|08|05)+([0-9]{8})\b)/g;

        if (phone.length > 0 && !isNaN(action.value) && !vnf_regex.test(phone)) {
            return {
                ...state,
                isValid: false,
                messageError: PHONE_MESSAGE_ERROR
            }
        }
        return {...state, isTouched: true};
    }

    return initInputState;
}

const useInput = (value = null) => {
    const [inputState, dispatch] = useReducer(inputReducer, value ? {...initInputState, value, isTouched: true, isValid: true} : initInputState);

    const hasError = !inputState.isValid && inputState.isTouched;

    const checkIsNotEmpty = (value) => {
        dispatch({
            type: "CHECK_EMPTY",
            value: value
        })
    }

    const checkIsNumber = (value) => {
        dispatch({
            type: "CHECK_NUMBER",
            value
        })
    }

    const checkIsPhoneNumber = (value) => {
        dispatch({
            type: "CHECK_PHONE",
            value
        })
    }

    const valueChanged = (value) => {
        dispatch({
            type: "ON_CHANGE",
            value
        })
    }

    const valueBlur = () => {
        dispatch({
            type: "ON_BLUR"
        })
    }

    return {
        value: inputState.value,
        isValid: inputState.isValid,
        hasError,
        messageError: inputState.messageError,
        valueChanged,
        valueBlur,
        checkIsNotEmpty,
        checkIsNumber,
        checkIsPhoneNumber
    }
}

export default useInput;