import React, { useState, useReducer } from 'react';

import AssignEmployeeContext from './assign-employee-context';

const defaultAssignEmpState = {
    dataAssignEmp: {
        location: null,
        address: null,
        quantity: '',
        postId: null,
        serviceId: null,
        empId: null,
        dateBooked: null,
        customerId: null,
        totalPrice: null,
        approvalDate: null
    }
}

const assignEmpReducer = (state, action) => {
    if (action.type === 'CHOOSE_POST') {
        return {
            dataAssignEmp: {
                ...state.dataAssignEmp,
                postId: action.postId,
                customerId: action.customerId,
                address: action.address,
                location: action.location,
                dateBooked: action.dateBooked,
                quantity: action.quantity
            }
        }
    }

    if (action.type === 'ASSIGN_EMP') {
        return {
            dataAssignEmp: {
                ...state.dataAssignEmp,
                serviceId: action.serviceId,
                empId: action.empId,
                totalPrice: action.totalPrice,
                approvalDate: action.approvalDate
            }
        }
    }

    return defaultAssignEmpState;
}

const AssignEmployeeProvider = props => {
    const [isShowAssignEmployee, setIsShowAssignEmployee] = useState(false);

    const [assignEmpState, dispatchAssignEmpAction] = useReducer(assignEmpReducer, defaultAssignEmpState);

    const choosePost = (postId, customerId, address, location, dateBooked, quantity) => {
        dispatchAssignEmpAction({
            type: "CHOOSE_POST",
            postId, customerId, address, location, dateBooked, quantity
        });
    }

    const assignEmp = (serviceId, empId, totalPrice, approvalDate) => {
        dispatchAssignEmpAction({
            type: "ASSIGN_EMP",
            serviceId, empId, totalPrice, approvalDate
        })
    }

    const showPopup = () => {
        setIsShowAssignEmployee(true);
    }

    const hidePopup = () => {
        setIsShowAssignEmployee(false);
    }

    const assignEmployeeContext = {
        isShowAssignEmployee,
        assignEmpState,
        showPopup,
        hidePopup,
        choosePost,
        assignEmp
    }

    return (
        <AssignEmployeeContext.Provider value={assignEmployeeContext}>
            {props.children}
        </AssignEmployeeContext.Provider>
    )
}

export default AssignEmployeeProvider;