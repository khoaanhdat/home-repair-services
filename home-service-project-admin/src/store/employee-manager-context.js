import React from 'react';

const EmployeeManagerContext = React.createContext({
    isShowEditEmployee: false,
    isShowNewEmployee: false,
    editEmpState: null,
    showEditForm: () => {},
    hideEditForm: () => {},
    showNewForm: () => {},
    hideNewForm: () => {},
    chooseEditEmployee: (empId, firstName, lastName, description, address, sex, available, phone, birthDay) => {}
});

export default EmployeeManagerContext;