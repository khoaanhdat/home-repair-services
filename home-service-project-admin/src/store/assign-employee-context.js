import React from 'react';

const AssignEmployeeContext = React.createContext({
    isShowAssignEmployee: false,
    assignEmpState: null,
    showPopup: () => {},
    hidePopup: () => {},
    choosePost: (postId, customerId, address, location, dateBooked, quantity) => {},
    assignEmp: (serviceId, empId, totalPrice, approvalDate) => {}
});

export default AssignEmployeeContext;