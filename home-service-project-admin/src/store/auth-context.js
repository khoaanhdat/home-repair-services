import React from 'react';

const AuthContext = React.createContext({
    isLoggedIn: false,
    token: null,
    role: null,
    companyId: null,
    employeeId: null,
    onLogout: () => { },
    onLogin: (sdt, password) => { }
});

export default AuthContext;