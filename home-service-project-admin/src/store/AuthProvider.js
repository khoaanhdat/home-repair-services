import React, { useState } from 'react';
import cookie from 'react-cookies';

import AuthContext from './auth-context';

const AuthProvider = props => {
    let token = null, role = null, companyId = null, employeeId = null;
    const cookieDataUser = cookie.load('user');
    if (cookieDataUser !== undefined) {
        token = cookieDataUser.token;
        role = cookieDataUser.role;
        companyId = cookieDataUser.companyId ?? null;
        employeeId = cookieDataUser.employeeId ?? null
    }

    const [tokenState, setTokenState] = useState(token);
    const [roleState, setRoleState] = useState(role);
    const [companyIdState, setCompanyIdState] = useState(companyId);
    const [empIdState, setEmpIdState] = useState(employeeId);

    const userIsLoggedIn = !!tokenState;

    const loginHandler = ({ token, expiresIn, role, companyId = null, empId = null }) => {
        setTokenState(token);
        setRoleState(role);
        setCompanyIdState(companyId);
        setEmpIdState(empId);

        const expirationTime = new Date();
        expirationTime.setTime(expirationTime.getTime() + (+expiresIn * 1000));

        let dataCookie = {
            token: token,
            role: role
        };

        if (companyId !== null) {
            dataCookie = { ...dataCookie, companyId: companyId };
        } else if (empId !== null) {
            dataCookie = { ...dataCookie, employeeId: empId }
        }

        cookie.save('user', JSON.stringify(dataCookie), { path: '/', expires: expirationTime });
    };

    const logoutHandler = () => {
        setTokenState(null);
        setRoleState(null);
        setCompanyIdState(null);
        setEmpIdState(null);
        cookie.remove('user', { path: '/' });
    };

    const authContext = {
        isLoggedIn: userIsLoggedIn,
        token: tokenState,
        role: roleState,
        companyId: companyIdState,
        employeeId: empIdState,
        onLogin: loginHandler,
        onLogout: logoutHandler
    }

    return (
        <AuthContext.Provider value={authContext}>
            {props.children}
        </AuthContext.Provider>
    )
}

export default AuthProvider;