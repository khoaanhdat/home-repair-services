import React, { useState, useReducer } from 'react';

import EmployeeManagerContext from './employee-manager-context';

const defaultEditEmpState = {
    dataEditEmp: {
        empId: null,
        firstName: null,
        lastName: null,
        description: null,
        address: null,
        location: null,
        sex: null,
        available: null,
        phone: null, 
        birthDay: null
    }
}

const editEmpReducer = (state, action) => {
    if (action.type === 'CHOOSE_EMP_EDIT') {
        return {
            dataEditEmp: {
                ...state.dataEditEmp,
                empId: action.empId,
                firstName: action.firstName,
                lastName: action.lastName,
                description: action.description,
                address: action.address,
                location: action.location,
                sex: action.sex,
                available: action.available,
                phone: action.phone,
                birthDay: action.birthDay
            }
        }
    }

    return defaultEditEmpState;
}

const EmployeeManagerProvider = props => {
    const [isShowEditEmployee, setIsShowEditEmployee] = useState(false);

    const [isShowNewEmployee, setIsShowNewEmployee] = useState(false);

    const [editEmpState, dispatchEditEmpAction] = useReducer(editEmpReducer, defaultEditEmpState);

    const chooseEditEmployee = (empId, firstName, lastName, description, address, location, sex, available, phone, birthDay) => {
        dispatchEditEmpAction({
            type: "CHOOSE_EMP_EDIT",
            empId, firstName, lastName, description, address, location, sex, available, phone, birthDay
        })
    }

    const showEditForm = () => {
        setIsShowEditEmployee(true);
    }

    const hideEditForm = () => {
        setIsShowEditEmployee(false);
    }

    const showNewForm = () => {
        setIsShowNewEmployee(true);
    }

    const hideNewForm = () => {
        setIsShowNewEmployee(false);
    }

    const editEmployeeContext = {
        isShowEditEmployee,
        isShowNewEmployee,
        editEmpState,
        showEditForm,
        hideEditForm,
        showNewForm,
        hideNewForm,
        chooseEditEmployee
    }

    return (
        <EmployeeManagerContext.Provider value={editEmployeeContext}>
            {props.children}
        </EmployeeManagerContext.Provider>
    )
}

export default EmployeeManagerProvider;