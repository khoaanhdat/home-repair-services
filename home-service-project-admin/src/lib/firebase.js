import firebase from 'firebase/app';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyDmyLQCKUvDaiL9LoDSxHIaJ-MIF0wjVXs",
    authDomain: "food-app-bd7a9.firebaseapp.com",
    databaseURL: "https://food-app-bd7a9-default-rtdb.firebaseio.com",
    projectId: "food-app-bd7a9",
    storageBucket: "food-app-bd7a9.appspot.com",
    messagingSenderId: "1053166667313",
    appId: "1:1053166667313:web:cfeebcf913ada274a9f710",
    measurementId: "G-5X8FHP3YFJ"
}
firebase.initializeApp(config);
export default firebase;