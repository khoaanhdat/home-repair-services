const API_DOMAIN = "https://home-repair-api.herokuapp.com";

export const GetListAssignmentSchedule = async ({ empId, token }) => {
  const response = await fetch(
    `${API_DOMAIN}/GetListAssignmentSchedule?Emp_Id=${empId}`,
    {
      headers: {
        "Content-Type": "application/json",
        Authorization: token,
      },
    }
  );
  const data = await response.json();
  const listAssignmentSchedule = data.List_Assignment_Schedule;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...listAssignmentSchedule];
};

export const GetListPost = async ({ token }) => {
  const response = await fetch(`${API_DOMAIN}/GetListPost`, {
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();
  const listServices = data.List_Services;

  if (!response.ok) {
    throw new Error(data.error || "Không tìm thấy bất cứ bài đăng thông báo sửa chữa nào.");
  }

  return [...listServices];
};

export const GetEmployeeDetailById = async (empId) => {
  if (empId) {
    const response = await fetch(
      `${API_DOMAIN}/GetEmployeeDetailById?Emp_Id=${empId}`
    );
    const data = await response.json();

    if (!response.ok) {
      throw new Error(data.message || "Không thể lấy dữ liệu.");
    }

    return data;
  }
};

export const UpdateEmployee = async ({ dataUpdate, token }) => {
  const response = await fetch(`${API_DOMAIN}/UpdateEmployee`, {
    method: "PUT",
    body: JSON.stringify(dataUpdate),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Không thể cập nhật.");
  }

  return data;
};

export const GetEmployeeByCompanyId = async ({ companyId, token }) => {
  if (companyId) {
    const response = await fetch(
      `${API_DOMAIN}/GetEmployeeByCompanyId?Company_Id=${companyId}`,
      {
        headers: {
          Authorization: token,
        },
      }
    );
    const data = await response.json();
    const listEmployee = data.List_Employee;

    if (!response.ok) {
      throw new Error(data.message || "Không thể lấy dữ liệu.");
    }

    return [...listEmployee];
  }
};

export const GetListServicesByCompanyId = async (companyId) => {
  if (companyId) {
    const response = await fetch(
      `${API_DOMAIN}/GetListServicesByCompanyId?Company_Id=${companyId}`
    );
    const data = await response.json();
    const listServices = data.List_Services;

    if (!response.ok) {
      throw new Error(data.message || "Không thể lấy dữ liệu.");
    }

    return [...listServices];
  }
};

export const ReportWorkComplete = async ({ processId, token }) => {
  const response = await fetch(`${API_DOMAIN}/ReportWorkComplete`, {
    method: "PUT",
    body: JSON.stringify({ Process_Id: processId }),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Không thể cập nhật.");
  }

  return data;
};

export const GetListTypeService = async ({ token }) => {
  const response = await fetch(`${API_DOMAIN}/GetListTypeService`, {
    headers: {
      Authorization: token,
    },
  });
  const data = await response.json();
  const listTypeService = data.List_Type_Service;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...listTypeService];
};

export const GetListUnit = async ({ token }) => {
  const response = await fetch(`${API_DOMAIN}/GetListUnit`, {
    headers: {
      Authorization: token,
    },
  });
  const data = await response.json();
  const listUnit = data.List_Unit;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...listUnit];
};

export const SignUp = async (registerData) => {
  const response = await fetch(`${API_DOMAIN}/SignUp`, {
    method: "POST",
    body: JSON.stringify(registerData),
    headers: {
      "Content-Type": "application/json",
    },
  });

  const data = await response.json();

  if (!response.ok) {
    let errorMessage = data.error;
    if (errorMessage === "Duplicate unique phone please choose others!") {
      errorMessage =
        "Số điện thoại này đã đăng ký tài khoản. Xin vui lòng kiểm tra lại";
    }
    throw new Error(errorMessage);
  }

  return data;
};

export const LogIn = async (loginData) => {
  const response = await fetch(`${API_DOMAIN}/LogIn`, {
    method: "POST",
    body: JSON.stringify(loginData),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const data = await response.json();

  if (!response.ok) {
    let errorMessage = data.error;
    if (errorMessage === "Wrong phone and password") {
      errorMessage = "Số điện thoại hoặc mật khẩu sai. Xin kiểm tra lại.";
    }
    throw new Error(errorMessage);
  }

  return data;
};

export const GetEmployee = async ({ companyId, date, location, token }) => {
  const response = await fetch(
    `${API_DOMAIN}/GetEmployee?Company_Id=${companyId}&Date=${date}&Location=${location}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
  const data = await response.json();
  const listEmployee = data.List_Employee;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...listEmployee];
};

export const AddServices = async ({ newServiceData, token }) => {
  const response = await fetch(`${API_DOMAIN}/AddServices`, {
    method: "POST",
    body: JSON.stringify(newServiceData),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });

  const data = await response.json();

  if (!response.ok) {
    throw new Error(
      data.message || "Máy chủ bị lỗi. Xin vui lòng thử lại sau."
    );
  }

  return data;
};

export const AssignmentWork = async ({ assignData, token }) => {
  const response = await fetch(`${API_DOMAIN}/AssignmentWork`, {
    method: "POST",
    body: JSON.stringify(assignData),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });

  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Máy chủ bị lỗi. Xin vui lòng thử lại sau.");
  }

  return data;
};

export const AddTypeServices = async ({ newTypeServiceData, token }) => {
  const response = await fetch(`${API_DOMAIN}/AddTypeServices`, {
    method: "POST",
    body: JSON.stringify(newTypeServiceData),
    headers: {
      "Content-Type": "application/json",
      Authorization: token,
    },
  });

  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Máy chủ bị lỗi. Xin vui lòng thử lại sau.");
  }

  return data;
};

export const RevenueStatisticsByCompanyId = async ({
  companyId,
  fromDate,
  toDate,
  token,
}) => {
  const response = await fetch(
    `${API_DOMAIN}/RevenueStatisticsByCompanyId?Company_Id=${companyId}&From_Date=${fromDate}&To_Date=${toDate}`,
    {
      method: "GET",
      headers: {
        Authorization: token,
      },
    }
  );
  const data = await response.json();
  const results = data.Results;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...results];
};

export const WorkStatisticsByCompanyId = async ({
  companyId,
  fromDate,
  toDate,
  token,
}) => {
  const response = await fetch(
    `${API_DOMAIN}/WorkStatisticsByCompanyId?Company_Id=${companyId}&From_Date=${fromDate}&To_Date=${toDate}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
  const data = await response.json();
  const results = data.Result;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...results];
};

export const WorkStatisticsByEmpId = async ({
  employeeId,
  fromDate,
  toDate,
  token,
}) => {
  const response = await fetch(
    `${API_DOMAIN}/WorkStatisticsByEmpId?Emp_Id=${employeeId}&From_Date=${fromDate}&To_Date=${toDate}`,
    {
      method: "GET",
      headers: {
        Authorization: token,
      },
    }
  );
  const data = await response.json();
  const results = data.Results;

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...results];
};

export const GetAllCompany = async ({ token }) => {
  const response = await fetch(`${API_DOMAIN}/GetAllCompany`, {
    headers: {
      Authorization: token,
    },
  });
  const data = await response.json();

  if (!response.ok) {
    throw new Error(data.error || "Không thể lấy dữ liệu.");
  }

  return [...data];
};
