import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';

import './index.css';
import App from './App';
import AuthProvider from './store/AuthProvider';
import { MessageContextProvider } from './store/message-context';

ReactDOM.render(
  <MessageContextProvider>
    <AuthProvider>
      <BrowserRouter>
        <App />
        <div id="recaptcha">
          <div id="get-otp-button"></div>
        </div>
      </BrowserRouter>
    </AuthProvider>
  </MessageContextProvider>,
  document.getElementById('root')
);