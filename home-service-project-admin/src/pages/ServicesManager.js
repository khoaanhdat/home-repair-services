import { Fragment, useContext, useEffect, useState } from "react";
import { useHistory } from "react-router";

import NewService from "../components/Services/NewService";
import ServiceList from "../components/Services/ServiceList"
import LoadingSpinner from "../components/UI/LoadingSpinner";
import useHttp from "../hooks/use-http";
import { GetListServicesByCompanyId, AddServices } from '../lib/api';
import AuthContext from "../store/auth-context";
import { MessageContext } from "../store/message-context";

const ServicesManager = (props) => {

    // Check unauthorized
    const history = useHistory();
    const { token, onLogout, companyId } = useContext(AuthContext);
    const { addMessage } = useContext(MessageContext);

    const { sendRequest: getListServices, status, data: listServices, error } = useHttp(GetListServicesByCompanyId, true);

    const { sendRequest: addNewService, status: statusAddService, error: errorAddService } = useHttp(AddServices);

    useEffect(() => {
        getListServices(companyId);
    }, [getListServices]);

    useEffect(() => {
        if (statusAddService === 'completed') {
            setIsShowAddService(false);
            getListServices(companyId);
            addMessage({
                message: "Thêm mới dịch vụ thành công.",
                className: "success"
            });
        } 

        if (errorAddService) {
            if (errorAddService === 'Unauthorized') {
                setTimeout(() => {
                    history.push('/');
                    onLogout();
                    addMessage({ auth: true });
                }, 100);
            } else {
                addMessage({
                    message: errorAddService,
                    className: "error"
                });
            }
        }
    }, [statusAddService, companyId, getListServices, addMessage, errorAddService]);

    const [isShowAddService, setIsShowAddService] = useState(false);

    const showAddService = () => {
        setIsShowAddService(true);
    }

    const hideAddService = () => {
        setIsShowAddService(false);
    }

    if (status === 'pending') {
        return <div className="loading"><LoadingSpinner /></div>
    }

    if (error) {
        return <span>{error}</span>
    }

    if (status === 'completed' && (!listServices || listServices.length === 0)) {
        return <span>Không tìm thấy bất cứ nhân viên nào</span>
    }

    const addNewServiceHandler = (newService) => {
        addNewService({
            newServiceData: {
                Name: newService.Service_Name,
                Price: newService.Price,
                Type_Id: newService.Type_Id,
                Company_Id: companyId
            },
            token
        });
    }

    return (
        <Fragment>
            <ServiceList listServices={listServices} showAddService={showAddService} />
            {isShowAddService && <NewService isLoading={statusAddService === 'pending' ? true : false} addNewService={addNewServiceHandler} isShowAddService={isShowAddService} hideAddService={hideAddService} />}
        </Fragment>
    );
}

export default ServicesManager;