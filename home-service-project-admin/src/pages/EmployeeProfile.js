import { Fragment, useContext, useEffect } from "react";

import Profile from "../components/Employee/Profile";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import useHttp from "../hooks/use-http";
import { GetEmployeeDetailById } from '../lib/api';
import AuthContext from "../store/auth-context";

const EmployeeProfile = (props) => {
    const { employeeId } = useContext(AuthContext);

    const { sendRequest: getEmp, status: getEmpStatus, data: empDetail, error: getEmpError } = useHttp(GetEmployeeDetailById, true);

    useEffect(() => {
        getEmp(employeeId);
    }, [])

    if (getEmpStatus === 'pending') {
        return <div className="loading"><LoadingSpinner /></div>
    }

    if (getEmpError) {
        return <span>{getEmpError}</span>
    }

    if (getEmpStatus === 'completed' && (!empDetail || Object.keys(empDetail).length === 0)) {
        return <span>Không tìm thấy dữ liệu nào.</span>
    }

    return (
        <Fragment>
            <Profile empDetail={empDetail} />
        </Fragment>
    );
}

export default EmployeeProfile;