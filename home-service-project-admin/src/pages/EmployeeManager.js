import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import EmployeeEdit from "../components/Employee/EmployeeEdit"
import EmployeeList from "../components/Employee/EmployeeList"
import NewEmployee from "../components/Employee/NewEmployee";
import useHttp from "../hooks/use-http";
import EmployeeManagerContext from '../store/employee-manager-context';
import { GetEmployeeByCompanyId } from '../lib/api'
import LoadingSpinner from "../components/UI/LoadingSpinner";
import AuthContext from "../store/auth-context";
import { MessageContext } from "../store/message-context";

const EmployeeManager = (props) => {
    // Check unauthorized
    const history = useHistory();
    const { token, onLogout, companyId } = useContext(AuthContext);
    const { addMessage } = useContext(MessageContext);

    const [reload, setReload] = useState();
    const { sendRequest: getListEmployeeFn, status, data: listEmp, error } = useHttp(GetEmployeeByCompanyId, true);

    useEffect(() => {
        getListEmployeeFn({
            companyId,
            token
        });
        setReload(false);
    }, [getListEmployeeFn, reload]);

    const empManagerCtx = useContext(EmployeeManagerContext);

    const isShownEditEmpForm = empManagerCtx.isShowEditEmployee;

    const isShownNewEmployee = empManagerCtx.isShowNewEmployee;

    if (status === 'pending') {
        return <div className="loading"><LoadingSpinner /></div>
    }

    if (error) {
        if (error === 'Unauthorized' || error === 'Access Denied!') {
            setTimeout(() => {
                history.push('/');
                onLogout();
                addMessage({ auth: true });
            }, 500);
            return false;
        } else {
            return <span>{error}</span>
        }
    }

    if (status === 'completed' && (!listEmp || listEmp.length === 0)) {
        return <span>Không tìm thấy bất cứ nhân viên nào</span>
    }

    const reloadHandler = () => {
        setReload(true);
    }

    return (
        <div>
            <EmployeeList listEmp={listEmp} />
            {isShownEditEmpForm && <EmployeeEdit companyId={companyId} reload={reloadHandler} />}
            {isShownNewEmployee && <NewEmployee companyId={companyId} reloadData={reloadHandler} />}
        </div>
    );
}

export default EmployeeManager;