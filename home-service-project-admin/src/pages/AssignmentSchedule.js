import { Fragment, useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import AssignmentScheduleList from "../components/Employee/AssignmentScheduleList";
import LoadingSpinner from "../components/UI/LoadingSpinner";
import useHttp from "../hooks/use-http";
import { GetListAssignmentSchedule } from "../lib/api";
import AuthContext from "../store/auth-context";
import { MessageContext } from "../store/message-context";

const AssignmentSchedule = (props) => {
  const [isReLoad, setIsReLoad] = useState(false);

  const history = useHistory();
  const { employeeId, token, onLogout } = useContext(AuthContext);
  const { addMessage } = useContext(MessageContext);

  const {
    sendRequest,
    status,
    data: listSchedule,
    error,
  } = useHttp(GetListAssignmentSchedule, true);

  useEffect(() => {
    sendRequest({ empId: employeeId, token });
    setIsReLoad(false);
  }, [sendRequest, isReLoad]);

  const reloadHandler = () => {
    setIsReLoad(true);
  };

  if (status === "pending") {
    return (
      <div className="loading">
        <LoadingSpinner />
      </div>
    );
  }

  if (error) {
    if (error === "Unauthorized") {
      setTimeout(() => {
        history.push("/");
        onLogout();
        addMessage({ auth: true });
      }, 100);
      return false;
    } else {
      return <span>{error}</span>;
    }
  }

  if (status === "completed" && (!listSchedule || listSchedule.length === 0)) {
    return (
      <span>
        Không có lịch phân công nào. Xin vui lòng liên hệ với công ty của bạn.
      </span>
    );
  }

  return (
    <Fragment>
      <AssignmentScheduleList
        reloadHandler={reloadHandler}
        listSchedule={listSchedule}
      />
    </Fragment>
  );
};

export default AssignmentSchedule;
