import { useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import AssignEmployee from "../components/Employee/AssignEmployee"
import PostList from "../components/Post/PostList"
import AssignEmployeeContext from "../store/assign-employee-context";
import useHttp from '../hooks/use-http';
import { GetListPost, GetListServicesByCompanyId } from "../lib/api";
import LoadingSpinner from '../components/UI/LoadingSpinner';
import AuthContext from "../store/auth-context";
import { MessageContext } from "../store/message-context";

const PostManager = (props) => {
    const { sendRequest: getListPostFn, status, data: listPost, error } = useHttp(GetListPost, true);

    const [isReload, setIsReload] = useState(false);

    // Check unauthorized
    const history = useHistory();
    const { token, onLogout, companyId } = useContext(AuthContext);
    const { addMessage } = useContext(MessageContext);

    useEffect(() => {
        getListPostFn({token: token});
        setIsReload(false);
    }, [getListPostFn, token, isReload]);

    const {
        sendRequest: getListServices,
        status: statusGetListServices,
        data: listServices,
        error: errorGetListServices
    } = useHttp(GetListServicesByCompanyId, true);

    useEffect(() => {
        getListServices(companyId);
    }, [getListServices]);

    const assignEmpCtx = useContext(AssignEmployeeContext);

    const isShowAssignEmpPopup = assignEmpCtx.isShowAssignEmployee;

    if (status === 'pending') {
        return <div className="loading"><LoadingSpinner /></div>
    }

    if (error) {
        if (error === 'Unauthorized') {
            setTimeout(() => {
                history.push('/');
                onLogout();
                addMessage({ auth: true });
            }, 500);
            return false;
        } else {
            return <span>{error}</span>
        }
    }

    if (status === 'completed' && (!listPost || listPost.length === 0)) {
        return <span>Không tìm thấy bất cứ bài đăng thông báo sửa chữa nào</span>
    }

    const reloadPostsHandler = () => {
        setIsReload(true);
    }

    return (
        <div>
            <PostList listPost={listPost} />
            {
                isShowAssignEmpPopup &&
                !errorGetListServices &&
                statusGetListServices === 'completed' &&
                listServices.length > 0 &&
                <AssignEmployee listServices={listServices} reloadPosts={reloadPostsHandler} />
            }
        </div>
    );
}

export default PostManager;