import { Fragment } from "react";
import Statistics from "../components/Employee/Statistics";

const EmployeeStatistics = (props) => {
    return (
        <Fragment>
            <Statistics />
        </Fragment>
    );
}

export default EmployeeStatistics;