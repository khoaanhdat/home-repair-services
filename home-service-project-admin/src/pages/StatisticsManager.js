import {Fragment} from 'react';

import StatisticsForm from '../components/Statistics/StatisticsForm';

const StatisticsManager = (props) => {
    return (
        <Fragment>
            <StatisticsForm />     
        </Fragment>
    );
}

export default StatisticsManager;